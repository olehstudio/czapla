import React from "react"

import "./customMenuToggler.scss"

function CustomMenuToggler(props) {
  return (
    <div className="menuToggler">
        <span className="topLine"></span>
        <span className="midLine"></span>
        <span className="bottomLine"></span>
    </div>
  )
}

export default CustomMenuToggler
