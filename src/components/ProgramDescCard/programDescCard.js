import React from "react"
import { Col } from "react-bootstrap"

import "./programDescCard.scss"

function ProgramDescCard(props) {
    return (
        <Col md={6} lg={4} style={{ marginBottom: '25px' }}>
            <div className="cardDescWrapper">
                <img src={props.img} alt="" />
                <h3 dangerouslySetInnerHTML={{ __html: props.title }} />
                <div dangerouslySetInnerHTML={{ __html: props.content }} />
            </div>
        </Col>
    )
}

export default ProgramDescCard
