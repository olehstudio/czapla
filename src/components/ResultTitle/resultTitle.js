import React from "react"
import { Col } from "react-bootstrap"

import TitleLogo from "../../images/title_logo.svg"
import "./resultTitle.scss"

function ResultTitle(props) {
  const wrapLastLetterInSpan = (string, br = false) => {
    let tempArr = string.split(" ")
    let brOutput = br ? "<br/>" : ""
    tempArr[tempArr.length - 1] =
      "<span>" + tempArr[tempArr.length - 1] + "</span>" + brOutput
    return tempArr.join(" ")
  }

  return (
    <Col md={12} className="resultTitle">
      <img src={`${TitleLogo}`} alt="Czapla Logo" />
      <h1 dangerouslySetInnerHTML={{ __html: wrapLastLetterInSpan(props.title) }} />
      <h2 dangerouslySetInnerHTML={{ __html: props.text }} />
    </Col>
  )
}

export default ResultTitle
