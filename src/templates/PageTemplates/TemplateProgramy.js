import React from "react"
import Layout from "../../components/layout"
import SEOCzapla from "../../components/seoczapla"

import ProgramsSection from "../../components/ProgramsSection/programsSection"
import CzaplaLogo from "../../images/czapla_logo.jpg"

const TemplateProgramy = ({ data }) => (
  <Layout>
    <SEOCzapla
      title={data.wordpressPage.yoast_meta.yoast_wpseo_title}
      description={
        data.wordpressPage.yoast_meta.yoast_wpseo_metadesc !== null
          ? data.wordpressPage.yoast_meta.yoast_wpseo_metadesc
          : ""
      }
      image={CzaplaLogo}
    />

    <ProgramsSection
      title={data.wordpressPage.title}
      desc={data.wordpressPage.content}
      nodes={data.allWordpressWpFilary}
    />
  </Layout>
)

export default TemplateProgramy

export const query = graphql`
  query($id: Int!) {
    wordpressPage(wordpress_id: { eq: $id }) {
      wordpress_id
      title
      content
      yoast_meta {
        yoast_wpseo_title
        yoast_wpseo_metadesc
      }
    }
    allWordpressWpFilary {
      nodes {
        acf {
          programy {
            post_title
            post_name
            post_content
          }
          count_of_bold_words
        }
        title
        featured_media {
          localFile {
            childImageSharp {
              resolutions(width: 560, cropFocus: CENTER, quality: 100) {
                src
              }
            }
          }
        }
      }
    }
  }
`
