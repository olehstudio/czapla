import React from "react"
import Layout from "../../components/layout"
import SEOCzapla from "../../components/seoczapla"
import { Container, Row } from "react-bootstrap"

import SmallPostCard from "../../components/SmallPostCard/smallPostCard"
import ResultTitle from "../../components/ResultTitle/resultTitle"
import CzaplaLogo from "../../images/czapla_logo.jpg"

const TemplateCategory = ({ data }) => {
  return (
    <Layout>
      <SEOCzapla
        title={data.wordpressTag.name}
        description={`Czapla and More. Podstrona tagu: ${data.wordpressTag.name}`}
        image={CzaplaLogo}
      />

      <Container fluid={true} className="standardPadding tagPage">
        <Row>
          <ResultTitle title="Strona Tagu" text={data.wordpressTag.name} />

          {data.allWordpressPost.nodes.map((post, index) => (
            <SmallPostCard
              key={index}
              col="3"
              image={
                post.acf.gallery[0].localFile.childImageSharp.resolutions.src
              }
              title={post.title}
              date={post.date}
              categories={post.categories}
              tags={post.tags}
              content={post.content}
              slug={post.slug}
            />
          ))}
        </Row>
      </Container>
    </Layout>
  )
}

export default TemplateCategory

export const query = graphql`
  query($id: Int!, $name: String!) {
    wordpressTag(wordpress_id: { eq: $id }) {
      name
      wordpress_id
    }
    allWordpressPost(filter: { tags: { elemMatch: { name: { eq: $name } } } }) {
      nodes {
        title
        content
        slug
        categories {
          name
          slug
        }
        tags {
          slug
          name
        }
        date(locale: "pl")
        acf {
          gallery {
            localFile {
              childImageSharp {
                resolutions(
                  width: 860
                  height: 500
                  cropFocus: CENTER
                  quality: 90
                ) {
                  src
                }
              }
            }
          }
        }
      }
    }
  }
`
