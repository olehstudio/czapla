import React from "react"
import Layout from "../../components/layout"
import SEOCzapla from "../../components/seoczapla"
import { Container, Row, Col } from "react-bootstrap"

import Title from "../../components/Title/title"
import ProgramTitle from "../../components/ProgramTitle/programTitle"
import ContactFormProduct from "../../components/ContactFormProduct/contactFormProduct"
import CzaplaLogo from "../../images/czapla_logo.jpg"
import "./TemplateSingleProduct.scss"

const TemplateSingleProduct = ({ data }) => {
  return (
    <Layout>
      <SEOCzapla
        title={data.wordpressWpProductCzapla.yoast_meta.yoast_wpseo_title}
        description={
          data.wordpressWpProductCzapla.yoast_meta.yoast_wpseo_metadesc !== null
            ? data.wordpressWpProductCzapla.yoast_meta.yoast_wpseo_metadesc
            : ""
        }
        image={
          data.wordpressWpProductCzapla.featured_media.localFile.childImageSharp
            .resolutions.src !== null
            ? data.wordpressWpProductCzapla.featured_media.localFile
                .childImageSharp.resolutions.src
            : CzaplaLogo
        }
      />

      <Container fluid={true} className="singleProduct">
        <Row>
          <Col md={12}>
            <Title
              text={data.wordpressWpProductCzapla.title}
              mainTitle={true}
            />
          </Col>

          <Col lg={{ span: 6, offset: 3 }} className="singleProduct__content">
            <div
              dangerouslySetInnerHTML={{
                __html: data.wordpressWpProductCzapla.content,
              }}
            />
          </Col>

          {data.wordpressWpProductCzapla.acf.price && (
            <Col lg={{ span: 6, offset: 3 }} className="singleProduct__price">
              <h5>
                CENA:
                <span
                  dangerouslySetInnerHTML={{
                    __html: data.wordpressWpProductCzapla.acf.price,
                  }}
                />
              </h5>
            </Col>
          )}
        </Row>

        <Col md={12}>
          <ProgramTitle
            mainProgramTitle={data.wordpressWpProductCzapla.title}
            text="Formularz zamówienia"
            mainTitle={false}
          />
        </Col>

        <ContactFormProduct product={data.wordpressWpProductCzapla.title} />
      </Container>
    </Layout>
  )
}

export default TemplateSingleProduct

export const query = graphql`
  query($id: Int!) {
    wordpressWpProductCzapla(wordpress_id: { eq: $id }) {
      title
      content
      acf {
        price
      }
      featured_media {
        localFile {
          childImageSharp {
            resolutions(width: 512, cropFocus: CENTER, quality: 100) {
              src
            }
          }
        }
      }
      yoast_meta {
        yoast_wpseo_title
        yoast_wpseo_metadesc
      }
    }
  }
`
