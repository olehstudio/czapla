import React from "react"
import Layout from "../components/layout"
import SEOCzapla from "../components/seoczapla"
import axios from "axios"
import queryString from "query-string"
import { Container, Row, Col } from "react-bootstrap"
import Skeleton from "@material-ui/lab/Skeleton"

import ResultTitle from "../components/ResultTitle/resultTitle"
import SmallPostCard from "../components/SmallPostCard/smallPostCard"
import NoResultIcon from "../images/no_result.svg"
import CzaplaLogo from "../images/czapla_logo.jpg"
import "./search.scss"

class SearchPage extends React.Component {
  constructor(props) {
    super(props)

    this.state = {
      searchText: "",
      isLoading: true,
      posts: [],
    }
  }

  componentDidMount() {
    const parsed = queryString.parse(window.location.search)
    this.setState({ searchText: parsed.search })

    axios({
      method: "GET",
      url: "https://czaplaandmore.pixelart.pl/wp-json/searchPosts/v2",
      params: {
        searchQuery: parsed.search,
      },
    }).then(response => {
      console.log(response.data)
      this.setState({
        posts: response.data,
        isLoading: false,
      })
    })
  }

  render() {
    return (
      <Layout>
        <SEOCzapla
          title="CzaplaAndMore - Wyniki wyszukiwania"
          description=""
          image={CzaplaLogo}
        />
        
        <Container fluid={true} className="standardPadding searchPage">
          <Row>
            <ResultTitle
              title="Wyniki wyszukiwania"
              text={this.state.searchText}
            />

            {this.state.isLoading ? (
              Array.from(new Array(4)).map(() => (
                <Col md={6} lg={3}>
                  <div style={{ marginBottom: "10px" }}>
                    <Skeleton height={240} />
                  </div>
                  <div style={{ marginBottom: "10px" }}>
                    <Skeleton height={30} />
                  </div>
                  <div style={{ marginBottom: "10px" }}>
                    <Skeleton height={20} />
                    <Skeleton height={20} />
                  </div>
                  <div style={{ marginBottom: "10px" }}>
                    <Skeleton height={70} />
                  </div>
                  <div style={{ marginBottom: "75px" }}>
                    <Skeleton height={40} width={180} />
                  </div>
                </Col>
              ))
            ) : this.state.posts.length > 0 ? (
              this.state.posts.map((post, index) => (
                <SmallPostCard
                  key={index}
                  col={3}
                  image={post.image}
                  title={post.title}
                  date={post.date}
                  categories={post.categories}
                  tags={post.tags}
                  content={post.content}
                  slug={post.slug}
                />
              ))
            ) : (
              <div
                style={{
                  width: "100%",
                  display: "flex",
                  flexDirection: "column",
                  alignItems: "center",
                }}
              >
                <img src={`${NoResultIcon}`} alt="No Result Icon" />
                <h2 className="noResultTitle">Niczego nie znaleziono</h2>
                <p style={{ marginBottom: "100px" }}>Spróbuj jeszcze raz</p>
              </div>
            )}
          </Row>
        </Container>
      </Layout>
    )
  }
}

export default SearchPage
