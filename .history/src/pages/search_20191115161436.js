import React from "react"
import Layout from "../components/layout"
import SEO from "../components/seo"
import axios from "axios"
import queryString from "query-string"
import { Container, Row } from "react-bootstrap"
import Skeleton from "@material-ui/lab/Skeleton"

import ResultTitle from "../components/ResultTitle/resultTitle"
import SmallPostCard from "../components/SmallPostCard/smallPostCard"

class SearchPage extends React.Component {
  constructor(props) {
    super(props)

    this.state = {
      searchText: "",
      isLoading: true,
      posts: [],
    }
  }

  componentDidMount() {
    const parsed = queryString.parse(window.location.search)
    this.setState({ searchText: parsed.search })

    axios({
      method: "GET",
      url: "https://czaplaandmore.pixelart.pl/wp-json/searchPosts/v2",
      params: {
        searchQuery: parsed.search,
      },
    }).then(response => {
      console.log(response.data)
      this.setState({
        posts: response.data,
        isLoading: false,
      })
    })
  }

  render() {
    return (
      <Layout>
        <SEO title="Home" keywords={[`gatsby`, `application`, `react`]} />
        <Container fluid={true} className="standardPadding">
          <Row>
            <ResultTitle
              title="Wyniki wyszukiwania"
              text={this.state.searchText}
            />

            {this.state.isLoading ? (
              <Col md={this.props.direction === "row" ? 3 : 12}>
                {/* <div style={{ marginBottom: "10px" }}>
                <Skeleton height={240} />
              </div>
              <div style={{ marginBottom: "10px" }}>
                <Skeleton height={30} />
              </div>
              <div style={{ marginBottom: "10px" }}>
                <Skeleton height={20} />
                <Skeleton height={20} />
              </div>
              <div style={{ marginBottom: "10px" }}>
                <Skeleton height={70} />
              </div>
              <div style={{ marginBottom: "10px" }}>
                <Skeleton height={40} width={180} />
              </div> */}
              </Col>
            ) : (
              this.state.posts.map((post, index) => (
                <SmallPostCard
                  key={index}
                  col={3}
                  image={post.image}
                  title={post.title}
                  date={post.date}
                  categories={post.categories}
                  tags={post.tags}
                  content={post.content}
                  slug={post.slug}
                />
              ))
            )}
          </Row>
        </Container>
      </Layout>
    )
  }
}

export default SearchPage
