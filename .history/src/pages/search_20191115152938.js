import React from "react"
import Layout from "../components/layout"
import SEO from "../components/seo"

import ResultTitle from "../../components/ResultTitle/resultTitle"

class SearchPage extends React.Component {
  constructor(props) {
    super(props)

    this.state = {
      posts: [],
      isLoading: true,
    }
  }

  render() {
    return (
        <>
        </>
    )
  }
}
const SearchPage = ({ data }) => (
  <Layout>
    <SEO title="Home" keywords={[`gatsby`, `application`, `react`]} />
    <HomepageCarousel data={data.wordpressPage.acf.slider_repeater} />
    <HomepageAbout
      title={data.wordpressPage.acf.about_title}
      content={data.wordpressPage.acf.about_content}
      video={data.wordpressPage.acf.about_video}
    />
    <ProgramsSection
      title={data.wordpressPage.acf.programs_title}
      desc={data.wordpressPage.acf.programs_description}
      nodes={data.allWordpressWpFilary}
    />
    <HomepageNumbers
      data={data.wordpressPage.acf.numbers_repeater}
      title={data.wordpressPage.acf.numbers_title}
      bg={data.wordpressPage.acf.numbers_background}
    />
    <Partners
      data={data.wordpressPage.acf.partners_repeater}
      title={data.wordpressPage.acf.partners_title}
    />
    <Newsletter />
    <References
      data={data.wordpressPage.acf.references_repeater}
      title={data.wordpressPage.acf.reference_title}
    />
  </Layout>
)

export default SearchPage
