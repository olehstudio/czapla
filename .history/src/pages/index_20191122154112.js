import React from "react"
import Layout from "../components/layout"
import SEO from "../components/seo"

import HomepageCarousel from "../components/HomepageCarousel/homepageCarousel"
import HomepageAbout from "../components/HomepageAbout/homepageAbout"
import ProgramsSection from "../components/ProgramsSection/programsSection"
import HomepageNumbers from "../components/HomepageNumbers/homepageNumbers"
import Partners from "../components/Partners/partners"
import References from "../components/References/references"
import Newsletter from "../components/Newsletter/newsletter"

const IndexPage = ({ data }) => (
  <Layout>
    <SEO title="Home" keywords={[`gatsby`, `application`, `react`]} />
    <HomepageCarousel data={data.wordpressPage.acf.slider_repeater} />
    <HomepageAbout
      title={data.wordpressPage.acf.about_title}
      content={data.wordpressPage.acf.about_content}
      video={data.wordpressPage.acf.about_video}
    />
    <ProgramsSection
      title={data.wordpressPage.acf.programs_title}
      desc={data.wordpressPage.acf.programs_description}
      nodes={data.allWordpressWpFilary}
    />
    <HomepageNumbers
      data={data.wordpressPage.acf.numbers_repeater}
      title={data.wordpressPage.acf.numbers_title}
      bg={data.wordpressPage.acf.numbers_background}
    />
    <Partners
      data={data.wordpressPage.acf.partners_repeater}
      title={data.wordpressPage.acf.partners_title}
    />
    <Newsletter />
    <References
      data={data.wordpressPage.acf.references_repeater}
      title={data.wordpressPage.acf.reference_title}
    />
  </Layout>
)

export default IndexPage
export const query = graphql`
  query {
    wordpressPage(wordpress_id: { eq: 11 }) {
      acf {
        slider_repeater {
          single_slide {
            localFile {
              childImageSharp {
                fluid(quality: 90, maxWidth: 1920) {
                  ...GatsbyImageSharpFluid_withWebp
                  }
              }
            }
          }
        }
        about_title
        about_content
        about_video
        programs_title
        programs_description
        numbers_title
        numbers_repeater {
          number_description
          number_single_number
        }
        numbers_background {
          localFile {
            childImageSharp {
              resolutions(
                width: 1280
                height: 480
                cropFocus: CENTER
                quality: 90
              ) {
                src
              }
            }
          }
        }
        partners_title
        partners_repeater {
          partner_link
          partner_image {
            localFile {
              childImageSharp {
                resolutions(width: 320, cropFocus: CENTER, quality: 90) {
                  src
                }
              }
            }
          }
        }
        reference_title
        references_repeater {
          reference_content
          reference_author
        }
      }
    }
    allWordpressWpFilary(sort: {fields: date, order: DESC}) {
      nodes {
        acf {
          programy {
            post_title
            post_name
            post_content
          }
        }
        title
        featured_media {
          localFile {
            childImageSharp {
              resolutions(width: 560, cropFocus: CENTER, quality: 90) {
                src
              }
            }
          }
        }
      }
    }
  }
`
