import React from "react"
import Layout from "../components/layout"
import SEO from "../components/seo"
import axios from "axios"
import { Container, Row, Col } from "react-bootstrap"

import ResultTitle from "../../components/ResultTitle/resultTitle"

class SearchPage extends React.Component {
  constructor(props) {
    super(props)

    this.state = {
      posts: [],
      isLoading: true,
    }
  }

  componentDidMount() {

  }

  render() {
    return (
        <Container>
            <Row>
                <ResultTitle title="Wyniki wyszukiwania" text={this.state.searchText} />
            </Row>
        </Container>>
    )
  }
}

export default SearchPage
