import React from "react"
import Layout from "../components/layout"
import SEO from "../components/seo"
import axios from "axios"
import queryString from 'query-string'
import { Container, Row, Col } from "react-bootstrap"

import ResultTitle from "../components/ResultTitle/resultTitle"

class SearchPage extends React.Component {
  constructor(props) {
    super(props)

    this.state = {
      searchText: "",
    }
  }

  componentDidMount() {
    const parsed = queryString.parse(window.location.search);
    console.log(parsed);
  }

  render() {
    return (
      <Layout>
        <SEO title="Home" keywords={[`gatsby`, `application`, `react`]} />
        <Container>
          <Row>
            <ResultTitle
              title="Wyniki wyszukiwania"
              text={this.state.searchText}
            />
          </Row>
        </Container>
      </Layout>
    )
  }
}

export default SearchPage
