import React from "react"
import Layout from "../components/layout"
import SEO from "../components/seo"
import axios from "axios"
import queryString from "query-string"
import { Container, Row, Col } from "react-bootstrap"

import Title from "../components/Title/Title"

class ContactPage extends React.Component {
  constructor(props) {
    super(props)

    this.state = {
      getText: "",
      isLoading: true,
    }
  }

  render() {
    return (
      <Layout>
        <SEO title="Home" keywords={[`gatsby`, `application`, `react`]} />
        <Container fluid={true} className="standardPadding">
          <Row>
            <Title
              text={}
              mainTitle={true}
            />
          </Row>
        </Container>
      </Layout>
    )
  }
}

export default ContactPage
export const query = graphql`
  query {
    wordpressPage(wordpress_id: { eq: 11 }) {
      acf {
        slider_repeater {
          single_slide {
            localFile {
              childImageSharp {
                resolutions(
                  width: 1920
                  height: 680
                  cropFocus: CENTER
                  quality: 90
                ) {
                  src
                }
              }
            }
          }
        }
        about_title
        about_content
        about_video
        programs_title
        programs_description
        numbers_title
        numbers_repeater {
          number_description
          number_single_number
        }
        numbers_background {
          localFile {
            childImageSharp {
              resolutions(
                width: 1280
                height: 480
                cropFocus: CENTER
                quality: 90
              ) {
                src
              }
            }
          }
        }
        partners_title
        partners_repeater {
          partner_link
          partner_image {
            localFile {
              childImageSharp {
                resolutions(width: 320, cropFocus: CENTER, quality: 90) {
                  src
                }
              }
            }
          }
        }
        reference_title
        references_repeater {
          reference_content
          reference_author
        }
      }
    }
    allWordpressWpFilary(sort: {fields: date, order: DESC}) {
      nodes {
        acf {
          programy {
            post_title
            post_name
            post_content
          }
        }
        title
        featured_media {
          localFile {
            childImageSharp {
              resolutions(width: 560, cropFocus: CENTER, quality: 90) {
                src
              }
            }
          }
        }
      }
    }
  }
`
