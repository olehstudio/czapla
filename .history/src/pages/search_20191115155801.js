import React from "react"
import Layout from "../components/layout"
import SEO from "../components/seo"
import axios from "axios"
import queryString from "query-string"
import { Container, Row, Col } from "react-bootstrap"

import ResultTitle from "../components/ResultTitle/resultTitle"
import SmallPostCard from "../SmallPostCard/smallPostCard"

class SearchPage extends React.Component {
  constructor(props) {
    super(props)

    this.state = {
      searchText: "",
      isLoading: true,
      posts: [],
    }
  }

  componentDidMount() {
    const parsed = queryString.parse(window.location.search)
    this.setState({ searchText: parsed.search })

    axios({
      method: "GET",
      url: "https://czaplaandmore.pixelart.pl/wp-json/searchPosts/v2",
      data: {
        searchQuery: parsed.search
      }
    }).then(response => {
      console.log(response.data)
      this.setState({
        posts: response.data,
        isLoading: false,
      })
    })
  }

  render() {
    return (
      <Layout>
        <SEO title="Home" keywords={[`gatsby`, `application`, `react`]} />
        <Container>
          <Row>
            <ResultTitle
              title="Wyniki wyszukiwania"
              text={this.state.searchText}
            />

            {this.state.posts.map((post, index) => (
              <SmallPostCard
                key={index}
                col={this.props.direction === "row" ? "3" : "12"}
                image={post.image}
                title={post.title}
                date={post.date}
                categories={post.categories}
                tags={post.tags}
                content={post.content}
                slug={post.slug}
              />
            ))}
          </Row>
        </Container>
      </Layout>
    )
  }
}

export default SearchPage
