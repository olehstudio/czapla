import React from "react"
import Layout from "../components/layout"
import SEO from "../components/seo"
import axios from "axios"
import queryString from "query-string"
import { Container, Row, Col } from "react-bootstrap"
import Skeleton from "@material-ui/lab/Skeleton"

import Title from "../components/Title/Title"
import SmallPostCard from "../components/SmallPostCard/smallPostCard"

class SearchPage extends React.Component {
  constructor(props) {
    super(props)

    this.state = {
      searchText: "",
      isLoading: true,
      posts: [],
    }
  }

  componentDidMount() {
    const parsed = queryString.parse(window.location.search)
    this.setState({ searchText: parsed.search })

    axios({
      method: "GET",
      url: "https://czaplaandmore.pixelart.pl/wp-json/searchPosts/v2",
      params: {
        searchQuery: parsed.search,
      },
    }).then(response => {
      console.log(response.data)
      this.setState({
        posts: response.data,
        isLoading: false,
      })
    })
  }

  render() {
    return (
      <Layout>
        <SEO title="Home" keywords={[`gatsby`, `application`, `react`]} />
        <Container fluid={true} className="standardPadding">
          <Row>
            <ResultTitle
              title="Wyniki wyszukiwania"
              text={this.state.searchText}
            />
          </Row>
        </Container>
      </Layout>
    )
  }
}

export default SearchPage
