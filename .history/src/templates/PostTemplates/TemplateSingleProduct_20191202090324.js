import React from "react"
import Layout from "../../components/layout"
import SEO from "../../components/seo"
import { Container, Row, Col } from "react-bootstrap"

import Title from "../../components/Title/title"
import ProgramTitle from "../../components/ProgramTitle/programTitle"
import ContactFormProduct from "../../components/ContactFormProduct/contactFormProduct"
import "./TemplateSingleProduct.scss"

const TemplateSingleProduct = ({ data }) => {
  return (
    <Layout>
      <SEO title="Home" keywords={[`gatsby`, `application`, `react`]} />

      <Container fluid={true} className="singleProduct">
        <Row>
          <Col md={12}>
            <Title
              text={data.wordpressWpProductCzapla.title}
              mainTitle={true}
            />
          </Col>

          <Col lg={{ span: 6, offset: 3 }} className="singleProduct__content">
            <div
              dangerouslySetInnerHTML={{
                __html: data.wordpressWpProductCzapla.content,
              }}
            />
          </Col>

          <Col lg={{ span: 6, offset: 3 }} className="singleProduct__content">
            <h5 className="singleProduct__price">
              <span dangerouslySetInnerHTML={{ __html: data.wordpressWpProductCzapla.acf.cena }} />
            </h5>
          </Col>
        </Row>

        <Col md={12}>
          <ProgramTitle
            mainProgramTitle={data.wordpressWpProductCzapla.title}
            text="Formularz zamówienia"
            mainTitle={false}
          />
        </Col>

        <ContactFormProduct product={data.wordpressWpProductCzapla.title} />
      </Container>
    </Layout>
  )
}

export default TemplateSingleProduct

export const query = graphql`
  query($id: Int!) {
    wordpressWpProductCzapla(wordpress_id: { eq: $id }) {
      title
      content
      acf {
        price
      }
    }
  }
`
