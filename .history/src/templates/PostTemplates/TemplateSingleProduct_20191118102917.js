import React from "react"
import Layout from "../../components/layout"
import SEO from "../../components/seo"
import { Container, Row, Col } from "react-bootstrap"

import ContactForm from "../../components/ContactForm/contactForm"
import Title from "../../components/Title/title"
import "./templateContact.scss"

const TemplateContact = ({ data }) => {
  return (
    <Layout>
      <SEO title="Home" keywords={[`gatsby`, `application`, `react`]} />

      <img
        style={{ width: "100%" }}
        src={
          data.wordpressWpProductCzapla.featured_media.localFile.childImageSharp
            .resolutions.src
        }
        alt={data.wordpressWpProductCzapla.title}
      />

      <Container fluid={true}>
        <Row>
          <Col md={12}>
            <Title text={data.wordpressWpProductCzapla.title} mainTitle={true} />
          </Col>

          
        </Row>
      </Container>
    </Layout>
  )
}

export default TemplateContact

export const query = graphql`
  query($id: Int!) {
    wordpressWpProductCzapla(wordpress_id: { eq: $id }) {
      title
      content
      featured_media {
        localFile {
          childImageSharp {
            resolutions(width: 560, cropFocus: CENTER, quality: 100) {
              src
            }
          }
        }
      }
    }
  }
`
