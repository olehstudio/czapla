import React from "react"
import Layout from "../../components/layout"
import SEO from "../../components/seo"
import { Container, Row, Col } from "react-bootstrap"
import { Link } from "gatsby"

import Newsletter from "../../components/Newsletter/newsletter"
import Title from "../../components/Title/Title"

const TemplateSinglePost = ({ data }) => {
  return (
    <Layout>
      <SEO title="Home" keywords={[`gatsby`, `application`, `react`]} />


      <Newsletter />
    </Layout>
  )
}

export default TemplateSinglePost

export const query = graphql`
  query($id: Int!) {
    wordpressPost(wordpress_id: { eq: $id }) {
      title
      content
      slug
      categories {
        name
        slug
      }
      date(formatString: "d MMMM Y", locale: "pl")
      acf {
        gallery {
          localFile {
            childImageSharp {
              resolutions {
                src
              }
            }
          }
        }
      }
    }
  }
`
