import React from "react"
import Layout from "../../components/layout"
import SEO from "../../components/seo"
import { Container, Row, Col, Carousel } from "react-bootstrap"
import { Link } from "gatsby"

import Newsletter from "../../components/Newsletter/newsletter"
// import MostReadable from "../../components/MostReadable/mostReadable"
import Arrow from "../../components/ArrowComponent/arrowComponent"
import Title from "../../components/Title/Title"
import "./TemplateSinglePost.scss"

const TemplateSinglePost = ({ data }) => {
  return (
    <Layout>
      <SEO title="Home" keywords={[`gatsby`, `application`, `react`]} />
      <Container fluid={true} className="standardPadding">
        <Row>
          <Col lg={9}>
            <Carousel prevIcon={<Arrow />} nextIcon={<Arrow />}>
              {data.wordpressPost.acf.gallery.map((slide, index) => (
                <Carousel.Item key={index}>
                  <img
                    className=""
                    src={
                      slide.localFile.childImageSharp.resolutions
                        .src
                    }
                    alt=""
                  />
                </Carousel.Item>
              ))}
            </Carousel>
          </Col>

          {/* <MostReadable /> */}
        </Row>
      </Container>
      <Newsletter />
    </Layout>
  )
}

export default TemplateSinglePost

export const query = graphql`
  query($id: Int!) {
    wordpressPost(wordpress_id: { eq: $id }) {
      title
      content
      slug
      categories {
        name
        slug
      }
      date(formatString: "d MMMM Y", locale: "pl")
      acf {
        gallery {
          localFile {
            childImageSharp {
              resolutions(width: 1200, height: 560, cropFocus: CENTER, quality: 90) {
                src
              }
            }
          }
        }
      }
    }
  }
`
