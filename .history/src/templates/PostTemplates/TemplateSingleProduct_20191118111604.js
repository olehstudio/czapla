import React from "react"
import Layout from "../../components/layout"
import SEO from "../../components/seo"
import { Container, Row, Col } from "react-bootstrap"

import Title from "../../components/Title/title"

const TemplateSingleProduct = ({ data }) => {
  return (
    <Layout>
      <SEO title="Home" keywords={[`gatsby`, `application`, `react`]} />

      <img
        style={{ width: "100%" }}
        src={
          data.wordpressWpProductCzapla.featured_media.localFile.childImageSharp
            .resolutions.src
        }
        alt={data.wordpressWpProductCzapla.title}
      />

      <Container fluid={true}>
        <Row>
          <Col md={12}>
            <Title text={data.wordpressWpProductCzapla.title} mainTitle={true} />
          </Col>

          <Col lg={{ span: 6, offset: 3 }}>
            <div dangerouslySetInnerHTML={{ __html: data.wordpressWpProductCzapla.content }} />
          </Col>
        </Row>
      </Container>
    </Layout>
  )
}

export default TemplateSingleProduct

export const query = graphql`
  query($id: Int!) {
    wordpressWpProductCzapla(wordpress_id: { eq: $id }) {
      title
      content
      featured_media {
        localFile {
          childImageSharp {
            resolutions(width: 1920, cropFocus: CENTER, quality: 90) {
              src
            }
          }
        }
      }
    }
  }
`
