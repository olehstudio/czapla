import React from "react"
import Layout from "../../components/layout"
import SEO from "../../components/seo"
import { Container, Row, Col, Carousel } from "react-bootstrap"
import { Link } from "gatsby"
import { StickyContainer, Sticky } from "react-sticky"

import Newsletter from "../../components/Newsletter/newsletter"
// import MostReadable from "../../components/MostReadable/mostReadable"
import Arrow from "../../components/ArrowComponent/arrowComponent"
import Title from "../../components/Title/Title"
import "./TemplateSinglePost.scss"

const TemplateSinglePost = ({ data }) => {
  return (
    <Layout>
      <SEO title="Home" keywords={[`gatsby`, `application`, `react`]} />
      <Container fluid={true} className="standardPadding singlePostWrapper">
        <Row>
          <Col lg={9}>
            <Carousel prevIcon={<Arrow />} nextIcon={<Arrow />}>
              {data.wordpressPost.acf.gallery.map((slide, index) => (
                <Carousel.Item key={index}>
                  <img
                    className=""
                    src={slide.localFile.childImageSharp.resolutions.src}
                    alt=""
                  />
                </Carousel.Item>
              ))}
            </Carousel>

            <Row>
              <Col md={4}>
                <StickyContainer>
                  <Sticky bottomOffset={250}>
                    {({
                      style,
                      isSticky,
                      wasSticky,
                      distanceFromTop,
                      distanceFromBottom,
                      calculatedHeight,
                    }) => (
                      <div style={style}>
                        {data.wordpressPost.tags.map((tags, index) => (
                          <a
                            href={`/tag/${tags.slug}`}
                            className="czaplaLink"
                            key={index}
                          >
                            {tags.name}
                          </a>
                        ))}
                      </div>
                    )}
                  </Sticky>
                </StickyContainer>
              </Col>

              <Col md={8}></Col>
            </Row>
          </Col>

          {/* <MostReadable /> */}
        </Row>
      </Container>

      <Newsletter />
    </Layout>
  )
}

export default TemplateSinglePost

export const query = graphql`
  query($id: Int!) {
    wordpressPost(wordpress_id: { eq: $id }) {
      title
      content
      slug
      categories {
        name
        slug
      }
      tags {
        slug
        name
      }
      date(formatString: "d MMMM Y", locale: "pl")
      acf {
        gallery {
          localFile {
            childImageSharp {
              resolutions(
                width: 1200
                height: 560
                cropFocus: CENTER
                quality: 90
              ) {
                src
              }
            }
          }
        }
      }
    }
  }
`
