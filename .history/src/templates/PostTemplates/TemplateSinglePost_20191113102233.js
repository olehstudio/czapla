import React from "react"
import Layout from "../../components/layout"
import SEO from "../../components/seo"
import { Container, Row, Col, Carousel } from "react-bootstrap"
import { Link } from "gatsby"
import { StickyContainer, Sticky } from "react-sticky"
import { CopyToClipboard } from "react-copy-to-clipboard"

import Newsletter from "../../components/Newsletter/newsletter"
// import MostReadable from "../../components/MostReadable/mostReadable"
import Arrow from "../../components/ArrowComponent/arrowComponent"
import Title from "../../components/Title/Title"
import "./TemplateSinglePost.scss"

const TemplateSinglePost = ({ data }) => {
  const currentUrl = window.location.href

  return (
    <Layout>
      <SEO title="Home" keywords={[`gatsby`, `application`, `react`]} />
      <Container fluid={true} className="standardPadding singlePostWrapper">
        <Row>
          <Col lg={9}>
            <Carousel prevIcon={<Arrow />} nextIcon={<Arrow />}>
              {data.wordpressPost.acf.gallery.map((slide, index) => (
                <Carousel.Item key={index}>
                  <img
                    className=""
                    src={slide.localFile.childImageSharp.resolutions.src}
                    alt=""
                  />
                </Carousel.Item>
              ))}
            </Carousel>

            <StickyContainer>
              <Row
                style={{
                  marginTop: "100px",
                }}
              >
                <Col md={4}>
                  <Sticky>
                    {({
                      style,
                      isSticky,
                      wasSticky,
                      distanceFromTop,
                      distanceFromBottom,
                      calculatedHeight,
                    }) => (
                      <div className="stickyWrapper" style={style}>
                        {data.wordpressPost.tags.map((tags, index) => (
                          <a
                            href={`/tag/${tags.slug}`}
                            className="czaplaLink"
                            key={index}
                          >
                            {tags.name}
                          </a>
                        ))}

                        <div className="socialLinksWrapper">
                          <CopyToClipboard text={currentUrl}>
                            <a href=""></a>
                          </CopyToClipboard>

                          <a
                            href={`https://www.facebook.com/sharer/sharer.php?u=${currentUrl}`}
                          ></a>

                          <a
                            href={`https://www.linkedin.com/shareArticle?mini=true&url=${currentUrl}`}
                          ></a>
                        </div>
                      </div>
                    )}
                  </Sticky>
                </Col>

                <Col md={8}>
                  <div
                    dangerouslySetInnerHTML={{
                      __html: data.wordpressPost.content,
                    }}
                  />
                </Col>
              </Row>
            </StickyContainer>
          </Col>

          {/* <MostReadable /> */}
        </Row>
      </Container>

      <Newsletter />
    </Layout>
  )
}

export default TemplateSinglePost

export const query = graphql`
  query($id: Int!) {
    wordpressPost(wordpress_id: { eq: $id }) {
      title
      content
      slug
      categories {
        name
        slug
      }
      tags {
        slug
        name
      }
      date(formatString: "d MMMM Y", locale: "pl")
      acf {
        gallery {
          localFile {
            childImageSharp {
              resolutions(
                width: 1200
                height: 560
                cropFocus: CENTER
                quality: 90
              ) {
                src
              }
            }
          }
        }
      }
    }
  }
`
