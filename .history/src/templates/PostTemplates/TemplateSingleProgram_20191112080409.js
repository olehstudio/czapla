import React from "react"
import Layout from "../../components/layout"
import SEO from "../../components/seo"
import { Container, Row, Col } from "react-bootstrap"
import { Link } from "gatsby"

import Newsletter from "../../components/Newsletter/newsletter"
import ProgramTitle from "../../components/ProgramTitle/programTitle"
import ProgramDescCard from "../../components/ProgramDescCard/programDescCard"

import GoalIcon from "../../images/icon_cel.svg"
import DurationIcon from "../../images/icon_czas.svg"
import BenefitsIcon from "../../images/icon_korzysci.svg"
import ForWhomIcon from "../../images/icon_dla_kogo.svg"
import GroupIcon from "../../images/icon_grupa.svg"

const TemplateSingleProgram = ({ data }) => {
  const getMainProgramTitle = () => {
    let currentProgramId = data.wordpressWpProgram.wordpress_id
    let mainProgramTitle = ""
    data.allWordpressWpFilary.nodes.map(singleNode => {
      let activeFilaryStatus = false
      singleNode.acf.programy.map(singleProgram => {
        if (singleProgram.wordpress_id === currentProgramId) {
          activeFilaryStatus = true
        }
        return true
      })
      if (activeFilaryStatus) mainProgramTitle = singleNode.title
      return true
    })
    return mainProgramTitle
  }

  const getListOfPrograms = () => {
    let currentProgramId = data.wordpressWpProgram.wordpress_id
    let mainProgram = ""
    data.allWordpressWpFilary.nodes.map(singleNode => {
      let activeFilaryStatus = false
      singleNode.acf.programy.map(singleProgram => {
        if (singleProgram.wordpress_id === currentProgramId) {
          activeFilaryStatus = true
        }
        return true
      })
      if (activeFilaryStatus) mainProgram = singleNode.acf.programy
      return true
    })
    return mainProgram
  }

  return (
    <Layout>
      <SEO title="Home" keywords={[`gatsby`, `application`, `react`]} />
      <ProgramTitle
        mainProgramTitle={getMainProgramTitle()}
        text={data.wordpressWpProgram.title}
        mainTitle={true}
      />

      <p
        style={{
          textAlign: "center",
          color: "#303845",
          fontSize: "18px",
          marginBottom: "40px",
        }}
        dangerouslySetInnerHTML={{
          __html: data.wordpressWpProgram.acf.description,
        }}
      />

      <Container fluid={true}>
        <Row className="row-eq-height" style={{ justifyContent: "center" }}>
          <ProgramDescCard
            img={GoalIcon}
            title="Cel"
            content={data.wordpressWpProgram.acf.goal}
          />
          <ProgramDescCard
            img={DurationIcon}
            title="Czas trwania"
            content={data.wordpressWpProgram.acf.duration}
          />
          <ProgramDescCard
            img={BenefitsIcon}
            title="Korzyści"
            content={data.wordpressWpProgram.acf.benefits}
          />
          <ProgramDescCard
            img={ForWhomIcon}
            title="Dla kogo"
            content={data.wordpressWpProgram.acf.for_whom}
          />
          <ProgramDescCard
            img={GroupIcon}
            title="Grupa"
            content={data.wordpressWpProgram.acf.group}
          />
          <Col
            md={12}
            style={{
              display: "flex",
              justifyContent: "center",
              margin: "40px 0 25px",
            }}
          >
            <Link
              class="czaplaLink"
              to={`kontakt/?program=${data.wordpressWpProgram.slug}`}
            >
              <span>Napisz&nbsp;</span> do nas
            </Link>
          </Col>
        </Row>
      </Container>

      <Newsletter />

      <ProgramTitle
        mainProgramTitle="Inne szkolenia"
        text={data.wordpressWpProgram.title}
        mainTitle={false}
      />

      <ul className="programLinks singleProgramLinks">
        {getListOfPrograms().map((program, index) => {
          return program.wordpress_id !== data.wordpressWpProgram.wordpress_id ? (
            <li key={index}>
              <svg
                width="13"
                height="24"
                viewBox="0 0 13 24"
                fill="none"
                xmlns="http://www.w3.org/2000/svg"
              >
                <path
                  fill-rule="evenodd"
                  clip-rule="evenodd"
                  d="M6.21398 12.3221C6.29881 12.3221 6.21398 12.3221 6.21398 12.3221C8.58909 12.1611 10.2008 9.34228 13 7.97315C10.6249 12 9.60699 11.5168 6.38364 14.4966C3.41475 17.396 3.49958 20.7785 1.03965 24C0.869999 20.0537 2.73615 17.1544 1.80308 13.2081C0.954824 9.66443 -0.74168 7.16779 0.361048 2.97987C1.2093 5.31544 1.2093 9.98658 2.99063 11.4362C3.16028 11.5973 3.16028 11.5973 3.2451 11.4362C5.78986 8.69799 4.68713 4.10738 8.16496 0C8.75874 5.2349 5.36573 8.53691 6.04433 12.2416C6.12916 12.3221 6.12916 12.3221 6.21398 12.3221Z"
                  fill="#B49678"
                />
              </svg>
              <Link
                to={`/program/${program.post_name}`}
                dangerouslySetInnerHTML={{ __html: program.post_title }}
              />
            </li>
          ) : null
        })}
      </ul>
    </Layout>
  )
}

export default TemplateSingleProgram

export const query = graphql`
  query($id: Int!) {
    wordpressWpProgram(wordpress_id: { eq: $id }) {
      acf {
        group
        goal
        for_whom
        duration
        description
        benefits
      }
      title
      wordpress_id
      slug
    }
    allWordpressWpFilary {
      nodes {
        acf {
          programy {
            wordpress_id
            post_title
            post_name
          }
        }
        title
      }
    }
  }
`
