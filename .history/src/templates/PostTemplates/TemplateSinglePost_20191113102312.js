import React from "react"
import Layout from "../../components/layout"
import SEO from "../../components/seo"
import { Container, Row, Col, Carousel } from "react-bootstrap"
import { Link } from "gatsby"
import { StickyContainer, Sticky } from "react-sticky"
import { CopyToClipboard } from "react-copy-to-clipboard"

import Newsletter from "../../components/Newsletter/newsletter"
// import MostReadable from "../../components/MostReadable/mostReadable"
import Arrow from "../../components/ArrowComponent/arrowComponent"
import Title from "../../components/Title/Title"
import "./TemplateSinglePost.scss"

const TemplateSinglePost = ({ data }) => {
  const currentUrl = window.location.href

  return (
    <Layout>
      <SEO title="Home" keywords={[`gatsby`, `application`, `react`]} />
      <Container fluid={true} className="standardPadding singlePostWrapper">
        <Row>
          <Col lg={9}>
            <Carousel prevIcon={<Arrow />} nextIcon={<Arrow />}>
              {data.wordpressPost.acf.gallery.map((slide, index) => (
                <Carousel.Item key={index}>
                  <img
                    className=""
                    src={slide.localFile.childImageSharp.resolutions.src}
                    alt=""
                  />
                </Carousel.Item>
              ))}
            </Carousel>

            <StickyContainer>
              <Row
                style={{
                  marginTop: "100px",
                }}
              >
                <Col md={4}>
                  <Sticky>
                    {({
                      style,
                      isSticky,
                      wasSticky,
                      distanceFromTop,
                      distanceFromBottom,
                      calculatedHeight,
                    }) => (
                      <div className="stickyWrapper" style={style}>
                        {data.wordpressPost.tags.map((tags, index) => (
                          <a
                            href={`/tag/${tags.slug}`}
                            className="czaplaLink"
                            key={index}
                          >
                            {tags.name}
                          </a>
                        ))}

                        <div className="socialLinksWrapper">
                          <CopyToClipboard text={currentUrl}>
                            <a href=""></a>
                          </CopyToClipboard>

                          <a
                            href={`https://www.facebook.com/sharer/sharer.php?u=${currentUrl}`}
                          >
                            <svg
                              width="11"
                              height="23"
                              viewBox="0 0 11 23"
                              fill="none"
                              xmlns="http://www.w3.org/2000/svg"
                            >
                              <path
                                d="M2.5 23H7.10001V11.4H10.3L10.6 7.5H7C7 7.5 7 6 7 5.3C7 4.4 7.20001 4 8.10001 4C8.80001 4 10.6 4 10.6 4V0C10.6 0 7.99999 0 7.39999 0C3.99999 0 2.4 1.5 2.4 4.4C2.4 6.9 2.4 7.5 2.4 7.5H0V11.5H2.4V23H2.5Z"
                                fill="#B49678"
                              />
                            </svg>
                          </a>

                          <a
                            href={`https://www.linkedin.com/shareArticle?mini=true&url=${currentUrl}`}
                          >
                            <svg
                              width="19"
                              height="20"
                              viewBox="0 0 19 20"
                              fill="none"
                              xmlns="http://www.w3.org/2000/svg"
                            >
                              <path
                                d="M4.19981 6.30005H0.299805V19.0001H4.19981V6.30005Z"
                                fill="#B49678"
                              />
                              <path
                                d="M2.3 4.6C3.6 4.6 4.6 3.5 4.6 2.3C4.6 0.999995 3.6 0 2.3 0C0.999995 0 0 1.1 0 2.3C0 3.6 0.999995 4.6 2.3 4.6Z"
                                fill="#B49678"
                              />
                              <path
                                d="M10.5998 12.4C10.5998 10.6 11.3998 9.50002 12.9998 9.50002C14.3998 9.50002 15.0998 10.5 15.0998 12.4C15.0998 14.2 15.0998 19.1 15.0998 19.1H18.9998C18.9998 19.1 18.9998 14.5 18.9998 11C18.9998 7.60002 17.0998 5.90002 14.3998 5.90002C11.6998 5.90002 10.5998 8.00002 10.5998 8.00002V6.30002H6.7998V19H10.5998C10.5998 19 10.5998 14.3 10.5998 12.4Z"
                                fill="#B49678"
                              />
                            </svg>
                          </a>
                        </div>
                      </div>
                    )}
                  </Sticky>
                </Col>

                <Col md={8}>
                  <div
                    dangerouslySetInnerHTML={{
                      __html: data.wordpressPost.content,
                    }}
                  />
                </Col>
              </Row>
            </StickyContainer>
          </Col>

          {/* <MostReadable /> */}
        </Row>
      </Container>

      <Newsletter />
    </Layout>
  )
}

export default TemplateSinglePost

export const query = graphql`
  query($id: Int!) {
    wordpressPost(wordpress_id: { eq: $id }) {
      title
      content
      slug
      categories {
        name
        slug
      }
      tags {
        slug
        name
      }
      date(formatString: "d MMMM Y", locale: "pl")
      acf {
        gallery {
          localFile {
            childImageSharp {
              resolutions(
                width: 1200
                height: 560
                cropFocus: CENTER
                quality: 90
              ) {
                src
              }
            }
          }
        }
      }
    }
  }
`
