import React, { useEffect } from "react"
import Layout from "../../components/layout"
import SEO from "../../components/seo"
import axios from "axios"
import moment from 'moment'
import 'moment/locale/pl'
import { Container, Row, Col, Carousel } from "react-bootstrap"
import { StickyContainer, Sticky } from "react-sticky"
import { CopyToClipboard } from "react-copy-to-clipboard"

import Newsletter from "../../components/Newsletter/newsletter"
// import MostReadable from "../../components/MostReadable/mostReadable"
import PostCard from "../../components/PostCard/postCard"
import Arrow from "../../components/ArrowComponent/arrowComponent"
import "./TemplateSinglePost.scss"

const TemplateSinglePost = ({ data }) => {
  let currentUrl = ""

  useEffect(() => {
    currentUrl = window.location.href
  })

  useEffect(() => {
    axios({
      method: "POST",
      url:
        "https://czaplaandmore.pixelart.pl/wp-json/postViewIncrementing/v2",
      data: {
        post_id: data.wordpressPost.wordpress_id
      },
    }).then(response => {
      console.log(response)
    })
  })

  return (
    <Layout>
      <SEO title="Home" keywords={[`gatsby`, `application`, `react`]} />
      <Container fluid={true} className="standardPadding singlePostWrapper">
        <Row>
          <Col lg={9}>
            <Carousel prevIcon={<Arrow />} nextIcon={<Arrow />}>
              {data.wordpressPost.acf.gallery.map((slide, index) => (
                <Carousel.Item key={index}>
                  <img
                    className=""
                    src={slide.localFile.childImageSharp.resolutions.src}
                    alt=""
                  />
                </Carousel.Item>
              ))}
            </Carousel>

            <StickyContainer>
              <Row
                style={{
                  marginTop: "100px",
                }}
              >
                <Col md={4}>
                  <Sticky disableHardwareAcceleration>
                    {({ style }) => (
                      <div className="stickyWrapper" style={style}>
                        {data.wordpressPost.tags.map((tags, index) => (
                          <a
                            href={`/tag/${tags.slug}`}
                            className="czaplaLink"
                            key={index}
                          >
                            {tags.name}
                          </a>
                        ))}

                        <div className="socialLinksWrapper">
                          <CopyToClipboard text={currentUrl}>
                            <span className="socialLink">
                              <svg
                                width="22"
                                height="22"
                                viewBox="0 0 22 22"
                                fill="none"
                                xmlns="http://www.w3.org/2000/svg"
                              >
                                <path
                                  d="M13.4229 8.5773C13.1608 8.3152 12.8729 8.08318 12.5593 7.88553C11.7343 7.35703 10.7718 7.07345 9.79218 7.07345C8.43013 7.06915 7.12393 7.61054 6.16147 8.5773L1.49955 13.2435C0.541385 14.206 0 15.5079 0 16.8656C0 19.7015 2.29444 22.0002 5.13026 22.0002C6.48802 22.0045 7.79422 21.4674 8.75668 20.5049L12.6065 16.6551C12.6753 16.5863 12.7139 16.4918 12.7139 16.393C12.7139 16.1911 12.5464 16.0278 12.3444 16.0278H12.1983C11.3949 16.0321 10.5957 15.8774 9.85234 15.5723C9.71484 15.5165 9.55586 15.5465 9.45274 15.654L6.68567 18.4253C5.82633 19.2847 4.4342 19.2847 3.57486 18.4253C2.71552 17.566 2.71552 16.1739 3.57486 15.3145L8.25826 10.6354C9.1176 9.77608 10.5097 9.77608 11.3691 10.6354C11.9491 11.1811 12.8514 11.1811 13.4315 10.6354C13.6807 10.3862 13.8311 10.0554 13.8569 9.70303C13.8741 9.28625 13.7194 8.87377 13.4229 8.5773Z"
                                  fill="#B49678"
                                />
                                <path
                                  d="M20.4953 1.50492C18.4888 -0.50164 15.2405 -0.50164 13.2339 1.50492L9.38837 5.34617C9.28095 5.45359 9.25088 5.61257 9.31103 5.75006C9.37118 5.88756 9.50438 5.97779 9.65477 5.97349H9.79226C10.5957 5.97349 11.3906 6.12817 12.134 6.43324C12.2715 6.48909 12.4304 6.45902 12.5336 6.3516L15.2963 3.59312C16.1557 2.73378 17.5478 2.73378 18.4071 3.59312C19.2665 4.45246 19.2665 5.84459 18.4071 6.70393L14.9698 10.1413L14.9397 10.1757L13.7366 11.3701C12.8773 12.2295 11.4852 12.2295 10.6258 11.3701C10.0458 10.8245 9.14346 10.8245 8.5634 11.3701C8.3142 11.6194 8.15951 11.9545 8.13803 12.3068C8.11225 12.7279 8.26693 13.1361 8.5634 13.4369C8.98878 13.8622 9.4829 14.2103 10.0286 14.4638C10.1059 14.5024 10.1833 14.5282 10.2606 14.5626C10.3379 14.597 10.4196 14.6228 10.4969 14.6485C10.5743 14.6786 10.6559 14.7044 10.7332 14.7259L10.9481 14.786C11.0942 14.8247 11.2402 14.8505 11.3906 14.8763C11.5711 14.902 11.7559 14.9192 11.9363 14.9278H12.1941H12.2156L12.4347 14.902C12.5164 14.8977 12.598 14.8806 12.6968 14.8806H12.8214L13.0749 14.8419L13.191 14.8204L13.4015 14.7774H13.4402C14.3425 14.5497 15.1631 14.0857 15.8205 13.4283L20.4953 8.76635C22.5019 6.75979 22.5019 3.50718 20.4953 1.50492Z"
                                  fill="#B49678"
                                />
                              </svg>
                            </span>
                          </CopyToClipboard>

                          <a
                            href={`https://www.facebook.com/sharer/sharer.php?u=${currentUrl}`}
                            className="socialLink"
                          >
                            <svg
                              width="11"
                              height="23"
                              viewBox="0 0 11 23"
                              fill="none"
                              xmlns="http://www.w3.org/2000/svg"
                            >
                              <path
                                d="M2.5 23H7.10001V11.4H10.3L10.6 7.5H7C7 7.5 7 6 7 5.3C7 4.4 7.20001 4 8.10001 4C8.80001 4 10.6 4 10.6 4V0C10.6 0 7.99999 0 7.39999 0C3.99999 0 2.4 1.5 2.4 4.4C2.4 6.9 2.4 7.5 2.4 7.5H0V11.5H2.4V23H2.5Z"
                                fill="#B49678"
                              />
                            </svg>
                          </a>

                          <a
                            href={`https://www.linkedin.com/shareArticle?mini=true&url=${currentUrl}`}
                            className="socialLink"
                          >
                            <svg
                              width="19"
                              height="20"
                              viewBox="0 0 19 20"
                              fill="none"
                              xmlns="http://www.w3.org/2000/svg"
                              style={{
                                marginLeft: "1px",
                              }}
                            >
                              <path
                                d="M4.19981 6.30005H0.299805V19.0001H4.19981V6.30005Z"
                                fill="#B49678"
                              />
                              <path
                                d="M2.3 4.6C3.6 4.6 4.6 3.5 4.6 2.3C4.6 0.999995 3.6 0 2.3 0C0.999995 0 0 1.1 0 2.3C0 3.6 0.999995 4.6 2.3 4.6Z"
                                fill="#B49678"
                              />
                              <path
                                d="M10.5998 12.4C10.5998 10.6 11.3998 9.50002 12.9998 9.50002C14.3998 9.50002 15.0998 10.5 15.0998 12.4C15.0998 14.2 15.0998 19.1 15.0998 19.1H18.9998C18.9998 19.1 18.9998 14.5 18.9998 11C18.9998 7.60002 17.0998 5.90002 14.3998 5.90002C11.6998 5.90002 10.5998 8.00002 10.5998 8.00002V6.30002H6.7998V19H10.5998C10.5998 19 10.5998 14.3 10.5998 12.4Z"
                                fill="#B49678"
                              />
                            </svg>
                          </a>
                        </div>
                      </div>
                    )}
                  </Sticky>
                </Col>

                <Col md={8}>
                  <h1
                    dangerouslySetInnerHTML={{
                      __html: data.wordpressPost.title,
                    }}
                  />

                  <div className="postDesc">
                    {moment(data.wordpressPost.date).locale('pl').format('LL')}&nbsp;|&nbsp;
                    {data.wordpressPost.categories.map((category, index) => (
                      <a href={`/${category.slug}`} key={index}>
                        {category.name}
                      </a>
                    ))}
                  </div>

                  <div
                    className="postContent"
                    dangerouslySetInnerHTML={{
                      __html: data.wordpressPost.content,
                    }}
                  />
                </Col>
              </Row>
            </StickyContainer>
          </Col>

          {/* <MostReadable /> */}
        </Row>
      </Container>

      <Newsletter />

      <Container fluid={true}>
        <Row>
          {data.allWordpressPost.nodes.map((post, index) => (
            <PostCard 
              col="3"
              title={post.title}
              date={post.date}
              categories={post.categories}
              tags={post.tags}
              content={post.content}
              slug={post.slug}
            />
          ))}
        </Row>
      </Container>
    </Layout>
  )
}

export default TemplateSinglePost

export const query = graphql`
  query($id: Int!, $category: String!) {
    wordpressPost(wordpress_id: { eq: $id }) {
      title
      content
      slug
      categories {
        name
        slug
      }
      tags {
        slug
        name
      }
      date(locale: "pl")
      wordpress_id
      acf {
        gallery {
          localFile {
            childImageSharp {
              resolutions(
                width: 1200
                height: 560
                cropFocus: CENTER
                quality: 90
              ) {
                src
              }
            }
          }
        }
      }
    }
    allWordpressPost(filter: {categories: {elemMatch: {name: {eq: $category}}}}, , limit: 4, sort: {fields: date, order: ASC}) {
      nodes {
        title
        content
        slug
        categories {
          name
          slug
        }
        tags {
          slug
          name
        }
        date(locale: "pl")
        acf {
          gallery {
            localFile {
              childImageSharp {
                resolutions(width: 400, height: 240, cropFocus: CENTER, quality: 90) {
                  src
                }
              }
            }
          }
        }
      }
    }
  }
`
