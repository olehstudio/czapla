import React from "react"
import Layout from "../../components/layout"
import SEO from "../../components/seo"
import { Container, Row, Col } from "react-bootstrap"

// import ContactForm from "../../components/ContactForm/contactForm"
import Title from "../../components/Title/title"
import "./templateContact.scss"

const TemplateContact = ({ data }) => {
  const settings = data.allWordpressAcfOptions.nodes[0].websiteSettings
  return (
    <Layout>
      <SEO title="Home" keywords={[`gatsby`, `application`, `react`]} />

      <img
        style={{ width: "100%" }}
        src={
          data.wordpressPage.acf.header_image.localFile.childImageSharp
            .resolutions.src
        }
        alt={data.wordpressPage.title}
      />

      <Container className="contactPage">
        <Row>
          <Col md={12}>
            <Title text={data.wordpressPage.title} mainTitle={true} />
          </Col>

          {/* <ContactForm /> */}

          <Col lg={3} className="contactPage__contactInfo">
            <h2>Dane kontaktowe</h2>
            <div
              dangerouslySetInnerHTML={{ __html: settings.footer_content }}
            />

            <div>
              <a
                href={`tel:${settings.footer_telefon}`}
                className="contactPage__contactInfo_tel"
              >
                {settings.footer_telefon}
              </a>
              <a
                href={`mailto:${settings.footer_email}`}
                className="contactPage__contactInfo_email"
              >
                {settings.footer_email}
              </a>
            </div>

            <div dangerouslySetInnerHTML={{ __html: settings.company_info }} />

            <h2>Dokumenty</h2>
            <div className="contactPage__contactInfo_documents">
              {settings.documents.map((document, index) => (
                <a href={document.document_file.source_url} targte="_blank">
                  {document.document_name}
                </a>
              ))}
            </div>
          </Col>
        </Row>
      </Container>
    </Layout>
  )
}

export default TemplateContact

export const query = graphql`
  query($id: Int!) {
    wordpressPage(wordpress_id: { eq: $id }) {
      wordpress_id
      title
      acf {
        header_image {
          localFile {
            childImageSharp {
              resolutions(
                width: 1920
                height: 640
                cropFocus: CENTER
                quality: 90
              ) {
                src
              }
            }
          }
        }
      }
    }
    allWordpressAcfOptions {
      nodes {
        websiteSettings {
          footer_content
          footer_email
          footer_telefon
          company_info
          facebook_link
          youtube_link
          linkedin_link
          documents {
            document_file {
              source_url
            }
            document_name
          }
        }
      }
    }
  }
`
