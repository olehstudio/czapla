import React from "react"
import { Link } from "gatsby"
import Layout from "../../components/layout"
import SEO from "../../components/seo"
import { Container, Row, Col } from "react-bootstrap"

import PostArrow from "../../images/post_arrow.svg"

import Title from "../../components/Title/title"
import Newsletter from "../../components/Newsletter/newsletter"
import MostReadable from "../../components/MostReadable/mostReadable"
import PostCard from "../../components/PostCard/postCard"
import "./TemplateCategory.scss"

const TemplateCategory = ({ data, pageContext }) => {
  const { currentPage, numPages } = pageContext
  const isFirst = currentPage === 1
  const isLast = currentPage === numPages
  const prevPage =
    currentPage - 1 === 1 ? "/" : "page-" + (currentPage - 1).toString()
  const nextPage = "page-" + (currentPage + 1).toString()

  return (
    <Layout>
      <SEO title="Home" keywords={[`gatsby`, `application`, `react`]} />

      <img
        style={{ width: "100%" }}
        src={
          data.wordpressPage.acf.header_image.localFile.childImageSharp
            .resolutions.src
        }
        alt={data.wordpressPage.title}
      />

      <Container fluid={true} className="standardPadding">
        <Row>
          <Col md={12}>
            <Title text={data.wordpressPage.title} mainTitle={true} />
          </Col>

          {data.allWordpressPost.nodes.map((post, index) => (
            <PostCard
              image={
                post.acf.gallery[0].localFile.childImageSharp.resolutions.src
              }
              title={post.title}
              date={post.date}
              categories={post.categories}
              tags={post.tags}
              content={post.content}
              slug={post.slug}
              countStatus={
                index === data.allWordpressPost.nodes.length - 1
                  ? "last"
                  : "notLast"
              }
            />
          ))}

          <Col md={12} className="postPagination">
            {!isFirst && (
              <Link
                className="prevPage"
                to={`/${data.allWordpressPost.nodes[0].categories[0].slug}/${prevPage}`}
                rel="prev"
              >
                <img src={`${PostArrow}`} alt="" />
              </Link>
            )}

            {numPages < 5
              ? Array.from({ length: numPages }, (_, i) => (
                  <Link
                    key={`pagination-number${i + 1}`}
                    to={`/${
                      data.allWordpressPost.nodes[0].categories[0].slug
                    }/${i === 0 ? "" : "page-" + (i + 1)}`}
                    className={`paginationLink ${currentPage === i + 1 &&
                      "currentPage"}`}
                  >
                    {i + 1}
                  </Link>
                ))
              : Array.from({ length: numPages }, (_, i) => {
                  switch (true) {
                    case i === 0:
                      return (
                        <Link
                          key={`pagination-number${i + 1}`}
                          to={`/${
                            data.allWordpressPost.nodes[0].categories[0].slug
                          }/${i === 0 ? "" : "page-" + (i + 1)}`}
                          className={`paginationLink ${currentPage === i + 1 &&
                            "currentPage"}`}
                        >
                          {i + 1}
                        </Link>
                      )
                    case i === currentPage:
                      return (
                        <Link
                          key={`pagination-number${i + 1}`}
                          to={`/${
                            data.allWordpressPost.nodes[0].categories[0].slug
                          }/${i === 0 ? "" : "page-" + (i + 1)}`}
                          className={`paginationLink`}
                        >
                          {i + 1}
                        </Link>
                      )
                    case i + 1 === currentPage:
                      return (
                        <Link
                          key={`pagination-number${i + 1}`}
                          to={`/${
                            data.allWordpressPost.nodes[0].categories[0].slug
                          }/${i === 0 ? "" : "page-" + (i + 1)}`}
                          className={`paginationLink ${currentPage === i + 1 &&
                            "currentPage"}`}
                        >
                          {i + 1}
                        </Link>
                      )
                    case i + 2 === currentPage:
                      return (
                        <Link
                          key={`pagination-number${i + 1}`}
                          to={`/${
                            data.allWordpressPost.nodes[0].categories[0].slug
                          }/${i === 0 ? "" : "page-" + (i + 1)}`}
                          className={`paginationLink`}
                        >
                          {i + 1}
                        </Link>
                      )
                    case i - 1 === numPages:
                      return (
                        <Link
                          key={`pagination-number${i + 1}`}
                          to={`/${
                            data.allWordpressPost.nodes[0].categories[0].slug
                          }/${i === 0 ? "" : "page-" + (i + 1)}`}
                          className={`paginationLink`}
                        >
                          {i + 1}
                        </Link>
                      )
                  }
                })}

            {!isLast && (
              <Link
                className="nextPage"
                to={`/${data.allWordpressPost.nodes[0].categories[0].slug}/${nextPage}`}
                rel="next"
              >
                <img src={`${PostArrow}`} alt="" />
              </Link>
            )}
          </Col>
        </Row>
      </Container>

      <Newsletter />

      <Container fluid={true} className="standardPadding">
        <Row>
          <MostReadable direction="row" />
        </Row>
      </Container>
    </Layout>
  )
}

export default TemplateCategory

export const query = graphql`
  query($category: String!, $limit: Int!, $skip: Int!) {
    wordpressPage(title: { eq: $category }) {
      title
      wordpress_id
      acf {
        header_image {
          localFile {
            childImageSharp {
              resolutions(
                width: 1920
                height: 620
                cropFocus: CENTER
                quality: 90
              ) {
                src
              }
            }
          }
        }
      }
    }
    allWordpressPost(
      sort: { fields: date, order: ASC }
      filter: { categories: { elemMatch: { name: { eq: $category } } } }
      limit: $limit
      skip: $skip
    ) {
      nodes {
        title
        content
        slug
        categories {
          name
          slug
        }
        tags {
          slug
          name
        }
        date(locale: "pl")
        acf {
          gallery {
            localFile {
              childImageSharp {
                resolutions(
                  width: 860
                  height: 500
                  cropFocus: CENTER
                  quality: 90
                ) {
                  src
                }
              }
            }
          }
        }
      }
    }
  }
`
