import React from "react"
import Layout from "../../components/layout"
import SEO from "../../components/seo"
import { Container, Row, Col } from "react-bootstrap"

import Title from "../../components/Title/title"
import Newsletter from "../../components/Newsletter/newsletter"
import MostReadable from "../../components/MostReadable/mostReadable"
import "./TemplateSinglePost.scss"

const TemplateSinglePost = ({ data }) => {
  let currentUrl = ""

  useEffect(() => {
    currentUrl = window.location.href
  })

  useEffect(() => {
    axios({
      method: "POST",
      url: "https://czaplaandmore.pixelart.pl/wp-json/postViewIncrementing/v2",
      data: {
        post_id: data.wordpressPost.wordpress_id,
      },
    }).then(response => {
      console.log(response)
    })
  })

  return (
    <Layout>
      <SEO title="Home" keywords={[`gatsby`, `application`, `react`]} />
      
      <Container fluid={true} className="standardPadding">
          <Row>
              <Col md={12}></Col>
          </Row>
      </Container>

      <Newsletter />

      <Container fluid={true} className="standardPadding">
        <Row>
          <Col md={12}>
            <Title text="Inne artykuły w tej kategorii" mainTitle={false} />
          </Col>

          <MostReadable />
        </Row>
      </Container>
    </Layout>
  )
}

export default TemplateSinglePost

export const query = graphql`
  query($id: Int!, $category: String!) {
    wordpressPost(wordpress_id: { eq: $id }) {
      title
      content
      slug
      categories {
        name
        slug
      }
      tags {
        name
        slug
      }
      date(locale: "pl")
      wordpress_id
      acf {
        gallery {
          localFile {
            childImageSharp {
              resolutions(
                width: 1200
                height: 560
                cropFocus: CENTER
                quality: 90
              ) {
                src
              }
            }
          }
        }
      }
    }
    allWordpressPost(
      filter: { categories: { elemMatch: { name: { eq: $category } } } }
      limit: 4
      sort: { fields: date, order: ASC }
    ) {
      nodes {
        title
        content
        slug
        categories {
          name
          slug
        }
        tags {
          slug
          name
        }
        date(locale: "pl")
        acf {
          gallery {
            localFile {
              childImageSharp {
                resolutions(
                  width: 400
                  height: 240
                  cropFocus: CENTER
                  quality: 90
                ) {
                  src
                }
              }
            }
          }
        }
      }
    }
  }
`
