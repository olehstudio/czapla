import React from "react"
import Layout from "../../components/layout"
import SEO from "../../components/seo"
import { Link } from "gatsby"
import { Container, Row, Col } from "react-bootstrap"

import Title from "../../components/Title/title"
import Logo from "../../images/logo_footer.svg"
// import "./programsSection.scss"

const TemplateProducts = ({ data }) => {
  const wrapFirstLetterInSpan = (string, br = false) => {
    let tempArr = string.split(" ")
    let brOutput = br ? "<br/>" : ""
    tempArr[0] = "<span>" + tempArr[0] + "</span>" + brOutput
    return tempArr.join(" ")
  }

  const excerptContent = text => {
    return text.substr(0, 800).concat("...")
  }

  return (
    <Layout>
      <SEO title="Home" keywords={[`gatsby`, `application`, `react`]} />

      <section className="projectsSection">
        <Container fluid={true}>
          <Row>
            <Col md={12}>
              <Title text={data.wordpressPage.title} mainTitle={true} />
            </Col>

            <Col md={12}>
              <div
                className="projectsSection__desc"
                dangerouslySetInnerHTML={{ __html: data.wordpressPage.content }}
              />
            </Col>
          </Row>

          {data.allWordpressWpProductCzapla.nodes.map((node, index) => (
            <Row key={index} className="projectsSection__projectRow">
              <Col md={6} className="projectsSection__imageCol">
                <img
                  src={`${node.featured_media.localFile.childImageSharp.resolutions.src}`}
                  alt={`${node.title}`}
                />

                <div className="innerCol">
                  <img src={`${Logo}`} alt="" />
                  <h4
                    dangerouslySetInnerHTML={{
                      __html: wrapFirstLetterInSpan(node.title, true),
                    }}
                  />
                </div>
              </Col>

              <Col md={6} className="projectsSection__contentCol">
                <h3
                  dangerouslySetInnerHTML={{
                    __html: wrapFirstLetterInSpan(node.title, false),
                  }}
                />
                <div dangerouslySetInnerHTML={{ __html: excerptContent(node.content) }}></div>

                <Link to={`/produkty/${node.slug}`} className="czaplaLink">Więcej</Link>
              </Col>
            </Row>
          ))}
        </Container>
      </section>
    </Layout>
  )
}

export default TemplateProducts

export const query = graphql`
  query($id: Int!) {
    wordpressPage(wordpress_id: { eq: $id }) {
      wordpress_id
      title
      content
    }
    allWordpressWpProductCzapla {
      nodes {
        title
        content
        slug
        featured_media {
          localFile {
            childImageSharp {
              resolutions(width: 1280, cropFocus: CENTER, quality: 100) {
                src
              }
            }
          }
        }
      }
    }
  }
`
