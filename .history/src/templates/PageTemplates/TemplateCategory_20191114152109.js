import React from "react"
import Layout from "../../components/layout"
import SEO from "../../components/seo"
import { Container, Row, Col } from "react-bootstrap"

import Title from "../../components/Title/title"
import Newsletter from "../../components/Newsletter/newsletter"
import MostReadable from "../../components/MostReadable/mostReadable"
import "./TemplateSinglePost.scss"

const TemplateCategory = ({ data }) => {
  return (
    <Layout>
      <SEO title="Home" keywords={[`gatsby`, `application`, `react`]} />

      <Container fluid={true} className="standardPadding">
        <Row>
          <Col md={12}>
              <Title text={data.wordpressPost.title} mainTitle={true} />
          </Col>
        </Row>
      </Container>

      <Newsletter />

      <Container fluid={true} className="standardPadding">
        <Row>
          <Col md={12}>
            <Title text="Inne artykuły w tej kategorii" mainTitle={false} />
          </Col>

          {data.allWordpressPost.nodes.map((post, index) => (
              <h1>{post.title}</h1>
          ))}

          <MostReadable />
        </Row>
      </Container>
    </Layout>
  )
}

export default TemplateCategory

export const query = graphql`
  query($id: Int!, $category: String!, $limit: Int!, $skip: Int!) {
    wordpressPost(wordpress_id: { eq: $id }) {
      title
      wordpress_id
    }
    allWordpressPost(
      sort: { fields: date, order: ASC }
      filter: { categories: { elemMatch: { name: { eq: $category } } } }
      limit: $limit
      skip: $skip
    ) {
      nodes {
        title
        content
        slug
        categories {
          name
          slug
        }
        tags {
          slug
          name
        }
        date(locale: "pl")
        acf {
          gallery {
            localFile {
              childImageSharp {
                resolutions(
                  width: 400
                  height: 240
                  cropFocus: CENTER
                  quality: 90
                ) {
                  src
                }
              }
            }
          }
        }
      }
    }
  }
`
