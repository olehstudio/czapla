import React from "react"
import Layout from "../../components/layout"
import SEO from "../../components/seo"
import { Container, Row, Col } from "react-bootstrap"

import SmallPostCard from "../../components/SmallPostCard/smallPostCard"
import TitleLogo from "../../images/title_logo.svg"
import "./TemplateTag.scss"

const TemplateCategory = ({ data }) => {
  return (
    <Layout>
      <SEO title="Home" keywords={[`gatsby`, `application`, `react`]} />

      <Container fluid={true} className="standardPadding tagPage">
        <Row>
          <Col md={12}>
            <img src={`${TitleLogo}`} alt="Czapla Logo" />
            <h1>
              Strona <span>Tagu</span>
            </h1>
            <h2 dangerouslySetInnerHTML={{ __html: data.wordpressTag.title }} />
          </Col>

          {data.allWordpressPost.nodes.map((post, index) => (
            <SmallPostCard
              key={index}
              col="3"
              image={
                post.acf.gallery[0].localFile.childImageSharp.resolutions.src
              }
              title={post.title}
              date={post.date}
              categories={post.categories}
              tags={post.tags}
              content={post.content}
              slug={post.slug}
            />
          ))}
        </Row>
      </Container>
    </Layout>
  )
}

export default TemplateCategory

export const query = graphql`
  query($id: Int!, $name: String!) {
    wordpressTag(wordpress_id: { eq: $id }) {
      name
      wordpress_id
    }
    allWordpressPost(filter: { tags: { elemMatch: { name: { eq: $name } } } }) {
      nodes {
        title
        content
        slug
        categories {
          name
          slug
        }
        tags {
          slug
          name
        }
        date(locale: "pl")
        acf {
          gallery {
            localFile {
              childImageSharp {
                resolutions(
                  width: 860
                  height: 500
                  cropFocus: CENTER
                  quality: 90
                ) {
                  src
                }
              }
            }
          }
        }
      }
    }
  }
`
