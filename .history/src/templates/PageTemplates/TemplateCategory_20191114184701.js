import React from "react"
import { Link } from "gatsby"
import Layout from "../../components/layout"
import SEO from "../../components/seo"
import { Container, Row, Col } from "react-bootstrap"

import Title from "../../components/Title/title"
import Newsletter from "../../components/Newsletter/newsletter"
import MostReadable from "../../components/MostReadable/mostReadable"
import PostCard from "../../components/PostCard/postCard"
// import "./TemplateSinglePost.scss"

const TemplateCategory = ({ data, pageContext }) => {
  const { currentPage, numPages } = pageContext
  const isFirst = currentPage === 1
  const isLast = currentPage === numPages
  const prevPage = currentPage - 1 === 1 ? "/" : (currentPage - 1).toString()
  const nextPage = (currentPage + 1).toString()

  return (
    <Layout>
      <SEO title="Home" keywords={[`gatsby`, `application`, `react`]} />

      <img
        style={{ width: "100%" }}
        src={
          data.wordpressPage.acf.header_image.localFile.childImageSharp
            .resolutions.src
        }
        alt={data.wordpressPage.title}
      />

      <Container fluid={true} className="standardPadding">
        <Row>
          <Col md={12}>
            <Title text={data.wordpressPage.title} mainTitle={true} />
          </Col>

          {data.allWordpressPost.nodes.map((post, index) => (
            <PostCard
              image={
                post.acf.gallery[0].localFile.childImageSharp.resolutions.src
              }
              title={post.title}
              date={post.date}
              categories={post.categories}
              tags={post.tags}
              content={post.content}
              slug={post.slug}
              countStatus={
                index === data.allWordpressPost.nodes.length - 1
                  ? "last"
                  : "notLast"
              }
            />
          ))}

          <Col md={12} className="postPagination">
            {!isFirst && (
              <Link to={prevPage} rel="prev">
                ← Previous Page
              </Link>
            )}
            {Array.from({ length: numPages }, (_, i) => (
              <Link
                key={`pagination-number${i + 1}`}
                to={`/${data.allWordpressPost.nodes[0].categories[0].slug}/${i === 0 ? "" :  "page-" + i + 1}`}
              >
                {i + 1}
              </Link>
            ))}
            {!isLast && (
              <Link to={nextPage} rel="next">
                Next Page →
              </Link>
            )}
          </Col>
        </Row>
      </Container>

      <Newsletter />

      <Container fluid={true} className="standardPadding">
        <Row>
          <MostReadable direction="row" />
        </Row>
      </Container>
    </Layout>
  )
}

export default TemplateCategory

export const query = graphql`
  query($category: String!, $limit: Int!, $skip: Int!) {
    wordpressPage(title: { eq: $category }) {
      title
      wordpress_id
      acf {
        header_image {
          localFile {
            childImageSharp {
              resolutions(
                width: 1920
                height: 620
                cropFocus: CENTER
                quality: 90
              ) {
                src
              }
            }
          }
        }
      }
    }
    allWordpressPost(
      sort: { fields: date, order: ASC }
      filter: { categories: { elemMatch: { name: { eq: $category } } } }
      limit: $limit
      skip: $skip
    ) {
      nodes {
        title
        content
        slug
        categories {
          name
          slug
        }
        tags {
          slug
          name
        }
        date(locale: "pl")
        acf {
          gallery {
            localFile {
              childImageSharp {
                resolutions(
                  width: 860
                  height: 500
                  cropFocus: CENTER
                  quality: 90
                ) {
                  src
                }
              }
            }
          }
        }
      }
    }
  }
`
