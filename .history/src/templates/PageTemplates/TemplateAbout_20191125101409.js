import React from "react"
import Layout from "../../components/layout"
import SEO from "../../components/seo"
import { Container, Row, Col } from "react-bootstrap"

import Title from "../../components/Title/title"
import LinkedinIcon from "../../images/linkedin.svg"
import "./TemplateAbout.scss"

const TemplateAbout = ({ data }) => (
  <Layout>
    <SEO title="Home" keywords={[`gatsby`, `application`, `react`]} />

    <Container>
      <Row>
        <Col md={12}>
          <Title text={data.wordpressPage.acf.about_title} mainTitle={true} />
        </Col>

        <Col md={6} lg={{ span: 4, offset: 2 }}>
          {data.wordpressPage.acf.about_gallery.map((image, index) => (
            <Img
              key={index}
              className=""
              fluid={image.localFile.childImageSharp.fluid}
              alt={image.alt_text}
              style={{
                width: "100%",
                borderRadius: "7px",
                marginBottom: "40px",
              }}
            />
          ))}
        </Col>
        <Col md={6} lg={{ span: 4, offset: 0 }}>
          <div
            className="contentText"
            dangerouslySetInnerHTML={{
              __html: data.wordpressPage.acf.about_content,
            }}
          />
        </Col>
      </Row>
    </Container>

    <Container fluid={true} className="standardPadding">
      <Row>
        <Col md={12}>
          <Title text={data.wordpressPage.acf.work_title} mainTitle={false} />
        </Col>

        <Col md={6}>
          <Img
            className=""
            fluid={
              data.wordpressPage.acf.work_image.localFile.childImageSharp.fluid
            }
            alt={data.wordpressPage.acf.work_image.alt_text}
            style={{ width: "100%", borderRadius: "10px" }}
          />
        </Col>
        <Col md={6} lg={{ span: 3, offset: 0 }}>
          <div
            className="contentText"
            dangerouslySetInnerHTML={{
              __html: data.wordpressPage.acf.work_content,
            }}
          />
        </Col>
      </Row>
    </Container>

    <Container fluid={true} className="standardPadding">
      <Row>
        <Col md={12}>
          <Title text={data.wordpressPage.acf.team_title} mainTitle={false} />
        </Col>

        {data.allWordpressWpMember.nodes.map((member, index) => (
          <Col key={index} md={6} lg={6} xl={3} className="singleMember">
            <div className="singleMember__imageWrapper">
              <Img
                className=""
                fluid={member.featured_media.localFile.childImageSharp.fluid}
                alt={member.title}
              />

              <div className="overlay">
                <h5 dangerouslySetInnerHTML={{ __html: member.title }} />

                <ul>
                  {member.acf.positions_repeater.map((position, index) => (
                    <li key={index}>
                      {index !== 0 ? " |" : null}
                      {position.single_position}
                    </li>
                  ))}
                </ul>

                <div
                  className="content"
                  dangerouslySetInnerHTML={{ __html: member.acf.description }}
                />

                <a
                  href={member.acf.linkedin_link}
                  target="_blank"
                  rel="noopener noreferrer"
                  className="socialLink"
                >
                  <img src={`${LinkedinIcon}`} alt="LinkedIn Social Link" />
                </a>
              </div>
            </div>
            <h4 dangerouslySetInnerHTML={{ __html: member.title }} />
          </Col>
        ))}
      </Row>
    </Container>
  </Layout>
)

export default TemplateAbout

export const query = graphql`
  query($id: Int!) {
    wordpressPage(wordpress_id: { eq: $id }) {
      wordpress_id
      title
      acf {
        about_gallery {
          alt_text
          localFile {
            childImageSharp {
              fluid(quality: 90, maxWidth: 920, cropFocus: CENTER) {
                ...GatsbyImageSharpFluid_withWebp
              }
            }
          }
        }
        about_content
        about_title
        work_title
        work_content
        work_image {
          alt_text
          localFile {
            childImageSharp {
              fluid(
                quality: 90
                maxWidth: 920
                maxHeight: 640
                cropFocus: CENTER
              ) {
                ...GatsbyImageSharpFluid_withWebp
              }
            }
          }
        }
        team_title
      }
    }
    allWordpressWpMember {
      nodes {
        title
        featured_media {
          localFile {
            childImageSharp {
              fluid(
                quality: 90
                maxWidth: 720
                maxHeight: 720
                cropFocus: CENTER
              ) {
                ...GatsbyImageSharpFluid_withWebp
              }
            }
          }
        }
        acf {
          linkedin_link
          description
          positions_repeater {
            single_position
          }
        }
      }
    }
  }
`
