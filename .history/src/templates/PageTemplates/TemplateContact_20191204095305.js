import React from "react"
import Layout from "../../components/layout"
import SEO from "../../components/seo"
import { Container, Row, Col } from "react-bootstrap"

import ContactForm from "../../components/ContactForm/contactForm"
import Title from "../../components/Title/title"
import FacebookIcon from "../../images/social_fb.png"
import YoutubeIcon from "../../images/social_yt.png"
import LinkedinIcon from "../../images/social_li.png"
import "./templateContact.scss"

const TemplateContact = ({ data }) => {
  const settings = data.allWordpressAcfOptions.nodes[0].websiteSettings
  return (
    <Layout>
      <SEO title="Home" keywords={[`gatsby`, `application`, `react`]} />

      <Container fluid={true} className="contactPage">
        <Row>
          <Col md={12}>
            <Title text={data.wordpressPage.title} mainTitle={true} />
          </Col>

          <ContactForm />

          <Col lg={2} className="contactPage__contactInfo">
            <h2>Dane kontaktowe</h2>
            <div
              className="contactPage__contactInfo_content"
              dangerouslySetInnerHTML={{ __html: settings.footer_content }}
            />

            <div style={{ marginBottom: "20px" }}>
              <a
                href={`tel:${settings.footer_telefon}`}
                className="contactPage__contactInfo_tel"
              >
                {settings.footer_telefon}
              </a>
              <a
                href={`mailto:${settings.footer_email}`}
                className="contactPage__contactInfo_email"
              >
                {settings.footer_email}
              </a>
            </div>

            <div
              className="contactPage__contactInfo_companyInfo"
              dangerouslySetInnerHTML={{ __html: settings.company_info }}
            />

            <div className="socialIcons">
              <a href={settings.facebook_link} taget="_blank">
                <img src={`${FacebookIcon}`} alt="Facebook Icon"/>
              </a>

              <a href={settings.youtube_link} taget="_blank">
                <img src={`${YoutubeIcon}`} alt="YouTube Icon"/>
              </a>

              <a href={settings.linkedin_link} taget="_blank">
                <img src={`${LinkedinIcon}`} alt="LinkedIn Icon"/>
              </a>
            </div>

            <h2>Dokumenty</h2>
            <div className="contactPage__contactInfo_documents">
              {settings.documents.map((document, index) => (
                <a href={document.document_file.source_url} target="_blank">
                  {document.document_name}
                </a>
              ))}
            </div>
          </Col>
        </Row>
      </Container>
    </Layout>
  )
}

export default TemplateContact

export const query = graphql`
  query($id: Int!) {
    wordpressPage(wordpress_id: { eq: $id }) {
      wordpress_id
      title
    }
    allWordpressAcfOptions {
      nodes {
        websiteSettings {
          footer_content
          footer_email
          footer_telefon
          company_info
          facebook_link
          youtube_link
          linkedin_link
          documents {
            document_file {
              source_url
            }
            document_name
          }
        }
      }
    }
  }
`
