import React from "react"
import { Link } from "gatsby"
import Layout from "../../components/layout"
import SEO from "../../components/seo"
import { Container, Row, Col } from "react-bootstrap"

import PostArrow from "../../images/post_arrow.svg"

import Title from "../../components/Title/title"
import Newsletter from "../../components/Newsletter/newsletter"
import MostReadable from "../../components/MostReadable/mostReadable"
import PostCard from "../../components/PostCard/postCard"
import "./TemplateCategory.scss"

const TemplateCategory = ({ data, pageContext }) => {
  const { currentPage, numPages } = pageContext
  const isFirst = currentPage === 1
  const isLast = currentPage === numPages
  const prevPage =
    currentPage - 1 === 1 ? "/" : "page-" + (currentPage - 1).toString()
  const nextPage = "page-" + (currentPage + 1).toString()

  return (
    <Layout>
      <SEO title="Home" keywords={[`gatsby`, `application`, `react`]} />

      <Container fluid={true} className="standardPadding">
        <Row>
          <Col md={12}>
            <Title text={data.wordpressPage.title} mainTitle={true} />
          </Col>

          
        </Row>
      </Container>
    </Layout>
  )
}

export default TemplateCategory

export const query = graphql`
  query($id: Int!, $name: String!) {
    wordpressTag(wordpress_id: {eq: $id }) {
      title
      wordpress_id
    }
    
  }
`
