import React from "react"
import Layout from "../../components/layout"
import SEO from "../../components/seo"

import ProgramsSection from "../../components/ProgramsSection/programsSection"

const TemplateProgramy = ({ data }) => (
  <Layout>
    <SEO title="Home" keywords={[`gatsby`, `application`, `react`]} />
    <ProgramsSection
      title={data.wordpressPage.title}
      desc={data.wordpressPage.content}
      nodes={data.allWordpressWpFilary}
    />
  </Layout>
)

export default TemplateProgramy

export const query = graphql`
  query($id: Int!) {
    wordpressPage(wordpress_id: { eq: $id }) {
      wordpress_id
      title
      content
    }
    allWordpressWpFilary {
      nodes {
        acf {
          programy {
            post_title
            post_name
            post_content
          }
        }
        title
        count_of_bold_words
        featured_media {
          localFile {
            childImageSharp {
              resolutions(width: 560, cropFocus: CENTER, quality: 100) {
                src
              }
            }
          }
        }
      }
    }
  }
`
