import React from "react"
import Layout from "../components/layout"
import SEO from "../components/seo"
import axios from "axios"
import queryString from "query-string"
import { Container, Row, Col } from "react-bootstrap"

import Title from "../components/Title/Title"

class ContactPage extends React.Component {
  constructor(props) {
    super(props)

    this.state = {
      getText: "",
      isLoading: true,
    }
  }

  render() {
    return (
      <Layout>
        <SEO title="Home" keywords={[`gatsby`, `application`, `react`]} />
        <Container fluid={true} className="standardPadding">
          <Row>
            <Title
              text={data.wordpressPage.title}
              mainTitle={true}
            />
          </Row>
        </Container>
      </Layout>
    )
  }
}

export default ContactPage

export const query = graphql`
  query {
    wordpressPage(wordpress_id: { eq: $id }) {
        wordpress_id
        title
        acf {
          main_image {
            localFile {
              childImageSharp {
                resolutions(
                  width: 1920
                  height: 640
                  cropFocus: CENTER
                  quality: 90
                ) {
                  src
                }
              }
            }
          }
          about_gallery {
            alt_text
            localFile {
              childImageSharp {
                resolutions(width: 420, cropFocus: CENTER, quality: 90) {
                  src
                }
              }
            }
          }
          about_content
          about_title
          work_title
          work_content
          work_image {
            alt_text
            localFile {
              childImageSharp {
                resolutions(
                  width: 840
                  height: 580
                  cropFocus: CENTER
                  quality: 100
                ) {
                  src
                }
              }
            }
          }
          team_title
        }
      }
  }
`
