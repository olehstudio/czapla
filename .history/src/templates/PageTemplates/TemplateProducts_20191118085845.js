import React from "react"
import Layout from "../../components/layout"
import SEO from "../../components/seo"

const TemplateProgramy = ({ data }) => (
  <Layout>
    <SEO title="Home" keywords={[`gatsby`, `application`, `react`]} />

    <section className="productsSection">
      <Container fluid={true}>
        <Row>
          <Col md={12}>
            <Title text={props.title} mainTitle={false} />
          </Col>

          <Col md={12}>
            <div
              className="projectsSection__desc"
              dangerouslySetInnerHTML={{ __html: props.desc }}
            />
          </Col>
        </Row>

        {props.nodes.nodes.map((node, index) => (
          <Row key={index} className="projectsSection__projectRow">
            <Col md={6} className="projectsSection__imageCol">
              <img
                src={`${node.featured_media.localFile.childImageSharp.resolutions.src}`}
                alt={`${node.title}`}
              />

              <div className="innerCol">
                <img src={`${Logo}`} alt="" />
                <h4
                  dangerouslySetInnerHTML={{
                    __html: wrapFirstLetterInSpan(node.title, true),
                  }}
                />
              </div>
            </Col>

            <Col md={6} className="projectsSection__contentCol">
              <h3
                dangerouslySetInnerHTML={{
                  __html: wrapFirstLetterInSpan(node.title, false),
                }}
              />
              <ul className="programLinks">
                {node.acf.programy.map((program, index) => (
                  <li key={index}>
                    <svg
                      width="13"
                      height="24"
                      viewBox="0 0 13 24"
                      fill="none"
                      xmlns="http://www.w3.org/2000/svg"
                    >
                      <path
                        fill-rule="evenodd"
                        clip-rule="evenodd"
                        d="M6.21398 12.3221C6.29881 12.3221 6.21398 12.3221 6.21398 12.3221C8.58909 12.1611 10.2008 9.34228 13 7.97315C10.6249 12 9.60699 11.5168 6.38364 14.4966C3.41475 17.396 3.49958 20.7785 1.03965 24C0.869999 20.0537 2.73615 17.1544 1.80308 13.2081C0.954824 9.66443 -0.74168 7.16779 0.361048 2.97987C1.2093 5.31544 1.2093 9.98658 2.99063 11.4362C3.16028 11.5973 3.16028 11.5973 3.2451 11.4362C5.78986 8.69799 4.68713 4.10738 8.16496 0C8.75874 5.2349 5.36573 8.53691 6.04433 12.2416C6.12916 12.3221 6.12916 12.3221 6.21398 12.3221Z"
                        fill="#B49678"
                      />
                    </svg>
                    <Link
                      to={`/program/${program.post_name}`}
                      dangerouslySetInnerHTML={{ __html: program.post_content }}
                    />
                  </li>
                ))}
              </ul>
            </Col>
          </Row>
        ))}
      </Container>
    </section>
  </Layout>
)

export default TemplateProgramy

export const query = graphql`
  query($id: Int!) {
    wordpressPage(wordpress_id: { eq: $id }) {
      wordpress_id
      title
      content
    }
    allWordpressWpProductCzapla {
      nodes {
        title
        content
        slug
        featured_media {
          localFile {
            childImageSharp {
              resolutions(width: 560, cropFocus: CENTER, quality: 100) {
                src
              }
            }
          }
        }
      }
    }
  }
`
