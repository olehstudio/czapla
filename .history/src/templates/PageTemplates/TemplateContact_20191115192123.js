import React from "react"
import Layout from "../../components/layout"
import SEO from "../../components/seo"
import { Container, Row, Col } from "react-bootstrap"

import Title from "../../components/Title/title"

const TemplateContact = ({ data }) => (
  <Layout>
    <SEO title="Home" keywords={[`gatsby`, `application`, `react`]} />

    <img
      style={{ width: "100%" }}
      src={
        data.wordpressPage.header_image.localFile.childImageSharp.resolutions
          .src
      }
      alt={data.wordpressPage.title}
    />

    <Container>
      <Row>
        <Col md={12}>
          <Title text={data.wordpressPage.title} mainTitle={true} />
        </Col>

        
      </Row>
    </Container>
    </Layout>
)

export default TemplateContact

export const query = graphql`
  query($id: Int!) {
    wordpressPage(wordpress_id: { eq: $id }) {
      wordpress_id
      title
      acf {
        header_image {
          localFile {
            childImageSharp {
              resolutions(
                width: 1920
                height: 640
                cropFocus: CENTER
                quality: 90
              ) {
                src
              }
            }
          }
        }
      }
    }
  }
`
