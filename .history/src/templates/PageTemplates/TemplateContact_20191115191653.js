import React from "react"
import Layout from "../../components/layout"
import SEO from "../../components/seo"
import axios from "axios"
import queryString from "query-string"
import StaticQuery from "gatsby"
import { Container, Row, Col } from "react-bootstrap"

import Title from "../../components/Title/Title"

class ContactPage extends React.Component {
  constructor(props) {
    super(props)

    this.state = {
      getText: "",
      isLoading: true,
    }
  }

  render() {
    return (
      <Layout>
        <SEO title="Home" keywords={[`gatsby`, `application`, `react`]} />

        
      </Layout>
    )
  }
}

export default () => (
  <StaticQuery
    query={graphql`
      query($id: Int!) {
        wordpressPage(wordpress_id: { eq: $id }) {
          wordpress_id
          title
          acf {
            main_image {
              localFile {
                childImageSharp {
                  resolutions(
                    width: 1920
                    height: 640
                    cropFocus: CENTER
                    quality: 90
                  ) {
                    src
                  }
                }
              }
            }
          }
        }
      }
    `}
    render={data => <ContactPage data={data} />}
  />
)
