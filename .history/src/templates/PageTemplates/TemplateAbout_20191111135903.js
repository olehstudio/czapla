import React from "react"
import Layout from "../../components/layout"
import SEO from "../../components/seo"
import { Container, Row, Col } from "react-bootstrap"

import Title from "../../components/Title/title"
import "./TemplateAbout.scss"

const TemplateAbout = ({ data }) => (
  <Layout>
    <SEO title="Home" keywords={[`gatsby`, `application`, `react`]} />

    <img
      style={{ width: "100%" }}
      src={
        data.wordpressPage.acf.main_image.localFile.childImageSharp.resolutions
          .src
      }
      alt={data.wordpressPage.title}
    />

    <Container>
      <Row>
        <Col md={12}>
          <Title text={data.wordpressPage.acf.about_title} mainTitle={true} />
        </Col>

        <Col md={6} lg={{ span: 4, offset: 2 }}>
          {data.wordpressPage.acf.about_gallery.map((image, index) => (
            <img
              src={image.localFile.childImageSharp.resolutions.src}
              alt={image.alt_text}
              style={{
                width: "100%",
                borderRadius: "7px",
                marginBottom: "40px",
              }}
            />
          ))}
        </Col>
        <Col md={6} lg={{ span: 4, offset: 0 }}>
          <div
            className="contentText"
            dangerouslySetInnerHTML={{
              __html: data.wordpressPage.acf.about_content,
            }}
          />
        </Col>
      </Row>
    </Container>

    <Container fluid={true} className="standardPadding">
      <Row>
        <Col md={12}>
          <Title text={data.wordpressPage.acf.work_title} mainTitle={false} />
        </Col>

        <Col md={6}>
          <img
            src={
              data.wordpressPage.acf.work_image.localFile.childImageSharp
                .resolutions.src
            }
            alt={data.wordpressPage.acf.work_image.alt_text}
            style={{ width: "100%", borderRadius: "10px" }}
          />
        </Col>
        <Col md={6} lg={{ span: 3, offset: 0 }}>
          <div
            className="contentText"
            dangerouslySetInnerHTML={{
              __html: data.wordpressPage.acf.work_content,
            }}
          />
        </Col>
      </Row>
    </Container>

    <Container fluid={true} className="standardPadding">
      <Row>
        <Col md={12}>
          <Title text={data.wordpressPage.acf.team_title} mainTitle={false} />
        </Col>

        {data.allWordpressWpMember.nodes.map((member, index) => (
          <Col key={index} md={6} lg={3} className="singleMember">
            <div className="imageWrapper">
              <img
                src={
                  member.featured_media.localFile.childImageSharp
                    .resolutions.src
                }
                alt={member.title}
              />
            </div>
            <h4 dangerouslySetInnerHTML={{ __html: member.title }} />
          </Col>
        ))}
      </Row>
    </Container>
  </Layout>
)

export default TemplateAbout

export const query = graphql`
  query($id: Int!) {
    wordpressPage(wordpress_id: { eq: $id }) {
      wordpress_id
      title
      acf {
        main_image {
          localFile {
            childImageSharp {
              resolutions(
                width: 1920
                height: 640
                cropFocus: CENTER
                quality: 90
              ) {
                src
              }
            }
          }
        }
        about_gallery {
          alt_text
          localFile {
            childImageSharp {
              resolutions(width: 420, cropFocus: CENTER, quality: 90) {
                src
              }
            }
          }
        }
        about_content
        about_title
        work_title
        work_content
        work_image {
          alt_text
          localFile {
            childImageSharp {
              resolutions(
                width: 840
                height: 580
                cropFocus: CENTER
                quality: 90
              ) {
                src
              }
            }
          }
        }
        team_title
      }
    }
    allWordpressWpMember {
      nodes {
        title
        featured_media {
          localFile {
            childImageSharp {
              resolutions(
                width: 560
                height: 560
                cropFocus: CENTER
                quality: 90
              ) {
                src
              }
            }
          }
        }
        acf {
          linkedin_link
          description
          positions_repeater {
            single_position
          }
        }
      }
    }
  }
`
