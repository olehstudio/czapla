import React from "react"
import Layout from "../components/layout"

const PageTemplate = () => (
  <Layout>
    <h1 style={{ marginTop: '250px' }}>Page Template</h1>
  </Layout>
)

export default PageTemplate