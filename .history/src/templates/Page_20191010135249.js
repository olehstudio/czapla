import React from "react"
import Layout from "../components/layout"

const PageTemplate = ({ data }) => (
  <Layout>
    <h1 style={{ marginTop: '250px' }}>Page Template</h1>
  </Layout>
)

export default PageTemplate

export const query = graphql`
  query($id: Int!) {
    wordpressPage(wordpress_id: { eq: $id }) {
      wordpress_id
    }
  }
`