import React from "react"
import { StaticQuery, graphql, Link } from "gatsby"

import { Container, Navbar, Nav, Row } from 'react-bootstrap';

class Header extends React.Component {
  constructor(props){
    super(props)
    this.state = {}
  }

  render(){
    return (
      <Container fluid="false">
        <Row>
        <Navbar bg="light" expand="lg">
          <Navbar.Brand href="#home">React-Bootstrap</Navbar.Brand>
          <Navbar.Toggle aria-controls="basic-navbar-nav" />
          <Navbar.Collapse id="basic-navbar-nav">
            <Nav className="mr-auto">
              <Nav.Link href="#home">Home</Nav.Link>
              <Nav.Link href="#link">Link</Nav.Link>
            </Nav>
          </Navbar.Collapse>
        </Navbar>
        </Row>
      </Container>
    )
  }
}

export default () => (
  <StaticQuery
    query={graphql`
      query {
        wordpressWpApiMenusMenusItems(name: { eq: "Menu" }) {
          items {
            title
            object_slug
          }
        }
      }
    `}
    render={data => (
      <Header menuItems={data.wordpressWpApiMenusMenusItems.items} />
    )}
  />
)