import React from "react"
import { StaticQuery, graphql, Link } from "gatsby"
import { Carousel } from "react-bootstrap"

import "./homepageCarousel.scss"

class HomepageCarousel extends React.Component {
  constructor(props) {
    super(props)
    this.state = {}
  }

  render() {
    return (
      <Carousel>
        {this.props.data.map(
          (slide,
          index) => (
            <Carousel.Item>
              <img
                className="d-block w-100"
                src="holder.js/800x400?text=First slide&bg=373940"
                alt="First slide"
              />
              <Carousel.Caption>
                <h3>First slide label</h3>
                <p>
                  Nulla vitae elit libero, a pharetra augue mollis interdum.
                </p>
              </Carousel.Caption>
            </Carousel.Item>
          ))
        )}
      </Carousel>
    )
  }
}

export default HomepageCarousel
