import React from "react"
import { StaticQuery, graphql, Link } from "gatsby"
import { Container, Row, Col, Carousel } from "react-bootstrap"

import "./homepageCarousel.scss"
import Arrow from "./arrowComponent"

class HomepageCarousel extends React.Component {
  constructor(props) {
    super(props)
    this.state = {}
  }

  render() {
    return (
      <Container fluid="true">
        <Row>
          <Col className="p-0">
            <Carousel
              prevIcon={<Arrow />}
              nextIcon={<Arrow />}
            >
              {this.props.data.map((slide, index) => (
                <Carousel.Item key={index}>
                  <img
                    className=""
                    src={
                      slide.single_slide.localFile.childImageSharp.resolutions
                        .src
                    }
                    alt=""
                  />
                  {/* <Carousel.Caption>
                    <h3>First slide label</h3>
                    <p>
                      Nulla vitae elit libero, a pharetra augue mollis interdum.
                    </p>
                  </Carousel.Caption> */}
                </Carousel.Item>
              ))}
            </Carousel>
          </Col>
        </Row>
      </Container>
    )
  }
}

export default HomepageCarousel
