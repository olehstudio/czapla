export default () => (
    <span><svg width="34" height="88" viewBox="0 0 34 88" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M29.7627 84L3.99998 44L29.7627 4" stroke="#B49678" stroke-width="8" stroke-linecap="round" stroke-linejoin="round"/></svg></span>
)