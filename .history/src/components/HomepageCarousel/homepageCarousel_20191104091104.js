import React from "react"
import { StaticQuery, graphql, Link } from "gatsby"
import { Container, Row, Col, Carousel } from "react-bootstrap"

import "./homepageCarousel.scss"
import Arrow from "./arrowComponent"

class HomepageCarousel extends React.Component {
  constructor(props) {
    super(props)
    this.state = {}
  }

  svgImage = '';

  render() {
    return (
      <Container fluid="true">
        <Row>
          <Col className="p-0">
            <Carousel
              prevIcon={<span><svg width="34" height="88" viewBox="0 0 34 88" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M29.7627 84L3.99998 44L29.7627 4" stroke="#B49678" stroke-width="8" stroke-linecap="round" stroke-linejoin="round"/></svg></span>}
              nextIcon={<span><svg width="34" height="88" viewBox="0 0 34 88" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M29.7627 84L3.99998 44L29.7627 4" stroke="#B49678" stroke-width="8" stroke-linecap="round" stroke-linejoin="round"/></svg></span>}
            >
              {this.props.data.map((slide, index) => (
                <Carousel.Item key={index}>
                  <img
                    className=""
                    src={
                      slide.single_slide.localFile.childImageSharp.resolutions
                        .src
                    }
                    alt=""
                  />
                  {/* <Carousel.Caption>
                    <h3>First slide label</h3>
                    <p>
                      Nulla vitae elit libero, a pharetra augue mollis interdum.
                    </p>
                  </Carousel.Caption> */}
                </Carousel.Item>
              ))}
            </Carousel>
          </Col>
        </Row>
      </Container>
    )
  }
}

export default HomepageCarousel
