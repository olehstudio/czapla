import React from "react"
import { Link } from "gatsby"
import { Col } from "react-bootstrap"
import moment from "moment"
import "moment/locale/pl"

import "./smallPostCard.scss"

function SmallPostCard(props) {
  const excerptContent = text => {
    return text.substr(0, 200).concat("...")
  }

  return (
    <Col md={props.col} className="smallPostCard">
      <Link
        to={`/${props.categories[0].slug}/${props.slug}`}
      >
        <img
          className="smallPostCard__mainImage"
          src={props.image}
          alt={props.title}
        />
      </Link>

      <div className="smallPostCard__wrapper">
        <h2
          className="smallPostCard__title"
          dangerouslySetInnerHTML={{ __html: props.title }}
        />

        <div className="smallPostCard__description">
          {moment(props.date)
            .locale("pl")
            .format("LL")}
          &nbsp;|&nbsp;
          {props.categories.map((category, index) => (
            <a href={`/${category.slug}`} key={index}>
              {category.name}
            </a>
          ))}
        </div>

        <div className="smallPostCard__tags">
          {props.tags.map((tag, index) => (
            <a href={`/${tag.slug}`} key={index}>
              {tag.name} {index !== props.tags.length - 1 ? ", " : null}
            </a>
          ))}
        </div>

        <div
          className="smallPostCard__content"
          dangerouslySetInnerHTML={{ __html: excerptContent(props.content) }}
        />

        <Link
          to={`/${props.categories[0].slug}/${props.slug}`}
          className="smallPostCard__link"
        >
          Czytaj dalej
        </Link>
      </div>
    </Col>
  )
}

export default SmallPostCard
