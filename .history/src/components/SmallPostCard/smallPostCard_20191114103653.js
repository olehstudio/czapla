import React from "react"
import { Link } from "gatsby"
import { Col } from "react-bootstrap"
import moment from "moment"
import "moment/locale/pl"

import "./smallPostCard.scss"

function SmallPostCard(props) {
  const excerptContent = text => {
    return text.substr(0, 255).concat("...")
  }

  return (
    <Col md={props.col} className="smallPostCard">
      <img src={props.image} alt={props.title} />

      <h2 dangerouslySetInnerHTML={{ __html: props.title }} />

      <div className="postDesc">
        {moment(props.date)
          .locale("pl")
          .format("LL")}
        &nbsp;|&nbsp;
        {props.categories.map((category, index) => (
          <a href={`/${category.slug}`} key={index}>
            {category.name}
          </a>
        ))}
      </div>

      <div className="tags">
        {props.tags.map((tag, index) => (
          <a href={`/${tag.slug}`} key={index}>
            {tag.name}
          </a>
        ))}
      </div>

      <div
        dangerouslySetInnerHTML={{ __html: excerptContent(props.content) }}
      />

      <Link to={`${props.slug}`}>Czytaj dalej</Link>
    </Col>
  )
}

export default SmallPostCard
