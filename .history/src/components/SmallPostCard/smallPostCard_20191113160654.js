import React from "react"
import { Link } from "gatsby"
import { Col } from "react-bootstrap"

import "./smallPostCard.scss"

function SmallPostCard(props) {
    return (
        <Col md={props.col} className="smallPostCard">
            <img src={props.image} alt={props.title}/>
        </Col>
    )
}

export default SmallPostCard
