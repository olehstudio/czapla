import React from "react"
import { Link } from "gatsby"
import { Col } from "react-bootstrap"

import "./smallPostCard.scss"

function SmallPostCard(props) {
    return (
        <Col md={props.col} className="smallPostCard">
            <img src={props.image} alt={props.title}/>
            <h2 dangerouslySetInnerHTML={{ __html: props.title }} />
            <div dangerouslySetInnerHTML={{ __html: props.content.substr(0, 255) }} />
        </Col>
    )
}

export default SmallPostCard
