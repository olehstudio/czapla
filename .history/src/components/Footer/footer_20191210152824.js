import React from "react"
import { StaticQuery, graphql, Link } from "gatsby"
import { Container, Row, Col } from "react-bootstrap"

import FacebookIcon from "../../images/social_fb.png"
import YoutubeIcon from "../../images/social_yt.png"
import LinkedinIcon from "../../images/social_li.png"
import Logo from "../../images/logo_footer.svg"
import "./footer.scss"

class Footer extends React.Component {
  constructor(props) {
    super(props)
  }

  componentDidMount() {
    const addSierotki = function(text) {
      let new_text = text
      let sierotki = ["a", "i", "o", "u", "w", "z"]
      let return_sierotki = [
        " a&nbsp;",
        " i&nbsp;",
        " o&nbsp;",
        " u&nbsp;",
        " w&nbsp;",
        " z&nbsp;",
      ]
      let complicated_return_sierotki = [
        "&nbsp;a&nbsp;",
        "&nbsp;i&nbsp;",
        "&nbsp;o&nbsp;",
        "&nbsp;u&nbsp;",
        "&nbsp;w&nbsp;",
        "&nbsp;z&nbsp;",
      ]

      sierotki.forEach(function(key, value) {
        var replace = "\\s" + key + "\\s"
        var regexp = new RegExp(replace, "g")
        new_text = new_text.replace(regexp, return_sierotki[value])

        var complicated_replace = "&nbsp;" + key + "\\s"
        var complicated_regexp = new RegExp(complicated_replace, "g")
        new_text = new_text.replace(
          complicated_regexp,
          complicated_return_sierotki[value]
        )
      })

      return new_text
    }

    var elements = document.querySelectorAll("li, p, h1, h2, h3, h4, h5, h6")
    elements.forEach(function(el, i) {
      el.innerHTML = addSierotki(el.innerHTML)
    })
  }

  render() {
    return (
      <>
        <div
          className="siteFooter"
          style={{
            backgroundImage: `url(${this.props.content.footer_background_image.localFile.childImageSharp.resolutions.src})`,
          }}
        >
          <Container>
            <Row>
              <Col lg={12} className="text-center">
                <img src={`${Logo}`} alt="" className="siteFooter__logo" />

                <div
                  className="siteFooter__content"
                  dangerouslySetInnerHTML={{
                    __html: this.props.content.footer_content,
                  }}
                />

                <div className="siteFooter__links">
                  <a
                    href={`tel:${this.props.content.footer_telefon}`}
                    className="siteFooter__tel"
                  >
                    {this.props.content.footer_telefon}
                  </a>
                  <a
                    href={`mailto:${this.props.content.footer_email}`}
                    className="siteFooter__email"
                  >
                    {this.props.content.footer_email}
                  </a>

                  <div className="socialIcons">
                    <a href={this.props.content.facebook_link} taget="_blank">
                      <img src={`${FacebookIcon}`} alt="Facebook Icon" />
                    </a>

                    <a href={this.props.content.youtube_link} taget="_blank">
                      <img src={`${YoutubeIcon}`} alt="YouTube Icon" />
                    </a>

                    <a href={this.props.content.linkedin_link} taget="_blank">
                      <img src={`${LinkedinIcon}`} alt="LinkedIn Icon" />
                    </a>
                  </div>

                  <a
                    href={this.props.content.privacy_policy_link}
                    className="siteFooter__privacy"
                    target="_blank"
                  >
                    {this.props.content.privacy_policy_text}
                  </a>
                </div>
              </Col>
            </Row>
          </Container>
        </div>

        <div className="siteCopyright">
          <p
            dangerouslySetInnerHTML={{
              __html: this.props.content.copyright_text,
            }}
          />
          <a href="https://sgr.pl" rel="noopener noreferrer" target="_blank">
            Studio Graficzne
          </a>
        </div>
      </>
    )
  }
}

export default () => (
  <StaticQuery
    query={graphql`
      query {
        allWordpressAcfOptions {
          nodes {
            websiteSettings {
              copyright_text
              footer_content
              footer_email
              footer_telefon
              privacy_policy_link
              privacy_policy_text
              facebook_link
              youtube_link
              linkedin_link
              footer_background_image {
                localFile {
                  childImageSharp {
                    resolutions(width: 1280, cropFocus: CENTER, quality: 100) {
                      src
                    }
                  }
                }
              }
            }
          }
        }
      }
    `}
    render={data => (
      <Footer content={data.allWordpressAcfOptions.nodes[0].websiteSettings} />
    )}
  />
)
