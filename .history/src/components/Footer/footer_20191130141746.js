import React from "react"
import { StaticQuery, graphql, Link } from "gatsby"
import { Container, Row, Col } from "react-bootstrap"

import FacebookIcon from "../../images/social_fb.png"
import YoutubeIcon from "../../images/social_yt.png"
import LinkedinIcon from "../../images/social_li.png"
import Logo from "../../images/logo_footer.svg"
import "./footer.scss"

class Footer extends React.Component {
  constructor(props) {
    super(props)
  }

  render() {
    return (
      <>
        <div
          className="siteFooter"
          style={{
            backgroundImage: `url(${this.props.content.footer_background_image.localFile.childImageSharp.resolutions.src})`,
          }}
        >
          <Container>
            <Row>
              <Col lg={12} className="text-center">
                <img src={`${Logo}`} alt="" className="siteFooter__logo" />

                <div
                  className="siteFooter__content"
                  dangerouslySetInnerHTML={{
                    __html: this.props.content.footer_content,
                  }}
                />

                <div className="siteFooter__links">
                  <a
                    href={`tel:${this.props.content.footer_telefon}`}
                    className="siteFooter__tel"
                  >
                    {this.props.content.footer_telefon}
                  </a>
                  <a
                    href={`mailto:${this.props.content.footer_email}`}
                    className="siteFooter__email"
                  >
                    {this.props.content.footer_email}
                  </a>

                  <div className="socialIcons">
                    <a href={this.props.content.facebook_link} taget="_blank">
                      <img src={`${FacebookIcon}`} alt="Facebook Icon" />
                    </a>

                    <a href={this.props.content.youtube_link} taget="_blank">
                      <img src={`${YoutubeIcon}`} alt="YouTube Icon" />
                    </a>

                    <a href={this.props.content.linkedin_link} taget="_blank">
                      <img src={`${LinkedinIcon}`} alt="LinkedIn Icon" />
                    </a>
                  </div>

                  <Link
                    to={this.props.content.privacy_policy_link}
                    className="siteFooter__privacy"
                  >
                    {this.props.content.privacy_policy_text}
                  </Link>
                </div>
              </Col>
            </Row>
          </Container>
        </div>

        <div className="siteCopyright">
          <p
            dangerouslySetInnerHTML={{
              __html: this.props.content.copyright_text,
            }}
          />
          <a href="https://sgr.pl" rel="noopener noreferrer" target="_blank">
            Studio Graficzne
          </a>
        </div>
      </>
    )
  }
}

export default () => (
  <StaticQuery
    query={graphql`
      query {
        allWordpressAcfOptions {
          nodes {
            websiteSettings {
              copyright_text
              footer_content
              footer_email
              footer_telefon
              privacy_policy_link
              privacy_policy_text
              facebook_link
              youtube_link
              linkedin_link
              footer_background_image {
                localFile {
                  childImageSharp {
                    resolutions(width: 1280, cropFocus: CENTER, quality: 100) {
                      src
                    }
                  }
                }
              }
            }
          }
        }
      }
    `}
    render={data => (
      <Footer content={data.allWordpressAcfOptions.nodes[0].websiteSettings} />
    )}
  />
)
