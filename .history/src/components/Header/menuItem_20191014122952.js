import React from "react"
import { Link } from "gatsby"

import SubMenu from "./subMenu"

class MenuItem extends React.Component {
    constructor(props){
        super(props)
        this.state = {
            isOpen: false
        }
    }

    render() {
        return (
            <li
                onMouseOver={() => {this.setState({ isOpen: true })}}
                onMouseLeave={() => {this.setState({ isOpen: false })}}
                // className={this.state.isOpen ? 'opened' : ''}
                // className={this.props.pageId === this.props.item.object_id ? 'active' : ''}  
                className={`${this.props.item.object_id}`} 
            >
                <Link 
                  to={this.props.item.url}
                >
                  {this.props.item.title}
                </Link>

                {this.props.item.wordpress_children ? 
                  (
                    <SubMenu 
                        submenuItems={this.props.item.wordpress_children} 
                    />
                  )  
                  :
                  ''
                  } 
            </li>
        )
    }
}

export default MenuItem