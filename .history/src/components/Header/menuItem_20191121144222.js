import React from "react"
import { Link } from "gatsby"
import { isMobile } from "react-device-detect"

import SubMenu from "./subMenu"

class MenuItem extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      isOpen: false,
      currentPage: "",
    }
  }

  componentDidMount() {
    if (
      window.location.pathname.split(/[/]+/).pop() ===
      this.props.item.object_slug
    ) {
      this.setState({ currentPage: "currentPage" })
    }
  }

  mobileOpenHandler = event => {
    this.setState({ isOpen: !this.state.isOpen })
  }

  render() {
    return (
      <li
        onMouseOver={() => {
          this.setState({ isOpen: true })
        }}
        onMouseLeave={() => {
          this.setState({ isOpen: false })
        }}
        className={this.state.isOpen ? "opened" : ""}
      >
        {this.props.item.object === "page" ? (
          <Link
            to={`/${this.props.item.object_slug}`}
            className={`${this.state.currentPage}`}
          >
            {this.props.item.title}
          </Link>
        ) : this.props.item.object === "custom" ? (
          <Link to={`${this.props.item.url}`}>{this.props.item.title}</Link>
        ) : (
          <Link
            to={`/${this.props.item.object}/${this.props.item.object_slug}`}
          >
            {this.props.item.title}
          </Link>
        )}

        {this.props.item.wordpress_children && isMobile && (
          <svg
            width="16"
            height="10"
            viewBox="0 0 16 10"
            fill="none"
            xmlns="http://www.w3.org/2000/svg"
          >
            <path
              d="M14 2L8 8L2 2"
              stroke="#C4C4C4"
              stroke-width="3"
              stroke-linecap="round"
              stroke-linejoin="round"
            />
          </svg>
        )}

        {this.props.item.wordpress_children ? (
          <SubMenu submenuItems={this.props.item.wordpress_children} />
        ) : (
          ""
        )}
      </li>
    )
  }
}

export default MenuItem
