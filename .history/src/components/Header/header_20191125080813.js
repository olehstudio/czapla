import React from "react"
import { StaticQuery, graphql, Link } from "gatsby"
import { Navbar, Nav } from "react-bootstrap"
import { isMobile } from "react-device-detect"

import MenuItem from "./menuItem"
import CustomMenuToggler from "../CustomMenuToggler/customMenuToggler"

import "./header.scss"
import FacebookHeader from "../../images/facebookHeader.svg"
import Logo from "../../images/logo.svg"

class Header extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      activeClass: "",
      searchBar: false,
      searchText: "",
      toggleStatus: false,
    }
  }

  componentDidMount() {
    window.addEventListener("scroll", () => {
      let activeClass = "scrolled"
      if (window.scrollY < 150) {
        activeClass = "top"
      }
      this.setState({ activeClass: activeClass })
    })
  }

  handleTogglerClick = event => {
    this.setState({ toggleStatus: !this.state.toggleStatus })
  }

  searchIconHandler = event => {
    event.preventDefault()
    this.setState({ searchBar: !this.state.searchBar })
  }

  searchInputHandler = event => {
    this.setState({ searchText: event.target.value })
  }

  _handleKeyDown = event => {
    if (event.key === 'Enter') {
      console.log('do validate');
    }
  }

  render() {
    return (
      <>
        <Navbar expand="lg" className={`${this.state.activeClass}`}>
          <Navbar.Brand>
            <Link to="/">
              <img src={Logo} alt="CzaplaAndMore Logo" />
            </Link>
          </Navbar.Brand>

          <Navbar.Collapse id="basic-navbar-nav">
            <Nav>
              {this.props.menuItems.map((item, index) => (
                <MenuItem item={item} key={index} pageId={this.props.pageId} />
              ))}
            </Nav>
          </Navbar.Collapse>

          <a href={this.props.settings.facebook_link} target="_blank" className="headerSocialLink">
            <img src={`${FacebookHeader}`} alt="Facebook Czapla" />
          </a>

          <a href="" className="searchIcon" onClick={this.searchIconHandler}>
            <svg
              width="16"
              height="16"
              viewBox="0 0 16 16"
              fill="none"
              xmlns="http://www.w3.org/2000/svg"
            >
              <path
                d="M11.4286 10.0571H10.6971L10.4229 9.78286C11.3371 8.77714 11.8857 7.40571 11.8857 5.94286C11.8857 2.65143 9.23429 0 5.94286 0C2.65143 0 0 2.65143 0 5.94286C0 9.23429 2.65143 11.8857 5.94286 11.8857C7.40571 11.8857 8.77714 11.3371 9.78286 10.4229L10.0571 10.6971V11.4286L14.6286 16L16 14.6286L11.4286 10.0571ZM5.94286 10.0571C3.65714 10.0571 1.82857 8.22857 1.82857 5.94286C1.82857 3.65714 3.65714 1.82857 5.94286 1.82857C8.22857 1.82857 10.0571 3.65714 10.0571 5.94286C10.0571 8.22857 8.22857 10.0571 5.94286 10.0571Z"
                fill="#B49678"
              />
            </svg>
          </a>

          <Navbar.Toggle children={<CustomMenuToggler />} onClick={this.handleTogglerClick} className={this.state.toggleStatus ? "opened" : null}/>

          <div
            className={`searchBar ${this.state.searchBar ? "open" : "hide"}`}
          >
            <div className="inputWrapper">
              <input
                type="text"
                onChange={this.searchInputHandler}
                onKeyDown={this._handleKeyDown}
                placeholder="wpisz szukany tekst"
              />
            </div>
            <a
              href={`/search/?search=${this.state.searchText}`}
              className="czaplaLink"
              id="searchButton"
            >
              Szukaj
            </a>
          </div>
        </Navbar>
      </>
    )
  }
}

export default () => (
  <StaticQuery
    query={graphql`
      query {
        wordpressWpApiMenusMenusItems(name: { eq: "Menu" }) {
          items {
            url
            title
            object_slug
            object
            wordpress_children {
              wordpress_children {
                title
                url
                object_slug
                object
              }
              title
            }
          }
        }
        wordpressPage {
          wordpress_id
        }
        allWordpressAcfOptions {
          nodes {
            websiteSettings {
              facebook_link
            }
          }
        }
      }
    `}
    render={data => (
      <Header
        menuItems={data.wordpressWpApiMenusMenusItems.items}
        pageId={data.wordpressPage.wordpress_id}
        settings={data.allWordpressAcfOptions.nodes[0].websiteSettings}
      />
    )}
  />
)
