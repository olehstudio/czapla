import React from "react"
import { Link } from "gatsby"

class SubMenu extends React.Component {
  render() {
    return (
      <div className="submenuWrapper">
        {this.props.submenuItems.map((children, index) => (
          <div
            key={`${children.title}${index}`}
            className="submenuWrapper__categoryCol"
          >
            <h3>{children.title}</h3>
            {children.wordpress_children.map((nestedChildren, index) =>
              nestedChildren.object === "page" ? (
                <Link
                  key={`${nestedChildren.title}${index}`}
                  to={`/${nestedChildren.object_slug}`}
                >
                  {nestedChildren.title}
                </Link>
              ) : (
                <Link
                  key={`${nestedChildren.title}${index}`}
                  to={`/${nestedChildren.object}/${nestedChildren.object_slug}`}
                >
                  {nestedChildren.title}
                </Link>
              )
            )}
          </div>
        ))}
      </div>
    )
  }
}

export default SubMenu
