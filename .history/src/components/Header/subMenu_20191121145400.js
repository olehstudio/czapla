import React from "react"

import SubMenuSection from "../SubMenuSection/subMenuSection"

class SubMenu extends React.Component {
  render() {
    return (
      <div className="submenuWrapper">
        {this.props.submenuItems.map((children, index) => (
          <SubMenuSection 
          key={`${children.title}${index}`}
          children={children}
          />
        ))}
      </div>
    )
  }
}

export default SubMenu
