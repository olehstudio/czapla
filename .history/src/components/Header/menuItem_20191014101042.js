import React from "react"
import { Link } from "gatsby"

class MenuItem extends React.Component {
    constructor(props){
        super(props)
    }

    render() {
        return (
            <Link 
                  to={item.url}                
                >
                  {item.title}
                </Link>

                {item.wordpress_children ? 
                  (
                    <SubMenu submenuItems={item.wordpress_children} />
                  )  
                  :
                  ''
                  } 
        )
    }
}

export default MenuItem