import React from "react"
import { StaticQuery, graphql, Link } from "gatsby"
import { Container, Navbar, Nav, Row } from "react-bootstrap"

import MenuItem from "./menuItem"
import SubMenu from "./subMenu"

import "../../sass/header.scss"
import Logo from "../../images/logo.svg"

class Header extends React.Component {
  constructor(props){
    super(props)
    this.state = {
      activeClass: ''
    }
  }

  componentDidMount(){
    window.addEventListener('scroll', () => {
      let activeClass = 'scrolled'
      if (window.scrollY < 50) {
        activeClass = 'top'
      }
      this.setState({ activeClass: activeClass });
    });
  }

  render(){
    return (
      <Navbar expand="xl" className={`${this.state.activeClass}`}>
        <Navbar.Brand href="#home">
          <img src={Logo} alt="CzaplaAndMore Logo"/>
        </Navbar.Brand>

        <Navbar.Toggle aria-controls="basic-navbar-nav" />

        <Navbar.Collapse id="basic-navbar-nav">
          <Nav>
            {this.props.menuItems.map((item, index) => (
              <MenuItem item={item} key={index} pageId={this.props.pageId} /> 
            ))}
          </Nav>
        </Navbar.Collapse>
      </Navbar>
    )
  }
}

export default () => (
  <StaticQuery
    query={graphql`
      query {
        wordpressWpApiMenusMenusItems(name: { eq: "Menu" }) {
          items {
            url
            title
            wordpress_children {
              wordpress_children {
                title
                url
              }
              title
            }
            object_id
          }
        }
      }
    `}
    render={data => (
      <Header menuItems={data.wordpressWpApiMenusMenusItems.items} pageId={data.wordpressPage.wordpress_id} />
    )}
  />
)