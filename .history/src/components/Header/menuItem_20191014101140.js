import React from "react"
import { Link } from "gatsby"

import SubMenu from "./subMenu"

class MenuItem extends React.Component {
    constructor(props){
        super(props)
    }

    render() {
        return (
            <>
                <Link 
                  to={this.props.item.url}                
                >
                  {this.props.item.title}
                </Link>

                {this.props.item.wordpress_children ? 
                  (
                    <SubMenu submenuItems={this.props.item.wordpress_children} />
                  )  
                  :
                  ''
                  } 
            </>
        )
    }
}

export default MenuItem