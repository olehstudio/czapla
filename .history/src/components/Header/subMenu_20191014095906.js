import React from "react"

class SubMenu extends React.Component {
    constructor(props) {
        super(props)
    }

    render() {
        return (
            <div className="submenuWrapper">
                {this.props.submenuItems.map((children, index) => (
                    <div key={`${children.title}${index}`} className="submenuWrapper__categoryCol">
                        <h3>{children.title}</h3>
                        {children.wordpress_children.map((nestedChildren, index) => (
                            <Link key={`${nestedChildren.title}${index}`} to={`${nestedChildren.url}`}>
                                {nestedChildren.title}
                            </Link>
                        ))}
                    </div>
                ))}
            </div>
        )
    }
}

export default SubMenu