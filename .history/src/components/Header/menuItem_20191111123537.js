import React from "react"
import { Link } from "gatsby"

import SubMenu from "./subMenu"

class MenuItem extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      isOpen: false,
      currentPage: '',
    }
  }

  componentDidMount() {
    if (window.location.pathname.split(/[/]+/).pop() === this.props.item.object_slug) {
      this.setState({ currentPage: 'currentPage' });
    }
  }

  render() {
    return (
      <li
        onMouseOver={() => {
          this.setState({ isOpen: true })
        }}
        onMouseLeave={() => {
          this.setState({ isOpen: false })
        }}
        className={this.state.isOpen ? "opened" : ""}
      >
        {this.props.item.object === "page" ? (
          <Link
            to={`/${this.props.item.object_slug}`}
            className={`${this.state.currentPage}`}
          >
            {this.props.item.title}
          </Link>
        ) : this.props.item.object === "sustom" ? ((
          <Link
            to={`${this.props.item.url}`}
          >
            {this.props.item.title}
          </Link>
        )) : ((
          <Link
            to={`/${this.props.item.object}/${this.props.item.object_slug}`}
          >
            {this.props.item.title}
          </Link>
        ))}

        {this.props.item.wordpress_children ? (
          <SubMenu submenuItems={this.props.item.wordpress_children} />
        ) : (
          ""
        )}
      </li>
    )
  }
}

export default MenuItem
