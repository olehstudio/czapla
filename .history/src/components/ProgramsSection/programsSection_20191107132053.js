import React from "react"
import { Container, Row, Col } from "react-bootstrap"

import Title from "../Title/title"
import Logo from "../../images/logo_footer.svg"
import "./programsSection.scss"

function ProgramsSection(props) {
  const wrapFirstLetterInSpan = (string, br = false) => {
      let tempArr = string.split(' ');
      let brOutput = br ? '<br/>' : '';
      tempArr[0] = '<span>' + tempArr[0] + '</span>' + brOutput;
      return tempArr.join(' ');
  }

  return (
    <section className="projectsSection">
      <Container fluid={true}>
        <Row>
          <Col md={12}>
            <Title text={props.title} mainTitle={false} />
          </Col>

          <Col md={12}>
            <div
              className="projectsSection__desc"
              dangerouslySetInnerHTML={{ __html: props.desc }}
            />
          </Col>
        </Row>

        {props.nodes.nodes.map((node, index) => (
          <Row key={index}>
            <Col md={6} className="projectsSection__imageCol">
                <img src={`${node.featured_media.localFile.childImageSharp.resolutions.src}`} alt={`${node.title}`}/>

                <div className="innerCol">
                    <img src={`${Logo}`} alt=""/>
                    <h4 dangerouslySetInnerHTML={{ __html: wrapFirstLetterInSpan(node.title, true) }} />
                </div>
            </Col>

            <Col md={6} className="projectsSection__contentCol">
              <h3 dangerouslySetInnerHTML={{ __html: wrapFirstLetterInSpan(node.title, false) }} />
              {node.acf.programy.map((program, index) => (
                <a key={index} href={`/program/${program.post_name}`} dangerouslySetInnerHTML={{ __html: program.post_content }} />
              ))}
            </Col>
          </Row>
        ))}
      </Container>
    </section>
  )
}

export default ProgramsSection
