import React from "react"
import { Container, Row, Col } from "react-bootstrap"

import Title from "../Title/title"
import Logo from "../../images/logo_footer.svg"
import "./programsSection.scss"

function ProgramsSection(props) {
  return (
    <section className="projectsSection">
      <Container>
        <Row>
          <Col md={12}>
            <Title text={props.title} mainTitle={false} />
          </Col>

          <Col md={12}>
            <div
              className="projectsSection__desc"
              dangerouslySetInnerHTML={{ __html: props.desc }}
            />
          </Col>
        </Row>

        {props.nodes.nodes.map((node, index) => (
            <Row key={index}>
                <Col md={6}>

                </Col>

                <Col md={6}>
                    
                </Col>
            </Row>
        ))}
      </Container>
    </section>
  )
}

export default ProgramsSection
