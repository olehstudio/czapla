import React from "react"
import { Container, Row, Col } from "react-bootstrap"

import Title from "../Title/title"
import "./programsSection.scss"

function ProgramsSection(props) {
  const generateKey = pre => {
    return `${pre}_${new Date().getTime()}`
  }

  return (
    <section className="projectsSection">
      <Container>
        <Row>
          <Col md={12}>
            <Title text={props.title} mainTitle={false} />
          </Col>

          <Col md={12}>
            <div
              className="projectsSection__desc"
              dangerouslySetInnerHTML={{ __html: props.desc }}
            />
          </Col>
        </Row>

        {props.nodes.nodes.map((node, index) => (
          <Row key={generateKey(index)}></Row>
        ))}
      </Container>
    </section>
  )
}

export default ProgramsSection
