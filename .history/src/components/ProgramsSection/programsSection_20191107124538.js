import React from "react"
import { Container, Row, Col } from "react-bootstrap"

import Title from "../Title/title"
import Logo from "../../images/logo_footer.svg"
import "./programsSection.scss"

function ProgramsSection(props) {
  return (
    <section className="projectsSection">
      <Container fluid={true}>
        <Row>
          <Col md={12}>
            <Title text={props.title} mainTitle={false} />
          </Col>

          <Col md={12}>
            <div
              className="projectsSection__desc"
              dangerouslySetInnerHTML={{ __html: props.desc }}
            />
          </Col>
        </Row>

        {props.nodes.nodes.map((node, index) => (
          <Row key={index}>
            <Col md={6} className="projectsSection__imageCol">
                <img src={`${node.featured_media.localFile.childImageSharp.resolutions.src}`} alt={`${node.title}`}/>

                <div className="innerCol">
                    <img src={`${Logo}`} alt=""/>
                    <h4 dangerouslySetInnerHTML={{ __html: node.title }} />
                </div>
            </Col>

            <Col md={6}>
              <h3 dangerouslySetInnerHTML={{ __html: node.title }} />
              {node.acf.programy.map((program, index) => (
                <a href={`/program/${program.post_name}`} dangerouslySetInnerHTML={{ __html: program.post_content }} />
              ))}
            </Col>
          </Row>
        ))}
      </Container>
    </section>
  )
}

export default ProgramsSection
