import React from "react"
import { Container, Row, Col } from "react-bootstrap"

import Title from "../Title/title"
import "./programsSection.scss"

function ProgramsSection(props) {
  return (
    <section className="projectsSection">
      <Container>
        <Col md={12}>
          <Title text={props.title} mainTitle={false} />
        </Col>

        <Col md={12}>
          <div dangerouslySetInnerHTML={{ __html: props.desc }} />
        </Col>
      </Container>
    </section>
  )
}

export default ProgramsSection
