import React from "react"
import { Link } from "gatsby"
import { Row, Col } from "react-bootstrap"
import moment from "moment"
import "moment/locale/pl"

import SeparatorIcon from "../../images/czapla_separator.svg"
import WhiteLogo from "../../images/logo_footer.svg"
import "./postCard.scss"

function postCard(props) {
  const excerptContent = text => {
    return text.substr(0, 300).concat("...")
  }

  return (
    <Col md={12} className="postCard">
      <Row>
        <Col md={12} lg={6}>
          {props.categories[0].slug !== "media-o-nas" ? (
            <Link to={`/${props.categories[0].slug}/${props.slug}`}>
              <img
                className="postCard__mainImage"
                src={props.image}
                alt={props.title}
              />
            </Link>
          ) : (
            <Link
              to={`/${props.categories[0].slug}/${props.slug}`}
              className="postCard__mediaWrapper"
            >
              <img src={`${WhiteLogo}`} className="postCard__mediaWrapper_czaplaLogo" alt="Czapla Logo" />
              <div className="postCard__mediaWrapper_overlay">
                <img
                  className=""
                  src={props.image}
                  alt={props.title}
                />
              </div>
            </Link>
          )}
        </Col>

        <Col md={12} lg={6} className="postCard__wrapper">
          <Link
            to={`/${props.categories[0].slug}/${props.slug}`}
            className="postCard__title"
          >
            <h2 dangerouslySetInnerHTML={{ __html: props.title }} />
          </Link>

          <div className="postCard__description">
            {moment(props.date)
              .locale("pl")
              .format("LL")}
            &nbsp;|&nbsp;
            {props.categories.map((category, index) => (
              <Link to={`/${category.slug}`} key={index}>
                {category.name}
              </Link>
            ))}
          </div>

          <div className="postCard__tags">
            {props.tags.map((tag, index) => (
              <Link to={`/tag/${tag.slug}`} key={index}>
                {tag.name} {index !== props.tags.length - 1 ? ", " : null}
              </Link>
            ))}
          </div>

          <div
            className="postCard__content"
            dangerouslySetInnerHTML={{
              __html: excerptContent(props.content),
            }}
          />

          <Link
            to={`/${props.categories[0].slug}/${props.slug}`}
            className="postCard__link"
          >
            Czytaj dalej
          </Link>
        </Col>

        {props.countStatus === "notLast" && (
          <Col md={12} className={`postCard__separator ${props.categories[0].slug === "media-o-nas" && "mediaCard"}`}>
            <div className="imageWrapper">
              <img src={`${SeparatorIcon}`} alt="Separator Icon" />
            </div>
          </Col>
        )}
      </Row>
    </Col>
  )
}

export default postCard
