import React from "react"
import { Container, Row, Col, Carousel } from "react-bootstrap"

import Arrow from "../ArrowComponent/arrowComponent"
import Title from "../Title/title"
import "./references.scss"

class References extends React.Component {
  constructor(props) {
    super(props)
  }

  render() {
    return (
      <section className="referencesSection">
        <Container>
          <Row>
            <Col md={12}>
              <Title text={this.props.title} mainTitle={false} />
            </Col>
            <Col className="p-0">
              <Carousel prevIcon={<Arrow />} nextIcon={<Arrow />}>
                {this.props.data.map((reference, index) => (
                  <Carousel.Item key={index}>
                    <div
                      dangerouslySetInnerHTML={{
                        __html: reference.reference_content,
                      }}
                      className="referencesSection__contentWrapper"
                    />
                    <div dangerouslySetInnerHTML={{ __html: reference.reference_author }} />
                  </Carousel.Item>
                ))}
              </Carousel>
            </Col>
          </Row>
        </Container>
      </section>
    )
  }
}

export default References
