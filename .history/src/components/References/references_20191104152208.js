import React from "react"
import { Container, Row, Col, Carousel } from "react-bootstrap"

import Arrow from "../ArrowComponent/arrowComponent"
import Title from "../Title/title"
import "./references.scss"

class References extends React.Component {
  constructor(props) {
    super(props)

    this.state = {
      carouselHeight: 0,
    }

    this.myDiv = React.createRef()
  }

  componentDidMount() {
    this.setState({ carouselHeight: this.myDiv.current.offsetHeight })
  }

  render() {
    return (
      <section className="referencesSection">
        <Container>
          <Row>
            <Col md={12}>
              <Title text={this.props.title} mainTitle={false} />
            </Col>
            <Col className="p-0" ref={this.myDiv}>
              <Carousel
                prevIcon={<Arrow />}
                nextIcon={<Arrow />}
                pauseOnHover={true}
                style={{
                  height: `${this.state.carouselHeight}px`
                }}
              >
                {this.props.data.map((reference, index) => (
                  <Carousel.Item key={index}>
                    <div
                      dangerouslySetInnerHTML={{
                        __html: reference.reference_content,
                      }}
                      className="referencesSection__contentWrapper"
                    />
                    <div
                      dangerouslySetInnerHTML={{
                        __html: reference.reference_author,
                      }}
                      className="referencesSection__author"
                    />
                  </Carousel.Item>
                ))}
              </Carousel>
            </Col>
          </Row>
        </Container>
      </section>
    )
  }
}

export default References
