import React from "react"
import { StaticQuery, graphql, Link } from "gatsby"
import { Container, Row, Col, Carousel } from "react-bootstrap"

import Title from "../Title/title"
import "./partners.scss"

class Partners extends React.Component {
  constructor(props) {
    super(props)
  }

  render() {
    return (
      <Container>
        <Row>
          <Col>
            <Title text={this.props.title} mainTitle={true} />
          </Col>
        </Row>
      </Container>
    )
  }
}

export default Partners
