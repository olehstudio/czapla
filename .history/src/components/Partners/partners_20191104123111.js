import React from "react"
import { StaticQuery, graphql, Link } from "gatsby"
import { Container, Row, Col, Carousel } from "react-bootstrap"

import Title from "../Title/title"
import "./partners.scss"

class Partners extends React.Component {
  constructor(props) {
    super(props)
  }

  render() {
    return (
      <Container>
        <Row>
          <Col md={12}>
            <Title text={this.props.title} mainTitle={false} />
          </Col>

          {this.props.data.map((partner, index) => (
            <Col md={3} key={index}>
              <a href={partner.partner_link} target="_blank">
                <img src={partner.partner_image.localFile.childImageSharp.resolutions.src} alt="Partner" />
              </a>
            </Col>
          ))}
        </Row>
      </Container>
    )
  }
}

export default Partners
