import React from "react"
import { Container, Row, Col } from "react-bootstrap"

import Title from "../Title/title"
import "./partners.scss"

class Partners extends React.Component {
  render() {
    return (
      <section className="partnerSection">
        <Container>
          <Row>
            <Col md={12}>
              <Title text={this.props.title} mainTitle={false} />
            </Col>

            {this.props.data.map((partner, index) => (
              <Col sm={6} md={3} key={index}>
                <a
                  href={partner.partner_link}
                  rel="noopener noreferrer"
                  target="_blank"
                  className="partnerSection__singlePartner"
                >
                  <img
                    src={
                      partner.partner_image.localFile.childImageSharp
                        .resolutions.src
                    }
                    alt="Partner"
                  />
                </a>
              </Col>
            ))}
          </Row>
        </Container>
      </section>
    )
  }
}

export default Partners
