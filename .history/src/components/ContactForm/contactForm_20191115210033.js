import React from "react"
import axios from "axios"
import queryString from "query-string"
import TextField from "@material-ui/core/TextField"
import CircularProgress from "@material-ui/core/CircularProgress"
import { Col } from "react-bootstrap"

import "./contactForm.scss"

class ContactForm extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      name: "",
      email: " ",
      subject: "",
      message: "",
      validEmail: "",
      isError: false,
      errorMessage: "",
      isSubmitting: false,
      isSent: false,
      sentMessage: "",
    }
  }

  componentDidMount() {
    const parsed = queryString.parse(window.location.search)
    this.setState({ subject: parsed.program })
  }

  handleChangeEmail = event => {
    this.setState({ email: event.target.value })
    const validEmailRegex =
        RegExp(/^(([^<>()\[\]\.,;:\s@\"]+(\.[^<>()\[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i);
    this.setState({ email: event.target.value });

    validEmailRegex.test(event.target.value) ? this.setState({ validEmail: true }) : this.setState({ validEmail: false })
}

  handleFormSubmit(event) {
    event.preventDefault()
    this.setState({
      isError: false,
      errorMessage: "",
      isSubmitting: !this.state.isSubmitting,
    })

    let formData = new FormData()
    formData.append("formName", this.state.name)
    formData.append("formEmail", this.state.email)
    formData.append("formMessage", this.state.message)

    axios({
      method: "post",
      url:
        "http://apirula.website/wp-json/contact-form-7/v1/contact-forms/61/feedback",
      data: formData,
      config: { headers: { "Content-Type": "multipart/form-data" } },
    })
      .then(response => {
        if (response.data.status === "mail_sent") {
          this.setState({
            isSent: !this.state.isSent,
            sentMessage: response.data.message,
            isSubmitting: !this.state.isSubmitting,
          })
        } else if (response.data.status === "validation_failed") {
          this.setState({
            isError: !this.state.isError,
            errorMessage: response.data.message,
            isSubmitting: !this.state.isSubmitting,
          })
        }
      })
      .catch(response => {
        console.log(response)
      })
  }

  render() {
    return (
      <Col lg={{ span: 6, offset: 3 }}>
        <form className="contactForm">
          <div className="formField formField__name">
            <TextField
              label="imię i nazwisko*"
              autoComplete="off"
              type="text"
              name="name"
              value={this.state.name}
              variant="filled"
              disabled={this.state.isSent ? true : false}
              onChange={e => this.setState({ name: e.target.value })}
            />
          </div>

          <div className={`formField formField__email ${this.state.validEmail ? "" : "errorField"}`}>
            <TextField
              label="e-mail*"
              autoComplete="off"
              type="email"
              name="email"
              value={this.state.email}
              variant="filled"
              disabled={this.state.isSent ? true : false}
              onChange={this.handleChangeEmail}
            />
          </div>

          <div className="formField formField__subject">
            <TextField
              label="temat*"
              autoComplete="off"
              type="text"
              name="subject"
              value={this.state.subject}
              variant="filled"
              disabled={this.state.isSent ? true : false}
              onChange={e => this.setState({ subject: e.target.value })}
            />
          </div>

          <div className="formField formField__message">
            <TextField
              label="Wiadomość*"
              multiline
              name="message"
              value={this.state.message}
              variant="filled"
              disabled={this.state.isSent ? true : false}
              onChange={e => this.setState({ message: e.target.value })}
            />
          </div>

          <p style={{ color: "red" }}> {this.state.errorMessage} </p>

          {this.state.isSubmitting ? (
            <CircularProgress color="#DA2B2F" />
          ) : this.state.isSent ? (
            <p
              style={{
                width: "100%",
                textAlign: "center",
                color: "#fff",
                fontWeight: "500",
                letterSpacing: "0.05rem",
                fontSize: "18px",
              }}
            >
              {" "}
              {this.state.sentMessage}{" "}
            </p>
          ) : (
            <input
              className="czaplaLink"
              type="submit"
              onClick={e => this.handleFormSubmit(e)}
              value="wyślij forlmularz"
            />
          )}
        </form>
      </Col>
    )
  }
}

export default ContactForm
