import React, { Component } from 'react';
import axios from 'axios';

import { Container, Row, Col, Form, Button } from "react-bootstrap"

import './contact-form.scss'
import Loader from "../Loader/loader"

class ContactForm extends Component {
    constructor(props) {
        super(props);
        this.state = {
            loading: false,
            response: null,
            name: null,
            email: null,
            tel: null,
            agree: null,
            theme: null,
            description: null,
            progress: null,
            valid: null,
            validation: {
                name: null,  //*
                email: null,  //*
                tel: null,
                agree: null,  //*
                theme: null,  //*
                description: null, //*
            }
        };
    }

    handleValidation = () => {
        if (this.state.validation.name === true
            && this.state.validation.email === true
            && this.state.validation.agree === true
            && this.state.validation.theme === true
            && this.state.validation.description === true
        ) {
            this.setState({ valid: true })
            return true
        }
        else {
            return false
        }
    }

    handleSubmit = (event, fieldName, file, metadata, load, error, progress, abort) => {
        event.preventDefault();
        if (!this.handleValidation()) {
            this.setState({ valid: false })
            return false
        }

        this.setState({ loading: true })

        let data = new FormData();
        data.append('your-name', this.state.name);
        data.append('your-email', this.state.email);
        data.append('tel-46', this.state.tel);
        data.append('acceptance-194', this.state.agree);
        data.append('your-subject', this.state.theme);
        data.append('your-message', this.state.description);

        axios({
            method: 'post',
            url: 'http://wp-proyal.pixelart.pl/wp-json/contact-form-7/v1/contact-forms/488/feedback',
            data: data,
            config: { headers: { 'Content-Type': 'multipart/form-data' } },
            onUploadProgress: (e) => {
                // updating progress indicator
                // progress(e.lengthComputable, e.loaded, e.total);
                this.setState({ progress: e })
            }
        })
            .then((response) => this.setState(
                {
                    loading: false,
                    response: response
                }
            ))

            .catch((response) => this.setState(
                {
                    loading: false,
                    response: response
                }
            ))
    }

    handleChangeName = event => {
        this.setState({ name: event.target.value });
        if (event.target.value.length >= 2) {
            this.setState(prevState => ({
                validation: {
                    ...prevState.validation,
                    name: true
                }
            }))
        } else if (event.target.value.length < 2) {
            this.setState(prevState => ({
                validation: {
                    ...prevState.validation,
                    name: false
                }
            }))
        }
    }

    handleChangeEmail = event => {

        const validEmailRegex =
            RegExp(/^(([^<>()\[\]\.,;:\s@\"]+(\.[^<>()\[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i);
        this.setState({ email: event.target.value });

        if (validEmailRegex.test(event.target.value)) {
            this.setState(prevState => ({
                validation: {
                    ...prevState.validation,
                    email: true
                }
            }))
        } else {
            this.setState(prevState => ({
                validation: {
                    ...prevState.validation,
                    email: false
                }
            }))
        }
    }

    handleChangeTel = event => {
        const validPhoneRegex = RegExp(/^\(?[+]?([0-9]{2})?[-. ]?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{3})$/);
        this.setState({ tel: event.target.value })
        if (validPhoneRegex.test(event.target.value)) {
            this.setState(prevState => ({
                validation: {
                    ...prevState.validation,
                    tel: true
                }
            }))
        } else {
            this.setState(prevState => ({
                validation: {
                    ...prevState.validation,
                    tel: false
                }
            }))
        }
    }

    handleChangeAgree = event => {
        this.setState({ agree: event.target.checked })
        if (event.target.checked === true)
            this.setState(prevState => ({
                validation: {
                    ...prevState.validation,
                    agree: true
                }
            }))
        else
            this.setState(prevState => ({
                validation: {
                    ...prevState.validation,
                    agree: false
                }
            }))
    }

    handleChangeTheme = event => {
        this.setState({ theme: event.target.value });
        if (event.target.value.length >= 2) {
            this.setState(prevState => ({
                validation: {
                    ...prevState.validation,
                    theme: true
                }
            }))
        } else if (event.target.value.length < 2) {
            this.setState(prevState => ({
                validation: {
                    ...prevState.validation,
                    theme: false
                }
            }))
        }
    }

    handleChangeDescription = event => {
        this.setState({ description: event.target.value });
        if (event.target.value.length >= 10) {
            this.setState(prevState => ({
                validation: {
                    ...prevState.validation,
                    description: true
                }
            }))
        } else if (event.target.value.length < 10) {
            this.setState(prevState => ({
                validation: {
                    ...prevState.validation,
                    description: false
                }
            }))
        }

    }

    // valid__form
    // error__form
    // active__form

    render() {
        return (
            <Form onSubmit={this.handleSubmit} className="form-contact">

                <Container fluid={true}>
                    {this.state.loading ? <Loader /> : null}
                    <Row>
                        <Col md={6}>
                            <Form.Group as={Row} controlId="formPlaintextPassword">
                                <Form.Label column xl="3">
                                    {this.props.fieldName &&
                                        this.props.fieldName.name_contact &&
                                        this.props.fieldName.name_contact
                                    }*
                                </Form.Label>
                                <Col xl={7}>
                                    <Form.Control
                                        type="text"
                                        onChange={this.handleChangeName}
                                        className={this.state.validation.name === null ? ' '
                                            : this.state.validation.name === true ? 'valid__form'
                                                : this.state.validation.name === false ? 'error__form' : 'active__form'}
                                    />
                                </Col>
                            </Form.Group>
                            <Form.Group as={Row} controlId="formPlaintextPassword">
                                <Form.Label column xl="3">
                                    {this.props.fieldName &&
                                        this.props.fieldName.e_mail_contact &&
                                        this.props.fieldName.e_mail_contact
                                    }*
                                </Form.Label>
                                <Col xl={7}>
                                    <Form.Control
                                        type="email"
                                        onChange={this.handleChangeEmail}
                                        className={this.state.validation.email === null ? ' '
                                            : this.state.validation.email === true ? 'valid__form'
                                                : this.state.validation.email === false ? 'error__form' : 'active__form'
                                        }
                                    />
                                </Col>
                            </Form.Group>
                            <Form.Group as={Row} controlId="formPlaintextPassword">
                                <Form.Label column xl="3">
                                    {this.props.fieldName &&
                                        this.props.fieldName.telefon_contact &&
                                        this.props.fieldName.telefon_contact
                                    }
                                </Form.Label>
                                <Col xl={7}>
                                    <Form.Control
                                        type="tel"
                                        onChange={this.handleChangeTel}
                                        className={this.state.validation.tel === null ? ' '
                                            : this.state.validation.tel === true ? 'valid__form'
                                                : this.state.validation.tel === false ? 'active__form' : 'error__form'
                                        }
                                    />
                                </Col>
                            </Form.Group>

                        </Col>
                        <Col md={6}>
                            <Form.Group as={Row} controlId="formPlaintextPassword">
                                <Form.Label column xl="3">
                                    {this.props.fieldName &&
                                        this.props.fieldName.theme_contact &&
                                        this.props.fieldName.theme_contact
                                    }*
                                </Form.Label>
                                <Col xl={7}>
                                    <Form.Control type="text" onChange={this.handleChangeTheme}
                                        className={this.state.validation.theme === null ? ' '
                                            : this.state.validation.theme === true ? 'valid__form'
                                                : this.state.validation.theme === false ? 'error__form' : 'active__form'}
                                    />
                                </Col>
                            </Form.Group>
                            <Form.Group as={Row} controlId="exampleForm.ControlTextarea1">
                                <Form.Label column xl="3">
                                    {this.props.fieldName &&
                                        this.props.fieldName.description_contact &&
                                        this.props.fieldName.description_contact
                                    }*
                                </Form.Label>
                                <Col xl={7}>
                                    <Form.Control
                                        as="textarea"
                                        rows="3"
                                        onChange={this.handleChangeDescription}
                                        className={this.state.validation.description === null ? ' '
                                            : this.state.validation.description === true ? 'valid__form'
                                                : this.state.validation.description === false ? 'error__form' : 'active__form'}
                                    />
                                </Col>
                            </Form.Group>
                        </Col>
                        <Col md={6}>
                            <Form.Group as={Row} controlId="formPlaintextPassword">
                                <label className="lebel" >*
                                    {this.props.fieldName &&
                                        this.props.fieldName.ruls_contact &&
                                        this.props.fieldName.ruls_contact
                                    }
                                </label>
                            </Form.Group>
                            <Form.Group as={Row} controlId="formPlaintextPassword" className="label-check">
                                <Form.Check
                                    label={this.props.fieldName &&
                                        this.props.fieldName.i_agree_contact &&
                                        this.props.fieldName.i_agree_contact
                                    }
                                    onChange={this.handleChangeAgree}
                                    className={this.state.validation.agree === null ? ' '
                                        : this.state.validation.agree === true ? 'valid__form'
                                            : this.state.validation.agree === false ? 'error__form' : 'active__form'
                                    }
                                />
                            </Form.Group>
                        </Col>
                        <Col md={6}>
                            <Form.Group as={Row} className="group-flex-end">
                                <Col xl={7}>
                                    <Button
                                        variant="outline-dark"
                                        type="submit"
                                        className={this.state.valid ? 'valid__form__ button-submit' :
                                            this.state.valid === null ? 'button-submit' : 'error_button__ button-submit'}
                                    >
                                        {this.props.fieldName &&
                                            this.props.fieldName.send_contact &&
                                            this.props.fieldName.send_contact
                                        }
                                    </Button>
                                </Col>
                            </Form.Group>
                        </Col>
                    </Row>
                </Container>
            </Form>
        );
    }
}

export default ContactForm