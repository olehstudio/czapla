import React from "react"
import axios from "axios"
import queryString from "query-string"
import TextField from "@material-ui/core/TextField"
import CircularProgress from "@material-ui/core/CircularProgress"
import { Col } from "react-bootstrap"

import "./contactForm.scss"

class ContactForm extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      name: "",
      email: "",
      subject: "",
      message: "",
      validEmail: "",
      acceptance_1: false,
      acceptance_2: false,
      validForm: false,
      isSubmitting: false,
      isSent: false,
      sentMessage: "",
    }
  }

  componentDidMount() {
    const parsed = queryString.parse(window.location.search)
    this.setState({ subject: parsed.program })
  }

  handleFormStatus() {
    return this.state.name.length > 0 &&
      this.state.validEmail &&
      this.state.subject.length > 0 &&
      this.state.message.length > 0 &&
      this.state.acceptance_1 &&
      this.state.acceptance_2
      ? this.setState({ validForm: true })
      : this.setState({ validForm: false })
  }

  hangleChangeName = event => {
    this.setState({ name: event.target.value }, () => this.handleFormStatus())
  }

  handleChangeEmail = event => {
    this.setState({ email: event.target.value })
    const validEmailRegex = RegExp(
      /^(([^<>()\[\]\.,;:\s@\"]+(\.[^<>()\[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i
    )
    validEmailRegex.test(event.target.value)
      ? this.setState({ validEmail: true }, () => this.handleFormStatus())
      : this.setState({ validEmail: false }, () => this.handleFormStatus())
  }

  hangleChangeSubject = event => {
    this.setState({ subject: event.target.value }, () =>
      this.handleFormStatus()
    )
  }

  hangleChangeMessage = event => {
    this.setState({ message: event.target.value }, () =>
      this.handleFormStatus()
    )
  }

  handleCheckbox1 = event => {
    this.setState({ acceptance_1: event.target.checked }, () =>
      this.handleFormStatus()
    )
  }

  handleCheckbox2 = event => {
    this.setState({ acceptance_2: event.target.checked }, () =>
      this.handleFormStatus()
    )
  }

  handleFormSubmit(event) {
    event.preventDefault()
    this.setState({
      isSubmitting: !this.state.isSubmitting,
    })

    let formData = new FormData()
    formData.append("formName", this.state.name)
    formData.append("formEmail", this.state.email)
    formData.append("formSubject", this.state.subject)
    formData.append("formMessage", this.state.message)

    axios({
      method: "post",
      url:
        "https://czaplaandmore.pixelart.pl/wp-json/contact-form-7/v1/contact-forms/289/feedback",
      data: formData,
      config: { headers: { "Content-Type": "multipart/form-data" } },
    })
      .then(response => {
        if (response.data.status === "mail_sent") {
          this.setState({
            isSent: !this.state.isSent,
            sentMessage: response.data.message,
            isSubmitting: !this.state.isSubmitting,
          })
        } else if (response.data.status === "validation_failed") {
          // this.setState({
          //   isError: !this.state.isError,
          //   errorMessage: response.data.message,
          //   isSubmitting: !this.state.isSubmitting,
          // })
          console.log(response)
        }
      })
      .catch(response => {
        console.log(response)
      })
  }

  render() {
    return (
      <Col lg={{ span: 6, offset: 3 }}>
        <form className="contactForm">
          <div className="formField formField__name">
            <TextField
              label="imię i nazwisko*"
              autoComplete="off"
              type="text"
              name="name"
              value={this.state.name}
              variant="filled"
              disabled={this.state.isSent ? true : false}
              onChange={this.hangleChangeName}
            />
          </div>

          <div
            className={`formField formField__email ${
              !this.state.validEmail && this.state.email.length > 0
                ? "errorField"
                : ""
            }`}
          >
            <TextField
              label="e-mail*"
              autoComplete="off"
              type="email"
              name="email"
              value={this.state.email}
              variant="filled"
              disabled={this.state.isSent ? true : false}
              onChange={this.handleChangeEmail}
            />
          </div>

          <div className="formField formField__subject">
            <TextField
              label="temat*"
              autoComplete="off"
              type="text"
              name="subject"
              value={this.state.subject}
              variant="filled"
              disabled={this.state.isSent ? true : false}
              onChange={this.hangleChangeSubject}
            />
          </div>

          <div className="formField formField__message">
            <TextField
              label="Wiadomość*"
              multiline
              name="message"
              value={this.state.message}
              variant="filled"
              disabled={this.state.isSent ? true : false}
              onChange={this.hangleChangeMessage}
            />
          </div>

          <p>
            <span>*</span> Pola konieczne do wysłania wiadomości
          </p>
          <p>
            Wyrażam zgodę na przetwarzanie moich <a href="">danych osobowych</a>{" "}
            przez Coach and More Sp. z o.o. w celu:
          </p>

          <div className="formField__checkboxWrapper">
            <input
              type="checkbox"
              id="acceptance_1"
              onChange={this.handleCheckbox1}
            />
            <label htmlFor="acceptance_1">
              przesyłania za pomocą środków komunikacji elektronicznej
              informacji handlowych w rozumieniu ustawy z dnia 18 lipca 2002 r.
              o świadczeniu usług drogą elektroniczną (Dz. U. z 2002 r., Nr 144,
              poz. 104 z późn. zm.),<span>*</span>
            </label>
          </div>

          <div className="formField__checkboxWrapper">
            <input
              type="checkbox"
              id="acceptance_2"
              onChange={this.handleCheckbox2}
            />
            <label htmlFor="acceptance_2">
              telefonicznego marketingu bezpośredniego własnych produktów lub
              usług.
            </label>
          </div>

          {this.state.isSubmitting ? (
            <CircularProgress color="#DA2B2F" />
          ) : this.state.isSent ? (
            <p
              style={{
                width: "100%",
                textAlign: "center",
                color: "#fff",
                fontWeight: "500",
                letterSpacing: "0.05rem",
                fontSize: "18px",
              }}
            >
              {" "}
              {this.state.sentMessage}{" "}
            </p>
          ) : (
            <input
              className="czaplaLink"
              type="submit"
              onClick={e => this.handleFormSubmit(e)}
              value="wyślij forlmularz"
              disabled={this.state.validForm ? false : "disabled"}
            />
          )}
        </form>
      </Col>
    )
  }
}

export default ContactForm
