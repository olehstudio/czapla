import React from "react"
import { StaticQuery, graphql, Link } from "gatsby"

import { Navbar  } from 'react-bootstrap';

class header extends React.Component {
  constructor(propsData){
    super(props)
    this.state = {}
  }

  render(){
    return (
      
    )
  }
}

export default () => {
  <StaticQuery
    query={graphql`
      query {
        wordpressWpApiMenusMenusItems(name: { eq: "Menu" }) {
          items {
            title
            object_slug
          }
        }
      }
    `}
    render={data => (
      <Header menuItems={data.wordpressWpApiMenusMenusItems.items} />
    )}
  />
}