import React from "react"

import TitleLogo from "../../images/title_logo.svg"
import "./programTitle.scss"

function ProgramTitle(props) {
    const wrapFirstLetterInSpan = (string, br = false) => {
        let tempArr = string.split(" ")
        let brOutput = br ? "<br/>" : ""
        tempArr[0] = "<span>" + tempArr[0] + "</span>" + brOutput
        return tempArr.join(" ")
    }

    const wrapLastLetterInSpan = (string, br = false) => {
        let tempArr = string.split(" ")
        let brOutput = br ? "<br/>" : ""
        tempArr[tempArr.length - 1] = "<span>" + tempArr[tempArr.length - 1] + "</span>" + brOutput
        return tempArr.join(" ")
    }

    return (
        <>
            {props.mainTitle ? (
                <div className="programTitleWrapper">
                    <img src={`${TitleLogo}`} alt="" />
                    <h2 className="programTitleWrapper__mainProgramTitle" dangerouslySetInnerHTML={{ __html: wrapFirstLetterInSpan(props.mainProgramTitle) }} />
                    <h1 className="programTitleWrapper__title" dangerouslySetInnerHTML={{ __html: wrapLastLetterInSpan(props.text) }} />
                </div>
            ) : (
                    <div className="programTitleWrapper">
                        <img src={`${TitleLogo}`} alt="" />
                        <h3 className="programTitleWrapper__mainProgramTitle" dangerouslySetInnerHTML={{ __html: wrapFirstLetterInSpan(props.mainProgramTitle) }} />
                        <h2 className="programTitleWrapper__title" dangerouslySetInnerHTML={{ __html: wrapLastLetterInSpan('W ramach' + props.text) }} />
                    </div>
                )}
        </>
    )
}

export default ProgramTitle
