import React from "react"
import { StaticQuery, graphql, Link } from "gatsby"
import { Container, Navbar, Nav, Row } from "react-bootstrap"

import "./newsletter.scss"
import Title from "../Title/title"

class Newsletter extends React.Component {
  constructor(props) {
    super(props)
    this.state = {}
  }

  render() {
    return (
      <section className="newsletterSection">
        <div
          style={{
            display: "block",
            width: "100vw",
          }}
        >
          <Title text={this.props.data.newsletter_title} mainTitle={false} />
        </div>
      </section>
    )
  }
}

export default () => (
  <StaticQuery
    query={graphql`
      query {
        allWordpressAcfOptions {
          nodes {
            websiteSettings {
              newsletter_title
              newsletter_nested_title
            }
          }
        }
      }
    `}
    render={data => (
      <Newsletter data={data.allWordpressAcfOptions.nodes[0].websiteSettings} />
    )}
  />
)
