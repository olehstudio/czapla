import React from "react"
import { StaticQuery, graphql, Link } from "gatsby"
import { Container, Navbar, Nav, Row } from "react-bootstrap"

import MenuItem from "./menuItem"

import "./newsletter.scss"
import Title from "../../Title/title"

class Header extends React.Component {
  constructor(props){
    super(props)
    this.state = {}
  }

  render(){
    return (
      
    )
  }
}

export default () => (
    <StaticQuery
      query={graphql`
        query {
          allWordpressAcfOptions {
            nodes {
              websiteSettings {
                newsletter_title
                newsletter_nested_title
              }
            }
          }
        }
      `}
      render={data => (
        <Newsletter content={data.allWordpressAcfOptions.nodes[0].websiteSettings} />
      )}
    />
  )