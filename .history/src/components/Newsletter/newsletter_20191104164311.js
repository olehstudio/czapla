import React from "react"
import { StaticQuery, graphql, Link } from "gatsby"
import { Container, Row, Col } from "react-bootstrap"
import TextField from "@material-ui/core/TextField"

import "./newsletter.scss"
import CzaplaImage from "../../images/czapla_newsletter.jpg"
import Title from "../Title/title"

class Newsletter extends React.Component {
  constructor(props) {
    super(props)
    this.state = {}
  }

  render() {
    return (
      <section className="newsletterSection">
        <Container>
          <Row>
            <Col md={12}>
              <Title
                text={this.props.data.newsletter_title}
                mainTitle={false}
              />
            </Col>
          </Row>
        </Container>

        <Container fluid={true} className="newsletterSection__wrapper">
          <Row>
            <Col md={3}>
              <img src={`${CzaplaImage}`} alt="Czapla" />
            </Col>
            <Col md={9} className="newsletterSection__content">
              <h3
                dangerouslySetInnerHTML={{
                  __html: this.props.data.newsletter_nested_title,
                }}
              />

              <form action="">
                <TextField id="" className="" label="Imię" margin="normal" />
                <TextField id="" className="" label="Adres email" margin="normal" />
              </form>
            </Col>
          </Row>
        </Container>
      </section>
    )
  }
}

export default () => (
  <StaticQuery
    query={graphql`
      query {
        allWordpressAcfOptions {
          nodes {
            websiteSettings {
              newsletter_title
              newsletter_nested_title
            }
          }
        }
      }
    `}
    render={data => (
      <Newsletter data={data.allWordpressAcfOptions.nodes[0].websiteSettings} />
    )}
  />
)
