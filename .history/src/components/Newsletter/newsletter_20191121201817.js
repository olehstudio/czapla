import React from "react"
import axios from "axios"
import { StaticQuery, graphql } from "gatsby"
import { Container, Row, Col } from "react-bootstrap"
import TextField from "@material-ui/core/TextField"
import CircularProgress from "@material-ui/core/CircularProgress"

import CzaplaImage from "../../images/czapla_newsletter.jpg"
import Title from "../Title/title"
import "./newsletter.scss"

class Newsletter extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      name: "",
      email: "",
      isError: false,
      errorText: "",
      isSubmitting: false,
      isSent: false,
      sentMessage: "",
    }
  }

  handleFormSubmit(event) {
    event.preventDefault()
    this.setState({
      isError: false,
      isSubmitting: true,
    })

    if (
      this.state.email.match(/^([\w.%+-]+)@([\w-]+\.)+([\w]{2,})$/i) &&
      this.state.name.length > 0
    ) {
      axios({
        method: "POST",
        url:
          "https://czaplaandmore.pixelart.pl/wp-json/newsletterSubscription/v2",
        data: {
          name: this.state.name,
          email: this.state.email,
        },
      }).then(response => {
        this.setState({
          isSent: true,
          sentMessage: "Proszę o potwierdzenie subskrybcji przez mail",
          isSubmitting: false,
        })
      })
    } else {
      this.setState({
        isError: true,
        errorText: "Poprawnie wypełnij wszystkie pola",
        isSubmitting: false,
      })
    }
  }

  render() {
    return (
      <section className="newsletterSection">
        <Container>
          <Row>
            <Col md={12}>
              <Title
                text={this.props.data.newsletter_title}
                mainTitle={false}
              />
            </Col>
          </Row>
        </Container>

        <Container fluid={true} className="newsletterSection__wrapper">
          <Row>
            <Col md={12} lg={3}>
              <img src={`${CzaplaImage}`} alt="Czapla" />
            </Col>
            <Col md={12} lg={9} className="newsletterSection__content">
              <h3
                dangerouslySetInnerHTML={{
                  __html: this.props.data.newsletter_nested_title,
                }}
              />

              <form action="">
                <TextField
                  id="newsletter_name"
                  className=""
                  label="Imię"
                  margin="normal"
                  style={{ width: "25%" }}
                  disabled={this.state.isSent ? true : false}
                  onChange={e => this.setState({ name: e.target.value })}
                />
                <TextField
                  id="newsletter_email"
                  className=""
                  type="email"
                  label="Adres email"
                  margin="normal"
                  style={{
                    marginLeft: "30px",
                    width: "45%",
                  }}
                  disabled={this.state.isSent ? true : false}
                  onChange={e => this.setState({ email: e.target.value })}
                />
                {this.state.isSubmitting ? (
                  <CircularProgress />
                ) : (
                  <input
                    type="submit"
                    disabled={this.state.isSent ? true : false}
                    onClick={e => this.handleFormSubmit(e)}
                    value="Zapisuję się"
                  />
                )}
              </form>
              {this.state.isError ? (
                <p className="errorNotification">{this.state.errorText}</p>
              ) : null}
              {this.state.isSent ? (
                <p className="sentNotification">{this.state.sentMessage}</p>
              ) : null}
            </Col>
          </Row>
        </Container>
      </section>
    )
  }
}

export default () => (
  <StaticQuery
    query={graphql`
      query {
        allWordpressAcfOptions {
          nodes {
            websiteSettings {
              newsletter_title
              newsletter_nested_title
            }
          }
        }
      }
    `}
    render={data => (
      <Newsletter data={data.allWordpressAcfOptions.nodes[0].websiteSettings} />
    )}
  />
)
