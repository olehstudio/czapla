import React from "react"
import axios from "axios"
import { StaticQuery, graphql, Link } from "gatsby"
import { Container, Row, Col } from "react-bootstrap"
import TextField from "@material-ui/core/TextField"

import CzaplaImage from "../../images/czapla_newsletter.jpg"
import Title from "../Title/title"
import "./newsletter.scss"
import { request } from "http"

class Newsletter extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      name: '',
      email: '',
      isError: false,
      isSubmitting: false,
    }
  }

  handleFormSubmit(event) {
    event.preventDefault()
    this.setState({
      isError: false,
      isSubmitting: !this.state.isSubmitting,
    })

    axios({
      method: "POST",
      url: "https://czaplaandmore.pixelart.pl/",
      data: {
        name: this.state.name,
        email: this.state.email
      }
    }).then(response => {
      console.log(response)
    })
  }

  render() {
    return (
      <section className="newsletterSection">
        <Container>
          <Row>
            <Col md={12}>
              <Title
                text={this.props.data.newsletter_title}
                mainTitle={false}
              />
            </Col>
          </Row>
        </Container>

        <Container fluid={true} className="newsletterSection__wrapper">
          <Row>
            <Col md={3}>
              <img src={`${CzaplaImage}`} alt="Czapla" />
            </Col>
            <Col md={9} className="newsletterSection__content">
              <h3
                dangerouslySetInnerHTML={{
                  __html: this.props.data.newsletter_nested_title,
                }}
              />

              <form action="">
                <TextField
                  id="newsletter_name"
                  className=""
                  label="Imię"
                  margin="normal"
                  style={{ width: "25%" }}
                  onChange={e => this.setState({ name: e.target.value })}
                />
                <TextField
                  id="newsletter_email"
                  className=""
                  type="email"
                  label="Adres email"
                  margin="normal"
                  style={{
                    marginLeft: "30px",
                    width: "45%",
                  }}
                  onChange={e => this.setState({ email: e.target.value })}
                />
                <input
                  type="submit"
                  onClick={e => this.handleFormSubmit(e)}
                  value="Zapisuję się"
                />
              </form>
            </Col>
          </Row>
        </Container>
      </section>
    )
  }
}

export default () => (
  <StaticQuery
    query={graphql`
      query {
        allWordpressAcfOptions {
          nodes {
            websiteSettings {
              newsletter_title
              newsletter_nested_title
            }
          }
        }
      }
    `}
    render={data => (
      <Newsletter data={data.allWordpressAcfOptions.nodes[0].websiteSettings} />
    )}
  />
)
