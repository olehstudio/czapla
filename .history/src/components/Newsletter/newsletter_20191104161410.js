import React from "react"
import { StaticQuery, graphql, Link } from "gatsby"
import { Container, Navbar, Nav, Row } from "react-bootstrap"

import "./newsletter.scss"
import CzaplaImage from "../../images/czapla_newsletter"
import Title from "../Title/title"

class Newsletter extends React.Component {
  constructor(props) {
    super(props)
    this.state = {}
  }

  render() {
    return (
      <section className="newsletterSection">
        <Container>
          <Row>
            <Col md={12}>
              <Title
                text={this.props.data.newsletter_title}
                mainTitle={false}
              />
            </Col>
          </Row>
        </Container>
        <Container fluid={true}>
          <Row>
            <Col md={12}>
              <Title
                text={this.props.data.newsletter_title}
                mainTitle={false}
              />
            </Col>
          </Row>
        </Container>
      </section>
    )
  }
}

export default () => (
  <StaticQuery
    query={graphql`
      query {
        allWordpressAcfOptions {
          nodes {
            websiteSettings {
              newsletter_title
              newsletter_nested_title
            }
          }
        }
      }
    `}
    render={data => (
      <Newsletter data={data.allWordpressAcfOptions.nodes[0].websiteSettings} />
    )}
  />
)
