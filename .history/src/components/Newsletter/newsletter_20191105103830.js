import React from "react"
import axios from "axios"
import { StaticQuery, graphql, Link } from "gatsby"
import { Container, Row, Col } from "react-bootstrap"
import TextField from "@material-ui/core/TextField"

import CzaplaImage from "../../images/czapla_newsletter.jpg"
import Title from "../Title/title"
import "./newsletter.scss"
import { request } from "http"

class Newsletter extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      isError: false,
      isSubmitting: false,
    }
  }

  handleFormSubmit(event) {
    event.preventDefault()
    this.setState({
      isError: false,
      isSubmitting: !this.state.isSubmitting,
    })

    let requestData = {}

    axios({
      method: "POST",
      url: "https://api.getresponse.com/v3/contacts",
    //   crossDomain: true,
      data: {
        name: "Oleh Rula",
        email: "oleh2@studiograficzne.com",
        dayOfCycle: "6",
        campaign: {
          campaignId: "8OUpI"
        }
      },
      config: {
        headers: {
          "Content-Type": "application/json",
          "X-Auth-Token": "api-key 66ad4df2998c86a9800f61ae43074c5b",
          "Access-Control-Allow-Origin": "*"
        }
      }
    }).then(response => {
      console.log(response)
    })
  }

  render() {
    return (
      <section className="newsletterSection">
        <Container>
          <Row>
            <Col md={12}>
              <Title
                text={this.props.data.newsletter_title}
                mainTitle={false}
              />
            </Col>
          </Row>
        </Container>

        <Container fluid={true} className="newsletterSection__wrapper">
          <Row>
            <Col md={3}>
              <img src={`${CzaplaImage}`} alt="Czapla" />
            </Col>
            <Col md={9} className="newsletterSection__content">
              <h3
                dangerouslySetInnerHTML={{
                  __html: this.props.data.newsletter_nested_title,
                }}
              />

              <form action="">
                <TextField
                  id="newsletter_name"
                  className=""
                  label="Imię"
                  margin="normal"
                  style={{ width: "25%" }}
                />
                <TextField
                  id="newsletter_email"
                  className=""
                  type="email"
                  label="Adres email"
                  margin="normal"
                  style={{
                    marginLeft: "30px",
                    width: "45%",
                  }}
                />
                <input
                  type="submit"
                  onClick={e => this.handleFormSubmit(e)}
                  value="Zapisuję się"
                />
              </form>
            </Col>
          </Row>
        </Container>
      </section>
    )
  }
}

export default () => (
  <StaticQuery
    query={graphql`
      query {
        allWordpressAcfOptions {
          nodes {
            websiteSettings {
              newsletter_title
              newsletter_nested_title
            }
          }
        }
      }
    `}
    render={data => (
      <Newsletter data={data.allWordpressAcfOptions.nodes[0].websiteSettings} />
    )}
  />
)
