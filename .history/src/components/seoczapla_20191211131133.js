import React from "react"
import Helmet from "react-helmet"

function SEOCzapla(props) {
  return (
    <Helmet>
      <title>{props.title}</title>
      <meta name="description" content={props.description} />
      <meta
        data-react-helmet="true"
        property="og:title"
        content={props.title}
      />
      <meta
        data-react-helmet="true"
        property="og:description"
        content={props.description}
      />
      <meta data-react-helmet="true" property="og:type" content="website" />
      <meta property="og:url" content="https://czaplaandmore.pl" />
      <meta
        data-react-helmet="true"
        property="og:image"
        content={props.image}
      ></meta>
      <link
        rel="canonical"
        href={data.wordpressPage.yoast_meta.yoast_wpseo_canonical}
      />
    </Helmet>
  )
}

export default SEOCzapla
