import React from "react"
import { StaticQuery, graphql, Link } from "gatsby"

import { Container, Navbar, Nav, Row } from 'react-bootstrap'

import "../sass/header.scss"
import Logo from "../images/logo.svg"

class Header extends React.Component {
  constructor(props){
    super(props)
    this.state = {
      activeClass: ''
    }
  }

  componentDidMount(){
    window.addEventListener('scroll', () => {
      let activeClass = 'scrolled'
      if (window.scrollY < 50) {
        activeClass = 'top'
      }
      this.setState({ activeClass: activeClass });
    });
  }

  render(){
    return (
      <Navbar expand="xl">
        <Navbar.Brand href="#home">
          <img src={Logo} alt="CzaplaAndMore Logo"/>
        </Navbar.Brand>

        <Navbar.Toggle aria-controls="basic-navbar-nav" />

        <Navbar.Collapse id="basic-navbar-nav">
          <Nav className="mr-auto">
            {this.props.menuItems.map((item, index) => (
              <Link key={index} to={item.object_slug}>
                {item.title}
              </Link>  
            ))}
          </Nav>
        </Navbar.Collapse>
      </Navbar>
    )
  }
}

export default () => (
  <StaticQuery
    query={graphql`
      query {
        wordpressWpApiMenusMenusItems(name: { eq: "Menu" }) {
          items {
            title
            object_slug
          }
        }
      }
    `}
    render={data => (
      <Header menuItems={data.wordpressWpApiMenusMenusItems.items} />
    )}
  />
)