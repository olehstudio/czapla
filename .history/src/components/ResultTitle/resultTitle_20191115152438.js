import React from "react"

import TitleLogo from "../../images/title_logo.svg"
import "./resultTitle.scss"

function ResultTitle(props) {
  return (
    <Col md={12} className="resultTitle">
      <img src={`${TitleLogo}`} alt="Czapla Logo" />
      <h1 dangerouslySetInnerHTML={{ __html: props.title }} />
      <h2 dangerouslySetInnerHTML={{ __html: props.text }} />
    </Col>
  )
}

export default ResultTitle
