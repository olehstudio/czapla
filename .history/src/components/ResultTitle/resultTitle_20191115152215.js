import React from "react"

import TitleLogo from "../../images/title_logo.svg"
import "./resultTitle.scss"

function ResultTitle(props) {
  return (
    <Col md={12} className="tagPage__title">
      <img src={`${TitleLogo}`} alt="Czapla Logo" />
      <h1>
        Strona <span>Tagu</span>
      </h1>
      <h2 dangerouslySetInnerHTML={{ __html: data.wordpressTag.name }} />
    </Col>
  )
}

export default ResultTitle
