export const Sierotki = () => {
  (function() {
    const addSierotki = function(text) {
      let new_text = text
      let sierotki = ["a", "i", "o", "u", "w", "z"]
      let return_sierotki = [
        " a&nbsp;",
        " i&nbsp;",
        " o&nbsp;",
        " u&nbsp;",
        " w&nbsp;",
        " z&nbsp;",
      ]
      let complicated_return_sierotki = [
        "&nbsp;a&nbsp;",
        "&nbsp;i&nbsp;",
        "&nbsp;o&nbsp;",
        "&nbsp;u&nbsp;",
        "&nbsp;w&nbsp;",
        "&nbsp;z&nbsp;",
      ]

      sierotki.forEach(function(key, value) {
        var replace = "\\s" + key + "\\s"
        var regexp = new RegExp(replace, "g")
        new_text = new_text.replace(regexp, return_sierotki[value])

        var complicated_replace = "&nbsp;" + key + "\\s"
        var complicated_regexp = new RegExp(complicated_replace, "g")
        new_text = new_text.replace(
          complicated_regexp,
          complicated_return_sierotki[value]
        )
      })

      return new_text
    }

    var elements = document.querySelectorAll("li, p, h1, h2, h3, h4, h5, h6")
    Array.prototype.forEach.call(elements, function(el, i) {
      el.innerHTML = addSierotki(el.innerHTML)
    })
  })()

  return null
}
