import React from "react"
import axios from "axios"
import { Row, Col } from "react-bootstrap"

import Title from "../Title/title"
import SmallPostCard from "../SmallPostCard/smallPostCard"
import "./mostReadable.scss"

class MostReadable extends React.Component {
  constructor(props) {
    super(props)

    this.state = {
      posts: [],
      isLoading: true,
    }
  }

  componentDidMount() {
    this.Skeleton = React.lazy(() => import("react-loading-skeleton"));

    axios({
      method: "GET",
      url: "https://czaplaandmore.pixelart.pl/wp-json/getMostViewedPosts/v2",
    }).then(response => {
      // console.log(response.data)
      this.setState({
        posts: response.data,
        isLoading: false,
      })
    })
  }

  render() {
    return (
      <Col
        lg={this.props.direction === "row" ? 12 : 3}
        className={`mostReadableSection ${
          this.props.direction === "column"
            ? "mostReadableSection__title"
            : null
        }`}
      >
        <Row>
          <Col md={12}>
            <Title text="Najczęściej czytane" mainTitle={false} />
          </Col>

          {this.state.isLoading ? (
            <Col md={this.props.direction === "row" ? 3 : 12}>
              
            </Col>
          ) : (
            this.state.posts.map((post, index) => (
              <SmallPostCard
                key={index}
                col={this.props.direction === "row" ? "3" : "12"}
                image={post.image}
                title={post.title}
                date={post.date}
                categories={post.categories}
                tags={post.tags}
                content={post.content}
                slug={post.slug}
              />
            ))
          )}
        </Row>
      </Col>
    )
  }
}

export default MostReadable
