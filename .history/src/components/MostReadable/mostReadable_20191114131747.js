import React from "react"
import axios from "axios"
import { Col } from "react-bootstrap"

import Title from "../Title/title"
import SmallPostCard from "../SmallPostCard/smallPostCard"
import "./mostReadable.scss"

class MostReadable extends React.Component {
  constructor(props) {
    super(props)

    this.state = {
      posts: [],
      isLoading: true,
    }
  }

  componentDidMount() {
    axios({
      method: "GET",
      url: "https://czaplaandmore.pixelart.pl/wp-json/getMostViewedPosts/v2",
    }).then(response => {
      console.log(response.data)
      this.setState({
        posts: response.data,
        isLoading: false,
      })
    })
  }

  render() {
    return (
      <Col lg={3} className="mostReadableSection">
        <Title text="Najczęściej czytane" mainTitle={false} />

        {this.state.posts.map((post, index) => (
          <SmallPostCard
            key={index}
            col="12"
            image={post.image}
            title={post.title}
            date={post.date}
            categories={post.categories}
            tags={post.tags}
            content={post.content}
            slug={post.slug}
          />
        ))}
      </Col>
    )
  }
}

export default MostReadable
