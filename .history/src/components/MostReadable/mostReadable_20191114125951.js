import React, { useState, useEffect } from "react"
import axios from "axios"
import { Col } from "react-bootstrap"

import SmallPostCard from "../SmallPostCard/smallPostCard"
import "./mostReadable.scss"

function MostReadable(props) {
  const [posts, setPosts] = React.useState([]);

  useEffect(() => {
    axios({
      method: "GET",
      url: "https://czaplaandmore.pixelart.pl/wp-json/getMostViewedPosts/v2",
    }).then(response => {
      console.log(response.data)
    //   setPosts(response.data);
    })
  })

  return (
    <Col lg={3}>
      <div>most viewed posts</div>

    </Col>
  )
}

export default MostReadable
