import React, { useState, useEffect } from "react"
import axios from "axios"
import { Col } from "react-bootstrap"

import SmallPostCard from "../SmallPostCard/smallPostCard"
import "./mostReadable.scss"

function MostReadable(props) {
  const [posts, setPosts] = React.useState([]);

  useEffect(() => {
    axios({
      method: "GET",
      url: "https://czaplaandmore.pixelart.pl/wp-json/getMostViewedPosts/v2",
    }).then(response => {
      console.log(response.data)
    //   setPosts(response.data);
    })
  })

  return (
    <Col lg={3}>
      <div>most viewed posts</div>

    </Col>
  )
}

import React from "react"
import { Container, Row, Col, Carousel } from "react-bootstrap"

import Arrow from "../ArrowComponent/arrowComponent"
import Title from "../Title/title"
import "./references.scss"

class MostReadable extends React.Component {
  constructor(props) {
    super(props)

    this.state = {
      posts: [],
    }

    this.myDiv = React.createRef()
  }

  componentDidMount() {
    this.setState({ carouselHeight: this.myDiv.current.offsetHeight })
  }

  render() {
    return (
      <section className="referencesSection">
        <Container>
          <Row>
            <Col md={12}>
              <Title text={this.props.title} mainTitle={false} />
            </Col>
            <Col ref={this.myDiv}>
              <Carousel
                prevIcon={<Arrow />}
                nextIcon={<Arrow />}
                pauseOnHover={true}
                slide={false}
              >
                {this.props.data.map((reference, index) => (
                  <Carousel.Item
                    key={index}
                    style={{
                      minHeight: `${this.state.carouselHeight}px`,
                    }}
                  >
                    <div
                      dangerouslySetInnerHTML={{
                        __html: reference.reference_content,
                      }}
                      className="referencesSection__contentWrapper"
                    />
                    <div
                      dangerouslySetInnerHTML={{
                        __html: reference.reference_author,
                      }}
                      className="referencesSection__author"
                    />
                  </Carousel.Item>
                ))}
              </Carousel>
            </Col>
          </Row>
        </Container>
      </section>
    )
  }
}

export default MostReadable
