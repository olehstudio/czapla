import React, { useEffect } from "react"
import { Col } from "react-bootstrap"

import SmallPostCard from "../SmallPostCard/smallPostCard"
import "./smallPostCard.scss"

function MostReadable(props) {
  useEffect(() => {
    axios({
      method: "GET",
      url: "https://czaplaandmore.pixelart.pl/wp-json/getMostViewedPosts/v2",
    }).then(response => {
      console.log(response)
    })
  })

  return (
    <Col lg={3}>
      <div>most viewed posts</div>
    </Col>
  )
}

export default MostReadable
