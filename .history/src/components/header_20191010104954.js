import React from "react"
import { StaticQuery, graphql, Link } from "gatsby"

const Header = () => (
  
)
export default () => {
  <StaticQuery
    query={graphql`
      query {
        wordpressWpApiMenusMenusItems(name: { eq: "Menu" }) {
          items {
            title
            object_slug
          }
        }
      }
    `}
    render={data => (
      <Header menuItems={data.wordpressWpApiMenusMenusItems.items} />
    )}
  />
}