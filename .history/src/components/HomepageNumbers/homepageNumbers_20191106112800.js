import React from "react"
import { Container, Row, Col } from "react-bootstrap"
import CountUp, { startAnimation } from "react-countup"
import VisibilitySensor from "react-visibility-sensor"

import Title from "../Title/title"
import "./homepageNumbers.scss"

class HomepageNumbers extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      didViewCountUp: false,
    }
  }

  onVisibilityChange = isVisible => {
    if (isVisible) {
      this.setState({ didViewCountUp: true })
    }
  }

  render() {
    return (
      <section className="homepageNumbers">
        <Container fluid="true">
          <Row>
            <Col md={12}>
              <Title text={this.props.title} mainTitle={false} />
            </Col>
          </Row>
        </Container>

        <Container>
        <Row className="homepageNumbers__wrapper" style={{
            backgroundImage: `linear-gradient(0deg, rgba(25, 25, 180, 0.85), rgba(25, 25, 180, 0.85)), url(${this.props.bg.localFile.childImageSharp.resolutions.src})`,
          }}>
            {this.props.data.map((section, index) => (
              <Col md={4} key={index}>
                <VisibilitySensor
                  onChange={this.onVisibilityChange}
                  offset={{
                    top: 10,
                  }}
                  delayedCall
                >
                  <CountUp
                    decimals={0}
                    start={0}
                    end={this.state.didViewCountUp ? section.number_single_number : 0}
                    separator=" "
                    duration={3}
                  />
                </VisibilitySensor>
                <p
                  dangerouslySetInnerHTML={{
                    __html: section.number_description,
                  }}
                />
              </Col>
            ))}
          </Row>
        </Container>
      </section>
    )
  }
}

export default HomepageNumbers
