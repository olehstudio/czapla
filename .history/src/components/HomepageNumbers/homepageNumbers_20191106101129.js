import React from "react"
import { Container, Row, Col } from "react-bootstrap"

import Title from "../Title/title"
import "./homepageNumbers.scss"

class HomepageNumbers extends React.Component {
  constructor(props) {
    super(props)
    this.state = {}
  }

  render() {
    return (
      <section className="newsletterSection">
        <Container>
          <Row>
            <Col md={12}>
              <Title
                text={this.props.data.newsletter_title}
                mainTitle={false}
              />
            </Col>
          </Row>
        </Container>

        <Container fluid={true} className="newsletterSection__wrapper">
          <Row>
            <Col md={3}>
              <img src={`${CzaplaImage}`} alt="Czapla" />
            </Col>
            <Col md={9} className="newsletterSection__content">
              <h3
                dangerouslySetInnerHTML={{
                  __html: this.props.data.newsletter_nested_title,
                }}
              />

              <form action="">
                <TextField
                  id="newsletter_name"
                  className=""
                  label="Imię"
                  margin="normal"
                  style={{ width: "25%" }}
                  disabled={this.state.isSent ? true : false}
                  onChange={e => this.setState({ name: e.target.value })}
                />
                <TextField
                  id="newsletter_email"
                  className=""
                  type="email"
                  label="Adres email"
                  margin="normal"
                  style={{
                    marginLeft: "30px",
                    width: "45%",
                  }}
                  disabled={this.state.isSent ? true : false}
                  onChange={e => this.setState({ email: e.target.value })}
                />
                {this.state.isSubmitting ? (
                  <CircularProgress />
                ) : (
                  <input
                    type="submit"
                    disabled={this.state.isSent ? true : false}
                    onClick={e => this.handleFormSubmit(e)}
                    value="Zapisuję się"
                  />
                )}
              </form>
              {this.state.isError ? (
                <p className="errorNotification">{this.state.errorText}</p>
              ) : null}
              {this.state.isSent ? (
                <p className="sentNotification">{this.state.sentMessage}</p>
              ) : null}
            </Col>
          </Row>
        </Container>
      </section>
    )
  }
}

export default HomepageNumbers