import React from "react"
import { Container, Row, Col } from "react-bootstrap"

import Title from "../Title/title"
import "./homepageNumbers.scss"

class HomepageNumbers extends React.Component {
  constructor(props) {
    super(props)
    this.state = {}
  }

  render() {
    return (
      <section className="homepageNumbers">
        <Container fluid="true">
          <Row>
            <Col md={12}>
              <Title
                text={this.props.title}
                mainTitle={false}
              />
            </Col>
          </Row>

          <Row className="homepageNumbers__wrapper">
              {this.props.data.map((section, index) => (
                  <Col md={4} key={index}>

                  </Col>
              ))}
          </Row>
        </Container>
      </section>
    )
  }
}

export default HomepageNumbers
