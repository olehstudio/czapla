import LazyLoad from "react-lazy-load"
import React from "react"
import { Container, Row, Col } from "react-bootstrap"

import Title from "../Title/title"
import "./homepageAbout.scss"

function HomepageAbout(props) {
  return (
    <section className="homepageAbout">
      <Container>
        <Row>
          <Col md={12}>
            <Title text={props.title} mainTitle={false} />
          </Col>

          <Col md={12}>
            <div dangerouslySetInnerHTML={{ __html: props.content }} />
          </Col>

          <Col md={12}>
            <LazyLoad offsetVertical={350}>
              <iframe src={`${props.video}`} border="none" style={{
                  width: '1200px',
                  maxWidth: '100%',
                  height: '600px',
                  background: '#EAEAEA',
                  border: 'none',
                  borderRadius: '7px',
                  margin: '50px auto 0',
              }}/>
            </LazyLoad>
          </Col>
        </Row>
      </Container>
    </section>
  )
}

export default HomepageAbout
