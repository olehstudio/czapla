import LazyLoad from 'react-lazy-load';
import React from "react"
import { StaticQuery, graphql, Link } from "gatsby"
import { Container, Row, Col } from "react-bootstrap"

import Logo from "../../images/logo_footer.svg"
import "./footer.scss"

class Footer extends React.Component {
  constructor(props) {
    super(props)
  }

  render() {
    return (
      <>
        <div
          className="siteFooter"
          style={{
            backgroundImage: `url(${this.props.content.footer_background_image.localFile.childImageSharp.resolutions.src})`,
          }}
        >
          <Container>
            <Row>
              <Col lg={12} className="text-center">
                <img src={`${Logo}`} alt="" className="siteFooter__logo" />

                <div
                  className="siteFooter__content"
                  dangerouslySetInnerHTML={{
                    __html: this.props.content.footer_content,
                  }}
                />

                <div className="siteFooter__links">
                  <a
                    href={`tel:${this.props.content.footer_telefon}`}
                    className="siteFooter__tel"
                  >
                    {this.props.content.footer_telefon}
                  </a>
                  <a
                    href={`mailto:${this.props.content.footer_email}`}
                    className="siteFooter__email"
                  >
                    {this.props.content.footer_email}
                  </a>

                  <Link
                    to={this.props.content.privacy_policy_link}
                    className="siteFooter__privacy"
                  >
                    {this.props.content.privacy_policy_text}
                  </Link>
                </div>
              </Col>
            </Row>
          </Container>
        </div>

        <div className="siteCopyright">
            <p dangerouslySetInnerHTML={{ __html: this.props.content.copyright_text }} />
            <a href="https://sgr.pl" target="_blank">Studio Graficzne</a>
        </div>
      </>
    )
  }
}

export default HomepageAbout