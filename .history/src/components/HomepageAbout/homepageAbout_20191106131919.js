import LazyLoad from "react-lazy-load"
import React from "react"
import { Container, Row, Col } from "react-bootstrap"

import Title from "../Title/title"
import "./homepageAbout.scss"

function HomepageAbout(props) {
  return (
    <section className="HomepageAbout">
      <Container>
        <Row>
          <Col md={12}>
            <Title text={props.title} mainTitle={false} />
          </Col>

          <Col md={12}>
              {props.content}
          </Col>
        </Row>
      </Container>
    </section>
  )
}

export default HomepageAbout
