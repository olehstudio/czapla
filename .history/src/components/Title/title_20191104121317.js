import React from "react"

import TitleLogo from "../../images/title_logo.svg"
import "./title.scss"

function Title(props) {
  return (
    <div className="titleWrapper">
      {props.mainTitle ? (
        <h1 className="titleWrapper__title">
            <img src={`${TitleLogo}`} alt=""/>
            {props.text}
        </h1>
      ) : (
        <img src={`${TitleLogo}`} alt=""/>
        <h2 className="titleWrapper__title">{props.text}</h2>
      )}
    </div>
  )
}

export default Title
