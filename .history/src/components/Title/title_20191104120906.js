import React from "react"

import "./title.scss"

function Title(props) {
  return (
    <>
      {props.mainTitle ? (
        <h1>{props.text}</h1>
      ) : (
        <h2>{props.text}</h2>
      )}
    </>
  )
}

export default Title
