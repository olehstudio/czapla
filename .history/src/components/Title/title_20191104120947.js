import React from "react"

import "./title.scss"

function Title(props) {
  return (
    <>
      {props.mainTitle ? (
        <h1 className="title">{props.text}</h1>
      ) : (
        <h2 className="title">{props.text}</h2>
      )}
    </>
  )
}

export default Title
