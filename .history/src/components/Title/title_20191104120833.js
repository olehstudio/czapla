import React from "react"

import "./title.scss"

function Title(props) {
  return (
    <>
      {this.props.mainTitle ? (
        <h1>{this.props.text}</h1>
      ) : (
        <h2>{this.props.text}</h2>
      )}
    </>
  )
}

export default Title
