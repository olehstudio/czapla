import React from "react"
import Link from "gatsby"

class SubMenuSection extends React.Component {
  constructor(props) {
    super(props)
    this.state = {}
  }

  componentDidMount() {
      console.log(this.props.children)
  }

  render() {
    return (
      <div className="submenuWrapper__categoryCol">
        <h3>{this.props.children.title}</h3>
        
      </div>
    )
  }
}

export default SubMenuSection
