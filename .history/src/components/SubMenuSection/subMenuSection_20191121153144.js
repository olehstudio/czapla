import React from "react"
import { Link } from "gatsby"
import { isMobile } from "react-device-detect"

class SubMenuSection extends React.Component {
  constructor(props) {
    super(props)
    this.state = {}
  }

  render() {
    return (
      <div className="submenuWrapper__categoryCol">
        <h3>{this.props.children.title}</h3>

        {this.props.children.wordpress_children && isMobile && (
          <svg
            width="16"
            height="10"
            viewBox="0 0 16 10"
            fill="none"
            xmlns="http://www.w3.org/2000/svg"
            onClick={isMobile && this.mobileOpenHandler}
          >
            <path
              d="M14 2L8 8L2 2"
              stroke="#C4C4C4"
              stroke-width="3"
              stroke-linecap="round"
              stroke-linejoin="round"
            />
          </svg>
        )}

        {this.props.children.wordpress_children.map((nestedChildren, index) =>
          nestedChildren.object === "page" ? (
            <Link
              key={`${nestedChildren.title}${index}`}
              to={`/${nestedChildren.object_slug}`}
            >
              {nestedChildren.title}
            </Link>
          ) : (
            <Link
              key={`${nestedChildren.title}${index}`}
              to={`/${nestedChildren.object}/${nestedChildren.object_slug}`}
            >
              {nestedChildren.title}
            </Link>
          )
        )}
      </div>
    )
  }
}

export default SubMenuSection
