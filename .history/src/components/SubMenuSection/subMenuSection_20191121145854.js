import React from "react"
import Link from "gatsby"

class SubMenuSection extends React.Component {
  constructor(props) {
    super(props)
    this.state = {}
  }

  render() {
    return (
      <div className="submenuWrapper__categoryCol">
        <h3>{this.props.children.title}</h3>
        {this.props.children}
      </div>
    )
  }
}

export default SubMenuSection
