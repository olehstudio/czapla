import React from "react"
import Link from "gatsby"

class SubMenuSection extends React.Component {
  constructor(props) {
    super(props)
    this.state = {}
  }

  componentDidMount() {
      console.log(this.props.children)
  }

  render() {
    return (
      <div className="submenuWrapper__categoryCol">
        <h3>{this.props.children.title}</h3>
        {this.props.children.wordpress_children.map((nestedChildren, index) =>
          nestedChildren.object === "page" ? (
            <Link
              key={`${nestedChildren.title}${index}`}
              to={`/${nestedChildren.object_slug}`}
            >
              {nestedChildren.title}
            </Link>
          ) : (
            <Link
              key={`${nestedChildren.title}${index}`}
              to={`/${nestedChildren.object}/${nestedChildren.object_slug}`}
            >
              {nestedChildren.title}
            </Link>
          )
        )}
      </div>
    )
  }
}

export default SubMenuSection
