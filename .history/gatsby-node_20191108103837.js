/**
 * Implement Gatsby's Node APIs in this file.
 *
 * See: https://www.gatsbyjs.org/docs/node-apis/
 */
// You can delete this file if you're not using it
const path = require(`path`)
exports.createPages = async ({ graphql, actions, reporter }) => {
  const { createPage } = actions
  const BlogPostTemplate = path.resolve("./src/templates/BlogPost.js")
  const PageTemplate = path.resolve("./src/templates/Page.js")
  const TemplateProgramyPage = path.resolve(
    "./src/templates/PageTemplates/TemplateProgramy.js"
  )

  const result = await graphql(`
    {
      allWordpressPost {
        edges {
          node {
            slug
            wordpress_id
          }
        }
      }
      allWordpressPage {
        edges {
          node {
            slug
            wordpress_id
          }
        }
      }
    }
  `)

  const templateProgramyPage = await graphql(`
  {
    allWordpressPage(filter: {template: {eq: "template-programy.php"}}) {
      edges {
        node {
          wordpress_id
          title
          path
        }
      }
    }
  }
  `)

  if (result.errors || templateProgramyPage.errors) {
    reporter.panicOnBuild(`Error while running GraphQL query.`)
    return
  }

  const BlogPosts = result.data.allWordpressPost.edges
  BlogPosts.forEach(post => {
    createPage({
      path: `/post/${post.node.slug}`,
      component: BlogPostTemplate,
      context: {
        id: post.node.wordpress_id,
      },
    })
  })
  const Pages = result.data.allWordpressPage.edges
  Pages.forEach(page => {
    createPage({
      path: `/${page.node.slug}`,
      component: PageTemplate,
      context: {
        id: page.node.wordpress_id,
      },
    })
  })
  const ProgramyPage = templateProgramyPage.data.allWordpressPage.edges
  ProgramyPage.forEach(page => {
    createPage({
      path: `/${page.node.path}`,
      component: TemplateProgramyPage,
      context: {
        id: page.node.wordpress_id,
      },
    })
  })
}
