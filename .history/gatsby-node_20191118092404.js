/**
 * Implement Gatsby's Node APIs in this file.
 *
 * See: https://www.gatsbyjs.org/docs/node-apis/
 */
// You can delete this file if you're not using it
const path = require(`path`)
exports.createPages = async ({ graphql, actions, reporter }) => {
  const { createPage } = actions
  const PageTemplate = path.resolve("./src/templates/Page.js")
  const TemplateProgramyPage = path.resolve(
    "./src/templates/PageTemplates/TemplateProgramy.js"
  )
  const TemplateProductsPage = path.resolve(
    "./src/templates/PageTemplates/TemplateProducts.js"
  )
  const TemplateAbout = path.resolve(
    "./src/templates/PageTemplates/TemplateAbout.js"
  )
  const TemplateContact = path.resolve(
    "./src/templates/PageTemplates/TemplateContact.js"
  )
  const TemplateSingleProgram = path.resolve(
    "./src/templates/PostTemplates/TemplateSingleProgram.js"
  )
  const TemplateSingleProduct = path.resolve(
    "./src/templates/PostTemplates/TemplateSingleProduct.js"
  )
  const TemplateSinglePost = path.resolve(
    "./src/templates/PostTemplates/TemplateSinglePost.js"
  )
  const TemplateTag = path.resolve(
    "./src/templates/PageTemplates/TemplateTag.js"
  )

  const result = await graphql(`
    {
      allWordpressPage {
        edges {
          node {
            slug
            wordpress_id
          }
        }
      }
    }
  `)

  const templateProgramyPage = await graphql(`
    {
      allWordpressPage(filter: { template: { eq: "template-programy.php" } }) {
        edges {
          node {
            wordpress_id
            title
            path
          }
        }
      }
    }
  `)

  const templateProductsPage = await graphql(`
    {
      allWordpressPage(filter: { template: { eq: "template-productsCzapla.php" } }) {
        edges {
          node {
            wordpress_id
            title
            path
          }
        }
      }
    }
  `)

  const templateAbout = await graphql(`
    {
      allWordpressPage(filter: { template: { eq: "template-about.php" } }) {
        edges {
          node {
            wordpress_id
            title
            path
          }
        }
      }
    }
  `)

  const templateContact = await graphql(`
    {
      allWordpressPage(filter: { template: { eq: "template-contact.php" } }) {
        edges {
          node {
            wordpress_id
            title
            path
          }
        }
      }
    }
  `)

  const templateSingleProgram = await graphql(`
    {
      allWordpressWpProgram {
        edges {
          node {
            wordpress_id
            path
          }
        }
      }
    }
  `)

  const templateSinglePost_aktualnosci = await graphql(`
    {
      allWordpressPost(
        filter: { categories: { elemMatch: { name: { eq: "Aktualności" } } } }
      ) {
        edges {
          node {
            slug
            wordpress_id
          }
        }
      }
    }
  `)

  const templateSinglePost_blog = await graphql(`
    {
      allWordpressPost(
        filter: { categories: { elemMatch: { name: { eq: "Blog" } } } }
      ) {
        edges {
          node {
            slug
            wordpress_id
          }
        }
      }
    }
  `)

  const templateSinglePost_mediaONas = await graphql(`
    {
      allWordpressPost(
        filter: { categories: { elemMatch: { name: { eq: "Media o nas" } } } }
      ) {
        edges {
          node {
            slug
            wordpress_id
          }
        }
      }
    }
  `)

  const templateTag = await graphql(`
    {
      allWordpressTag {
        edges {
          node {
            slug
            wordpress_id
            name
          }
        }
      }
    }
  `)

  if (result.errors || templateProgramyPage.errors) {
    reporter.panicOnBuild(`Error while running GraphQL query.`)
    return
  }

  const Pages = result.data.allWordpressPage.edges
  Pages.forEach(page => {
    createPage({
      path: `/${page.node.slug}`,
      component: PageTemplate,
      context: {
        id: page.node.wordpress_id,
      },
    })
  })

  const ProgramyPage = templateProgramyPage.data.allWordpressPage.edges
  ProgramyPage.forEach(page => {
    createPage({
      path: `${page.node.path}`,
      component: TemplateProgramyPage,
      context: {
        id: page.node.wordpress_id,
      },
    })
  })

  const ProductsPage = templateProductsPage.data.allWordpressPage.edges
  ProductsPage.forEach(page => {
    createPage({
      path: `${page.node.path}`,
      component: TemplateProductsPage,
      context: {
        id: page.node.wordpress_id,
      },
    })
  })

  const AboutPage = templateAbout.data.allWordpressPage.edges
  AboutPage.forEach(page => {
    createPage({
      path: `${page.node.path}`,
      component: TemplateAbout,
      context: {
        id: page.node.wordpress_id,
      },
    })
  })

  const ContactPage = templateContact.data.allWordpressPage.edges
  ContactPage.forEach(page => {
    createPage({
      path: `${page.node.path}`,
      component: TemplateContact,
      context: {
        id: page.node.wordpress_id,
      },
    })
  })

  const SingleProgramPage =
    templateSingleProgram.data.allWordpressWpProgram.edges
  SingleProgramPage.forEach(page => {
    createPage({
      path: `${page.node.path}`,
      component: TemplateSingleProgram,
      context: {
        id: page.node.wordpress_id,
      },
    })
  })

  const SingleProductsPage =
    templateSingleProduct.data.allWordpressWpProgram.edges
  SingleProgramPage.forEach(page => {
    createPage({
      path: `${page.node.path}`,
      component: TemplateSingleProgram,
      context: {
        id: page.node.wordpress_id,
      },
    })
  })

  // Single Post by categories
  const SinglePost_aktualnosci = templateSinglePost_aktualnosci.data.allWordpressPost.edges
  SinglePost_aktualnosci.forEach(post => {
    createPage({
      path: `/aktualnosci/${post.node.slug}`,
      component: TemplateSinglePost,
      context: {
        id: post.node.wordpress_id,
        category: 'Aktualności',
      },
    })
  })
  const SinglePost_blog = templateSinglePost_blog.data.allWordpressPost.edges
  SinglePost_blog.forEach(post => {
    createPage({
      path: `/blog/${post.node.slug}`,
      component: TemplateSinglePost,
      context: {
        id: post.node.wordpress_id,
        category: 'Blog',
      },
    })
  })
  const SinglePost_mediaONas = templateSinglePost_mediaONas.data.allWordpressPost.edges
  SinglePost_mediaONas.forEach(post => {
    createPage({
      path: `/media-o-nas/${post.node.slug}`,
      component: TemplateSinglePost,
      context: {
        id: post.node.wordpress_id,
        category: 'Media o nas',
      },
    })
  })

  // Create blog-list pages
  const postsAktualnosci = templateSinglePost_aktualnosci.data.allWordpressPost.edges
  const postsPerPage = 2
  const numPagesAktualnosci = Math.ceil(postsAktualnosci.length / postsPerPage)
  Array.from({ length: numPagesAktualnosci }).forEach((_, i) => {
    createPage({
      path: i === 0 ? `/aktualnosci` : `/aktualnosci/page-${i + 1}`,
      component: path.resolve("./src/templates/PageTemplates/TemplateCategory.js"),
      context: {
        category: 'Aktualności',
        limit: postsPerPage,
        skip: i * postsPerPage,
        numPages: numPagesAktualnosci,
        currentPage: i + 1,
      },
    })
  })
  const postsBlog = templateSinglePost_blog.data.allWordpressPost.edges
  const numPagesBlog = Math.ceil(postsBlog.length / postsPerPage)
  Array.from({ length: numPagesBlog }).forEach((_, i) => {
    createPage({
      path: i === 0 ? `/blog` : `/blog/page-${i + 1}`,
      component: path.resolve("./src/templates/PageTemplates/TemplateCategory.js"),
      context: {
        category: 'Blog',
        limit: postsPerPage,
        skip: i * postsPerPage,
        numPages: numPagesBlog,
        currentPage: i + 1,
      },
    })
  })
  const postsMediaONas = templateSinglePost_mediaONas.data.allWordpressPost.edges
  const numPagesMediaONas = Math.ceil(postsMediaONas.length / postsPerPage)
  Array.from({ length: numPagesMediaONas }).forEach((_, i) => {
    createPage({
      path: i === 0 ? `/media-o-nas` : `/media-o-nas/page-${i + 1}`,
      component: path.resolve("./src/templates/PageTemplates/TemplateCategory.js"),
      context: {
        category: 'Media o nas',
        limit: postsPerPage,
        skip: i * postsPerPage,
        numPages: numPagesMediaONas,
        currentPage: i + 1,
      },
    })
  })

  // Tag template
  const TagPage = templateTag.data.allWordpressTag.edges
  TagPage.forEach(tag => {
    createPage({
      path: `/tag/${tag.node.slug}`,
      component: TemplateTag,
      context: {
        id: tag.node.wordpress_id,
        name: tag.node.name,
      },
    })
  })
}
