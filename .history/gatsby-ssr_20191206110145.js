/**
 * Implement Gatsby's SSR (Server Side Rendering) APIs in this file.
 *
 * See: https://www.gatsbyjs.org/docs/ssr-apis/
 */

// You can delete this file if you're not using it

import React from "react";
export function onRenderBody({ setHeadComponents }) {
setHeadComponents([
     <script
        type="text/javascript"
        src="https://www.googletagmanager.com/gtag/js?id=UA-111807501-1"
      />,
     <script
     dangerouslySetInnerHTML={{
        __html = `
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'UA-111807501-1');
        gtag('config', 'AW-822281140');
   	 `
     }}
     />,
]);
}