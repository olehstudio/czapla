/**
 * Implement Gatsby's Node APIs in this file.
 *
 * See: https://www.gatsbyjs.org/docs/node-apis/
 */
// You can delete this file if you're not using it
const path = require(`path`)
exports.createPages = async ({ graphql, actions, reporter }) => {
  const { createPage } = actions
  const BlogPostTemplate = path.resolve("./src/templates/BlogPost.js")
  const PageTemplate = path.resolve("./src/templates/Page.js")
  const TemplateProgramyPage = path.resolve(
    "./src/templates/PageTemplates/TemplateProgramy.js"
  )
  const TemplateAbout = path.resolve(
    "./src/templates/PageTemplates/TemplateAbout.js"
  )
  const TemplateSingleProgram = path.resolve(
    "./src/templates/PostTemplates/TemplateSingleProgram.js"
  )
  const TemplateSinglePost = path.resolve(
    "./src/templates/PostTemplates/TemplateSinglePost.js"
  )

  const result = await graphql(`
    {
      allWordpressPage {
        edges {
          node {
            slug
            wordpress_id
          }
        }
      }
    }
  `)

  const templateProgramyPage = await graphql(`
    {
      allWordpressPage(filter: { template: { eq: "template-programy.php" } }) {
        edges {
          node {
            wordpress_id
            title
            path
          }
        }
      }
    }
  `)

  const templateAbout = await graphql(`
    {
      allWordpressPage(filter: { template: { eq: "template-about.php" } }) {
        edges {
          node {
            wordpress_id
            title
            path
          }
        }
      }
    }
  `)

  const templateSingleProgram = await graphql(`
    {
      allWordpressWpProgram {
        edges {
          node {
            wordpress_id
            path
          }
        }
      }
    }
  `)

  const templateSinglePost = await graphql(`
    {
      allWordpressPost {
        edges {
          node {
            slug
            wordpress_id
          }
        }
      }
    }
  `)

  if (result.errors || templateProgramyPage.errors) {
    reporter.panicOnBuild(`Error while running GraphQL query.`)
    return
  }

  const Pages = result.data.allWordpressPage.edges
  Pages.forEach(page => {
    createPage({
      path: `/${page.node.slug}`,
      component: PageTemplate,
      context: {
        id: page.node.wordpress_id,
      },
    })
  })

  const ProgramyPage = templateProgramyPage.data.allWordpressPage.edges
  ProgramyPage.forEach(page => {
    createPage({
      path: `${page.node.path}`,
      component: TemplateProgramyPage,
      context: {
        id: page.node.wordpress_id,
      },
    })
  })

  const AboutPage = templateAbout.data.allWordpressPage.edges
  AboutPage.forEach(page => {
    createPage({
      path: `${page.node.path}`,
      component: TemplateAbout,
      context: {
        id: page.node.wordpress_id,
      },
    })
  })

  const SingleProgramPage =
    templateSingleProgram.data.allWordpressWpProgram.edges
  SingleProgramPage.forEach(page => {
    createPage({
      path: `${page.node.path}`,
      component: TemplateSingleProgram,
      context: {
        id: page.node.wordpress_id,
      },
    })
  })
  
  const SinglePost = templateSinglePost.data.allWordpressPost.edges
  SinglePost.forEach(post => {
    createPage({
      path: `/post/${post.node.slug}`,
      component: TemplateSinglePost,
      context: {
        id: post.node.wordpress_id,
      },
    })
  })
}
